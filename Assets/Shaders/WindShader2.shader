﻿Shader "MechVR/WindShader2" {
   Properties {
      _MainTex ("Texture Image", 2D) = "white" {}
      _WorldSize("World Size", vector) = (1, 1, 1, 1)
      _WindSpeed("Wind Speed", vector) = (1, 1, 1, 1)
      _HeightCutoff("Height Cutoff", float) = 1.0
   }
   
   SubShader
     {
         Tags { "Queue"="Transparent" }
         Cull Off
         
         CGPROGRAM
         
         #pragma surface surf Lambert vertex:vert alpha
         
         struct Input {
            float2 uv_MainTex;
         };
                
         float4 _WorldSize;
         float4 _WindSpeed;
         float _HeightCutoff;
             
         void vert (inout appdata_full v) {
            float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
            float2 normalizedPos = worldPos.xz / _WorldSize.xz;
                   normalizedPos += _Time * _WindSpeed.xz;
                
            float heightFactor = v.vertex.y > _HeightCutoff;
            v.vertex.x += sin(normalizedPos.x) * heightFactor;
            v.vertex.y += cos(normalizedPos.y) * heightFactor;
         }
         
         sampler2D _MainTex;
         
         void surf (Input IN, inout SurfaceOutput o) {
            half4 c = tex2D (_MainTex, IN.uv_MainTex);
            o.Albedo = c.rgb;
            o.Alpha = c.a;
         }
         
         ENDCG
     }
}
