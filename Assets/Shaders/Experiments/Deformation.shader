﻿
Shader "MechVR/Deformation" {
    Properties {
      _MainTex ("Texture Image", 2D) = "white" {}
      _WorldSize("World Size", vector) = (1, 1, 1, 1)
      _PivotOffset("PivotOffset", vector) = (1, 1, 1, 1)
      _Amplitude("_Amplitude", float) = 1
      _Min("Min", float) = 1
   }
   
   SubShader
     {
         Tags { "RenderType"="Opaque" }
         
         
         Pass
         {
             CGPROGRAM
 
             #pragma vertex vert
             #pragma fragment frag
    
             // User-specified uniforms            
             uniform sampler2D _MainTex;
             float4 _WorldSize;
             float4 _PivotOffset;
             float _Amplitude;
             float _Min;
             
             struct vertexInput {
                float4 vertex : POSITION;
                float4 tex : TEXCOORD0;
             };
             struct vertexOutput {
                float4 pos : SV_POSITION;
                float4 tex : TEXCOORD0;
                float2 ps : TEXCOORD1;
             };
     
             vertexOutput vert(vertexInput input) 
             {
                vertexOutput output;
                
                float4 worldPos = mul(unity_ObjectToWorld, input.vertex);
                float2 normalizedPos = worldPos.xz / _WorldSize.xz;
                       normalizedPos += _PivotOffset.xz;
                       
                float offset = sin(normalizedPos.y) * _Amplitude;  
                float lengthFactor = offset > _Min;
                input.vertex.z += sin(normalizedPos.y) * _Amplitude * lengthFactor;
                
             
                output.ps = normalizedPos;
                output.pos = UnityObjectToClipPos(input.vertex);
                output.tex = input.tex;
 
                return output;
             }
     
             float4 frag(vertexOutput input) : COLOR
             {
                float4 c = tex2D(_MainTex, float2(input.tex.xy));
                
                //debug
                //c.r += sin(input.ps.y);
                
                return c;  
             }
     
             ENDCG
         }
     }
}
