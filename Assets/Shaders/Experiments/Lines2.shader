﻿Shader "Custom Shaders/Lines2" 
{
     //это будет видно в инспекторе
    Properties {
        _MainTex ("Texture (RGB)", 2D) = "white" { }
        _HighlightColor ("Highlight Color", Color) = (1,1,1,1)
        _DistanceX ("Distance X", Float) = 1.0
        _DistanceY ("Distance Y", Float) = 1.0
        _Width ("Width", Float) = 0.1
    }
    SubShader {
        Tags { 
            //
            "Queue" = "Geometry"
            //непрозрачный шейдер (для очереди вывода изображения)
            "RenderType" = "Opaque"
         }
        //без отсечения, полигон виден с двух сторон
        
        //вставка на языке шейдеров CG
        CGPROGRAM
        
            //объявляем функцию surf с параметром Lambert(освещение по ламберту)
            #pragma surface surf Lambert
            //входная структура
            struct Input 
            {
                //uv координаты
                float2 uv_MainTex;
                //позиция точки в игровом мире
                float3 worldPos;
            };
            
            sampler2D _MainTex;
            fixed4 _HighlightColor;
            float _DistanceX;
            float _DistanceY;
            float _Width;
            
            void surf (Input IN, inout SurfaceOutput o) 
            {
                //получить цвет пикселя rgba
               float4 tex = tex2D (_MainTex, IN.uv_MainTex);
               //какие пиксели пропускаем при рендеринге
               //clip (tex.a - 0.5);
               //наложение текстуры на объект
               _Width *= 0.01;
               if (/*fmod( IN.uv_MainTex.y - IN.uv_MainTex.x  , _DistanceX * 0.01) < _Width ||*/
                fmod( IN.uv_MainTex.y + IN.uv_MainTex.x  , _DistanceY * 0.01) < _Width){
                    o.Albedo = _HighlightColor;
               }else{o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb; }
            }
        
        ENDCG
    }
    
    Fallback "Diffuse"
} 