﻿Shader "Custom/GradientSky"
{
	Properties
	{
		_SkyColor ("Sky Color", Color) = (0.375, 0.625, 0.75, 1)
		_EquatorColor ("Equator Color", Color) = (1, 0.898, 0.918, 1)
		_GroundColor ("Ground Color", Color) = (0.5, 0.5, 0.5, 1)

		_Power ("Power", Float) = 1
		_Intensity ("Intensity", Float) = 1
	}

	Subshader
	{
		Tags { "Queue" = "Background" "RenderType" = "Background" "PreviewType" = "Skybox" }

		Pass
		{
			Cull Off

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			struct appdata
			{
				float4 position : POSITION;
				float4 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 position : SV_POSITION;
				half4 color : COLOR;
			};

			half4 _SkyColor;
			half4 _EquatorColor;
			half4 _GroundColor;

			half _Power;
			half _Intensity;

			v2f vert (appdata input)
			{
				v2f output;

				// Position
				output.position = UnityObjectToClipPos(input.position);

				// Color
				float fac = pow(abs(input.texcoord.y), _Power) * sign(input.texcoord.y);

				float skyFac = max(0, fac);
				float groundFac = max(0, -fac);
				float equatorFac = 1 - skyFac - groundFac;

				output.color = (_SkyColor * skyFac + _EquatorColor * equatorFac + _GroundColor * groundFac) * _Intensity;

				return output;
			}

			half4 frag (v2f input) : SV_Target
			{
				return input.color;
			}

			ENDCG
		}

	}

	Fallback Off
}