﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuzzleFlash : MonoBehaviour {

	Transform m_mesh;

	void Start () {
		m_mesh = transform.Find("Mesh");
		DisableMesh();
	}

	public void Flash (float time) {		
		m_mesh.gameObject.SetActive(true);
		m_mesh.RotateAround(m_mesh.position, m_mesh.up, 360f * Random.value);
		
		Invoke("DisableMesh", time);
	}
	
	void DisableMesh () {
		m_mesh.gameObject.SetActive(false);
	}
}
