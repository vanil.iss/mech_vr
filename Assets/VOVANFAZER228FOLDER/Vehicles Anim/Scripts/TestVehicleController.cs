﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestVehicleController : MonoBehaviour {
	
	public float force = 2000f;
	public float torque = 2000f;
	
	Rigidbody m_rigidbody;
	VehicleAnimator m_vehicleAnimator;
	
	void Awake () {
		m_rigidbody = GetComponent<Rigidbody>();
		m_vehicleAnimator = GetComponent<VehicleAnimator>();
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		m_rigidbody.AddForce(transform.forward * force * Input.GetAxis("Vertical") * Time.deltaTime);
		m_rigidbody.AddTorque(transform.up * torque * Input.GetAxis("Horizontal") * Time.deltaTime);
		
		if (Input.GetButtonDown("Jump"))
			m_vehicleAnimator.SetTrigger(VehicleAnimator.Trigger.Shoot);
	}
}
