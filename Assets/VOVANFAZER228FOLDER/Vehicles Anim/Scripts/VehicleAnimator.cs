﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleAnimator : MonoBehaviour {
	
	[Serializable]
	public enum Trigger {
		Stand,
		Shoot,
		Death
	}
	
	[Serializable]
	public class Parameters {
		public bool hasSuspension = true;
		public bool hasWheels = true;
		[Space]
		public float maxForwardAccel = 10f;
		public float maxAngularVelocity = 60f;
		[Space]
		public float blendXLerp = 16f;
		public float blendYLerp = 8f;
		[Space]
		public float wheelRotMult = 0.35f;
		public Transform[] rotatingWheels;
		[Space]
		public float muzzleFlashTime = 0.1f;
	}
	
	public Parameters prm => m_prm;
	
	[SerializeField] Parameters m_prm = new Parameters();

	Animator m_animator;
	MuzzleFlash m_muzzleFlash;
	
	float m_blendX;
	Vector3 m_prevForward; // Previous forward vector
	
	float m_blendY;
	Vector3 m_prevPosition;
	Vector3 m_prevVelocity;
	
	float m_wheelRot;

	void Awake() {
		m_animator = GetComponent<Animator>();
		m_muzzleFlash = GetComponentInChildren<MuzzleFlash>();
	}
	
	// Use this for initialization
	void Start () {
		m_prevPosition = transform.position;
		m_prevVelocity = Vector3.zero;
		m_prevForward = transform.forward;
	}
	
	void FixedUpdate () {
		if (m_prm.hasSuspension)
			UpdateSuspension();
	}
	
	void LateUpdate () {
		if (m_prm.hasWheels)
			UpdateWheels();
	}
	
	public void SetTrigger (Trigger trigger) {
		m_animator.SetTrigger(trigger.ToString());
		
		if (trigger == Trigger.Shoot)
			m_muzzleFlash.Flash(m_prm.muzzleFlashTime);
	}
		
	
	void UpdateSuspension () {
		float inversedDeltaTime = 1 / Time.deltaTime;
		
		// Update "Blend X"
		Vector3 forward = transform.forward;
		float angularVelocity = Vector3.SignedAngle(m_prevForward, forward, Vector3.up) * inversedDeltaTime;
		m_prevForward = forward;
		
		// Wheel rotation caching
		m_wheelRot = angularVelocity;
		
		float blendX = Mathf.InverseLerp(0f, m_prm.maxAngularVelocity, Mathf.Abs(angularVelocity))
						* Mathf.Sign(angularVelocity);
		m_blendX = Mathf.Lerp(m_blendX, blendX, m_prm.blendXLerp *  Time.deltaTime);
		m_animator.SetFloat("Blend X", m_blendX);	
		
		// Update "Blend Y"
		Vector3 position = transform.position;
		Vector3 velocity = (position - m_prevPosition) * inversedDeltaTime;
		m_prevPosition = position;
		Vector3 acceleration = (velocity - m_prevVelocity) * inversedDeltaTime;
		m_prevVelocity = velocity;
		float forwardAcceleration = Vector3.Dot(acceleration, transform.forward);
		
		float blendY = Mathf.InverseLerp(0f , m_prm.maxForwardAccel, Mathf.Abs(forwardAcceleration))
						* Mathf.Sign(forwardAcceleration) * -1f;
		m_blendY = Mathf.Lerp(m_blendY, blendY, m_prm.blendYLerp * Time.deltaTime);
		m_animator.SetFloat("Blend Y", m_blendY);
	}
	
	void UpdateWheels () {
		for (int i = 0; i < m_prm.rotatingWheels.Length; i++) {
			Transform wheel = m_prm.rotatingWheels[i];
			wheel.RotateAround(wheel.position, transform.up, m_wheelRot * m_prm.wheelRotMult);
		}
	}
}
