﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapIcon : MonoBehaviour {

	public Transform targetTransform;
	public bool is_always_on_map;
	public float minScale = 1;

	private RectTransform rectTransform;
	private MiniMap miniMap;


	void Start()
	{
		miniMap = GetComponentInParent<MiniMap>();
		rectTransform = GetComponent<RectTransform>();
	}

	void LateUpdate()
	{
		Vector2 pos = miniMap.GetLocalPosition(targetTransform.position);
		if(is_always_on_map){ pos = miniMap.GetBorderPosition(pos); }
		rectTransform.localPosition = pos;
		rectTransform.localEulerAngles = miniMap.GetLocalRotation(targetTransform.eulerAngles);

		float scale = Mathf.Max(minScale, miniMap.zoom);
		rectTransform.localScale = new Vector3(scale, scale, 1);
	}
}
