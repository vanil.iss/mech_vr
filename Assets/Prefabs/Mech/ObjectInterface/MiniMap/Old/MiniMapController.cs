﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapController : MonoBehaviour {

	//объект иконки моба
	public GameObject mobIconGameObject;
	//объект даты, для получения списка всех мобов
	Data data;
	//последнее количество мобов на карте, нужно для проверки на изменение списка в Data
	int lastMobCount = 0;
	//список мобов, которые уже имеют иконку на карте
	List<GameObject> mobListWithIcons = new List<GameObject>();
	//сам скриптовый объект карты, хранящий ее параметры
	private MiniMap miniMap;

	void Start () {
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<Data>();
		lastMobCount = data.mobList.Count;
		foreach(GameObject obj in data.mobList){ AddIcon(obj); }

		miniMap = transform.GetComponent<MiniMap>();
	}
	
	void LateUpdate () {
		if (lastMobCount != data.mobList.Count) {
			lastMobCount = data.mobList.Count;
			foreach (GameObject obj in data.mobList) {
				if (!mobListWithIcons.Contains (obj)) { AddIcon(obj); }
			}
		}
	}

	public void AddIcon(GameObject obj)
	{
		GameObject icon = Instantiate (mobIconGameObject);
		icon.GetComponent<MiniMapIcon> ().targetTransform = obj.transform;
		icon.transform.SetParent (transform);
		mobListWithIcons.Add (obj);
	}

	public void Zoom(float value)
	{
		miniMap.zoom += value;
		if (miniMap.zoom < 0) miniMap.zoom = 0;
	}
}
