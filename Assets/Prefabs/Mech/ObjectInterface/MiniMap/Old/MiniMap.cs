﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour {

	public Transform mechTransform;
	private Vector2 mechXRotation = new Vector2();
	private Vector2 mechYRotation = new Vector2(); 	
	
	public float zoom = 1;
	
	public Vector2 GetLocalPosition(Vector3 position)
	{
		mechXRotation.Set(mechTransform.right.x, -mechTransform.right.z);
		mechYRotation.Set(-mechTransform.forward.x, mechTransform.forward.z);

		Vector3 offset = position - mechTransform.position;
		Vector2 pos = offset.x * mechXRotation;
		pos += offset.z * mechYRotation;
		pos *= zoom;
		return pos;
	}

	public Vector3 GetLocalRotation(Vector3 rotation) { 
		rotation.Set(0,0, mechTransform.eulerAngles.y - rotation.y);
		return rotation;
	}

	public Vector2 GetBorderPosition(Vector2 point)
	{
		Rect rect = GetComponent<RectTransform>().rect;
		point = Vector2.Max(point, rect.min);
		point = Vector2.Min(point, rect.max);
		return point;
	}
}
