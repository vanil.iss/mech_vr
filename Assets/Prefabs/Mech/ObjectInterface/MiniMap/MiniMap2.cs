﻿using System.Collections;
using System.Collections.Generic;
using Unit.Mech;
using UnityEditor;
using UnityEngine;
using UnityEngine.XR.WSA.Persistence;

public class MiniMap2 : MonoBehaviour
{

	[SerializeField] private Transform mechTransform;
	[SerializeField] private float map_scale = 1;
	
	[Header(" Icons: [Mech, Mob, Building, WayPoint] ")]
	[SerializeField] private GameObject[] iconsObjects = new GameObject[4];
	[System.Serializable] public enum IconType { Mech = 0, Mob = 1, Building = 2, WayPoint = 3 }
	
	[System.Serializable]
	public class MiniMapTarget
	{
		public Transform target;
		public IconType type;
		public float scale = 1;
		public RectTransform Icon { get; set; }
	}
	
	[SerializeField] private MiniMapTarget[] targets = {};

	[Space]
	[SerializeField] private bool MovingMap;
	[SerializeField] private Transform globalMapTarget;
	[SerializeField] private RectTransform globalMapIcon;
	
	void Start () {
		foreach (MiniMapTarget t in targets) { AddIcon(t); }
	}
	
	void Update () {

		foreach (MiniMapTarget t in targets)
		{
			if (t.target == null)
			{
				Debug.Log("MiniMap error: target transform is null, possibly you forgot set target in editor.");
				break;
			}
			t.Icon.localPosition = GetIconPosition(t.target.position);
			t.Icon.localEulerAngles = GetIconRotation(t.target.eulerAngles);
		}

		if (MovingMap)
		{
			globalMapIcon.localPosition = GetIconPosition(globalMapTarget.position);
			globalMapIcon.localEulerAngles = GetIconRotation(globalMapTarget.eulerAngles);
		}

	}

	private void AddIcon(MiniMapTarget t)
	{
		int typeInt = (int) t.type;
		if ( typeInt >= iconsObjects.Length) { Debug.Log("MiniMap error: wrong icon type."); return; }
		
		GameObject iconObject = iconsObjects[typeInt];
		if (iconObject == null) { Debug.Log("MiniMap error: Theres no icon of type " + t.type.ToString() + " possibly you forgot to set icon object in editor."); return; }
		
		GameObject icon = Instantiate(iconObject);
		icon.transform.SetParent (transform);
		
		t.Icon = icon.GetComponent<RectTransform>();
		t.Icon.localScale = new Vector3(t.scale, t.scale, 1);
	}

	private Vector3 GetIconRotation(Vector3 targetRotation)
	{
		targetRotation.Set(0, 0, mechTransform.eulerAngles.y - targetRotation.y);
		return targetRotation; 
	}

	Vector2 mechXRotation = new Vector2();
	Vector2 mechYRotation = new Vector2();
	private Vector3 GetIconPosition(Vector3 targetPosition)
	{
		mechXRotation.Set( mechTransform.right.x,   -mechTransform.right.z);
		mechYRotation.Set(-mechTransform.forward.x,  mechTransform.forward.z);

		Vector3 offset = targetPosition - mechTransform.position;
		Vector2 iconPosition = offset.x * mechXRotation;
				iconPosition += offset.z * mechYRotation;
				iconPosition *= map_scale;
		return iconPosition;
	}
}
