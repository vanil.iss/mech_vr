﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public enum MiniMapTargetType
{
	Mech = 0, Mob = 1, Building = 2, WayPoint = 3
}

[Serializable]
public class MiniMapTarget
{
	public Transform target;
	public MiniMapTargetType type;
	public float scale = 12.5f;

	public float getX() { return target.position.x; }
	public float getY() { return target.rotation.y; }
	public float getZ() { return target.position.z; }
	public float getW() { return target.rotation.w; }
}

public class MiniMap1 : MonoBehaviour
{
	[SerializeField]
	private MiniMapTarget[] targets = {};

	//private Image image;
	private Renderer renderer;
	private Vector4[] iconsCoordinates;
	private Vector4[] iconsProperties;

	private int[][] atlasInts =
	{
		new int[]{1, 1}, 
		new int[]{0, 1}, 
		new int[]{0, 0}, 
		new int[]{1, 0}
	};
	
	// Use this for initialization
	void Start () {
		//image = GetComponent<Image>();
		renderer = GetComponent<Renderer>();
		iconsCoordinates = new Vector4[256];
		iconsProperties = new Vector4[256];
		
		for (int i = 0; i < targets.Length; i++)
		{
			iconsCoordinates[i] = new Vector4(0, 0, 0, -1);
			iconsProperties[i] = new Vector4(0, 0, 0, 0);
		}

		UpdateProperties();
	}
	
	// Update is called once per frame
	void Update () {

		for (int i = 0; i < iconsCoordinates.Length &&  i < targets.Length ; i++)
		{
			iconsCoordinates[i] = new Vector4(targets[i].getX(), targets[i].getY(), targets[i].getZ(), targets[i].getW());
		}
		//image.GetModifiedMaterial(image.material).SetVectorArray("_Targets", iconsCoordinates);
		renderer.sharedMaterial.SetVectorArray("_Targets", iconsCoordinates);
		
	}

	void UpdateProperties()
	{
		for (int i = 0; i < iconsCoordinates.Length && i < targets.Length; i++)
		{
			int[] atlasCoordinates = atlasInts[(int)targets[i].type];
			iconsProperties[i] = new Vector4(atlasCoordinates[0], atlasCoordinates[1], targets[i].scale, 0);
		}
		//image.GetModifiedMaterial(image.material).SetVectorArray("_TargetsProperties", iconsProperties);
		renderer.sharedMaterial.SetVectorArray("_TargetsProperties", iconsProperties);
	}

	private void OnEnable()
	{
		if(iconsCoordinates != null) UpdateProperties();
	}
}
