﻿Shader "Custom/MiniMap" {

    Properties {
        [Header(Icon)]
        _IconsAtlas ("Icons Atlas", 2D) = "white" { }
        [IntRange] _IconImageX("Icon Image X",  Range (0, 255)) = 1
        [IntRange] _IconImageY("Icon Image Y",  Range (0, 255)) = 1
        _Scale("Icon Scale", Float) = 1
        
        [Header(Target)]
        _WorldSize("WorldSize", Vector) = (0, 0, 0, 0)
        _Offset("Offset", Vector) = (0, 0, 0, 0)
        _Position("Position", Vector) = (0, 0, 0, 0)
        
       [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
       [Header(Standart Shader)] _Color ("Tint", Color) = (1,1,1,1)
     
        _StencilComp ("Stencil Comparison", Float) = 8
        _Stencil ("Stencil ID", Float) = 0
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255
 
        _ColorMask ("Color Mask", Float) = 15       
    }

	SubShader {
		Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }
		
		Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass [_StencilOp]
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }
        
        Cull Off
        Lighting Off
        ZWrite Off
        ZTest [unity_GUIZTestMode]
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask [_ColorMask]

		Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
 
            #include "UnityCG.cginc"
            #include "UnityUI.cginc"
         
            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };
 
            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                half2 texcoord  : TEXCOORD0;
                float4 worldPosition : TEXCOORD1;
            };
         
            fixed4 _Color;
            fixed4 _TextureSampleAdd;
 
            bool _UseClipRect;
            float4 _ClipRect;
 
            bool _UseAlphaClip;
 
            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.worldPosition = IN.vertex;
                OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);
 
                OUT.texcoord = IN.texcoord;
             
                #ifdef UNITY_HALF_TEXEL_OFFSET
                OUT.vertex.xy += (_ScreenParams.zw-1.0)*float2(-1,1);
                #endif
             
                OUT.color = IN.color * _Color;
                return OUT;
            }
 
            sampler2D _MainTex;
            sampler2D _IconsAtlas;
            float _Scale;
            int _IconImageX;
            int _IconImageY;
            
            float4 _Position;
            float4 _WorldSize;
            float4 _Offset;
 
            fixed4 frag(v2f IN) : SV_Target
            {
                
                // 1 / 64 - 1 pix;  32/64 - pix in icon
                float  iconSize = 32.0 / 64.0;
                float2 bounds = float2(iconSize, iconSize) * _Scale;
                
                float2 center = float2((_WorldSize.x/2 /*+ _Offset.x*/) / _WorldSize.x,
                                       (_WorldSize.y/2 /*+ _Offset.z*/) / _WorldSize.y);
                
                float2 position = float2((_Position.x + _Offset.x) / (_WorldSize.x * 2),
                                         (_Position.z + _Offset.z) / (_WorldSize.y * 2));
                position += center;                                             
                                         
                IN.texcoord -= (center - position);
    
                fixed ang = _Position.y; 
                fixed resultX = position.x + (IN.texcoord.x-position.x) * cos(ang) - (IN.texcoord.y-position.y) * sin(ang);
                fixed resultY = position.y + (IN.texcoord.x-position.x) * sin(ang) + (IN.texcoord.y-position.y) * cos(ang);
              
                position -= bounds / 2; 
                
                half4 icolor = tex2D(_IconsAtlas, ((float2(resultX, resultY) - position) / _Scale + 
                                                    float2(_IconImageX, _IconImageY) * iconSize) );
                float is_icon = step(position.x, resultX) * step(resultX, (position + bounds).x) *
                                step(position.y, resultY) * step(resultY, (position + bounds).y) * icolor.a;   
                 
                             
                half4 color = (tex2D(_MainTex, IN.texcoord) + _TextureSampleAdd) * IN.color; 
                color = color * (1 - is_icon) + icolor * is_icon;
                
                if (_UseClipRect){ color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect); }
                if (_UseAlphaClip){ clip (color.a - 0.001); }
               
                return color;
            }
        ENDCG
        }
	}
	FallBack "Diffuse"
}

 