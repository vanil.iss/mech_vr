﻿Shader "Custom/MiniMap2" {

    Properties {
        _MainTex ("Sprite Texture", 2D) = "white" {}
        
        [Header(Icon)]
        _IconsAtlas ("Icons Atlas", 2D) = "white" { }
        
        [Header(Target)]
        _WorldSize("WorldSize", Vector) = (0, 0, 0, 0)
        _Offset("Offset", Vector) = (0, 0, 0, 0)
        [Header(Follow)]
        [IntRange] _FollowIndex("Follow Index",  Range (0, 255)) = 0     
    }

	SubShader {
		Tags
        {
            "Queue"="Geometry"
            "IgnoreProjector"="True"
            "RenderType"="Opaque"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }
        
        Cull Back
        Lighting Off
        ZWrite Off

		Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
         
            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };
 
            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                half2 texcoord  : TEXCOORD0;
                float4 worldPosition : TEXCOORD1;
            };
 
            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.worldPosition = IN.vertex;
                OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color;
                return OUT;
            }
 
            sampler2D _MainTex;
            sampler2D _IconsAtlas;
            // 1 / 64 - 1 pix;  32/64 - pix in icon
            static const float iconSize = 32.0 / 64.0;
            
            
            static const int MaxCount = 256;
            half4 _Targets[MaxCount];
            half4 _TargetsProperties[MaxCount];
            half4 _WorldSize;
            half4 _Offset;
            
            float _FollowIndex;
 
            fixed4 frag(v2f IN) : SV_Target
            {
                    //follow
                    float2 center =         float2(0.5, 0.5);
                    float2 followPosition = float2((_Targets[_FollowIndex].x + _WorldSize.x + _Offset.x) / (_WorldSize.x * 2),
                                                   (_Targets[_FollowIndex].z + _WorldSize.y + _Offset.z) / (_WorldSize.y * 2));  
                                                                       
                
                half4 color = (tex2D(_MainTex, IN.texcoord)) * IN.color;
                IN.texcoord -= (center - followPosition);
                
                for(int i = 0; i < MaxCount; i++){
                    // _TargetsProperties.z is for scale, xy - icon numbers
                    float _Scale = _TargetsProperties[i].z * 0.005;
                    float _IconImageX = _TargetsProperties[i].x;
                    float _IconImageY = _TargetsProperties[i].y;
                    
                    float2 bounds = float2(iconSize, iconSize) * _Scale;
                    
                    float2 position = float2((_Targets[i].x + _WorldSize.x + _Offset.x) / (_WorldSize.x*2),
                                             (_Targets[i].z + _WorldSize.y + _Offset.z) / (_WorldSize.y*2));
                    //position -= (center - followPosition);
                    
                    fixed ang = _Targets[i].y; 
                    fixed resultX = position.x + (IN.texcoord.x-position.x) * cos(ang) - (IN.texcoord.y-position.y) * sin(ang);
                    fixed resultY = position.y + (IN.texcoord.x-position.x) * sin(ang) + (IN.texcoord.y-position.y) * cos(ang);
              
                    position -= bounds / 2;
                    half4 icolor = tex2D(_IconsAtlas, ((float2(resultX, resultY) - position) / _Scale + 
                                                        float2(_IconImageX, _IconImageY) * iconSize) );
                    float is_icon = step(position.x, resultX) * step(resultX, (position + bounds).x) *
                                    step(position.y, resultY) * step(resultY, (position + bounds).y) * icolor.a;
                    
                    color = color * (1 - is_icon) + icolor * is_icon;
                }  
               
                return color;
            }
        ENDCG
        }
	}
	FallBack "Diffuse"
}

 