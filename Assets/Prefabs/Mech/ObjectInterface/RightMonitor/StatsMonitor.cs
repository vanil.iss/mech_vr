﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsMonitor : MonoBehaviour
{
	private int colorRange = 100;
	private int[][] colorOffsets =
	{
		new int[] {9, 14},
		new int[] {2, 13},
		new int[] {3, 10},
		new int[] {0, 8},
		new int[] {1, 7}
	};
	
	[Serializable]
	public class MechPartIcon
	{
		public SpriteRenderer sprite;
		public MechPartType type;
		public MechPartState colorIndex;
	}

	//[SerializeField] private MechAnimator mech;
	[SerializeField] private MechPartIcon[] parts = {};

	private bool is_shield_work = false;
	private int shield_border_offset = 1;
	
	// Use this for initialization
	void Start ()
	{
		UpdateScreen();
	}

	public void Blink(MechPartType type)
	{
		foreach (MechPartIcon part in parts)
		{
			if(part.type.Equals(type))StartCoroutine(BlinkIcon(part));
		}
	}

	public void BlinkAll()
	{
		foreach (MechPartIcon part in parts)
		{
			StartCoroutine(BlinkIcon(part));
		}
	}

	public void SetValue(MechPart part)
	{
		foreach (MechPartIcon partIcon in parts)
		{
			if (partIcon.type.Equals(part.type)) { partIcon.colorIndex = part.state; }
		}
		UpdateScreen();
	}

	public void SetShieldValue(bool is_shield_work)
	{
		this.is_shield_work = is_shield_work;
		UpdateScreen();
	}

	public void UpdateScreen()
	{
		for (int i = 0; i < parts.Length; i++)
		{
			int[] colorOffset = colorOffsets[(int) parts[i].colorIndex];
			parts[i].sprite.material.SetFloat("_ColorOffsetBorder", is_shield_work ? shield_border_offset : colorOffset[0]);
			parts[i].sprite.material.SetFloat("_ColorOffsetFill", colorOffset[1]);
		}
	}

	private IEnumerator BlinkIcon(MechPartIcon part)
	{
		MechPartState _colorIndex = part.colorIndex;

		for (int i = 0; i < 5; i++)
		{
			part.colorIndex = MechPartState.Red;
			UpdateScreen();
			yield return new WaitForSeconds(0.1f);
			
			part.colorIndex = _colorIndex;
			UpdateScreen();
			yield return new WaitForSeconds(0.1f);
		}
	}
}
