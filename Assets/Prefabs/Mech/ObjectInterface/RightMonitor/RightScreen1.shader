﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/RightScreen1" {
	 Properties
    {
        _MainTex ("Sprite Texture", 2D) = "white" {}
        _ColorRamp ("Colour Palette", 2D) = "gray" {}
        _ControlColor ("Control Color", Color) = (104,169,65,1)
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }

    SubShader
    {
        Tags
        { 
            "Queue"="Transparent" 
            "IgnoreProjector"="True" 
            "RenderType"="Transparent" 
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Fog { Mode Off }
        Blend One OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile DUMMY PIXELSNAP_ON
            #include "UnityCG.cginc"

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                half2 texcoord  : TEXCOORD0;
            };

            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color;
                #ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif

                return OUT;
            }

            sampler2D _MainTex;
            sampler2D _ColorRamp;
            float _ColorOffsetBorder;
            float _ColorOffsetFill;
            
            fixed4 _ControlColor;
            float _CutOff;
            
            static const float pixelSize = 1.0 / 16.0;
            static const float pixelSize2 = 1.0 / 512.0;
            static const int MaxCount = 6;
            half4 _Parts[MaxCount];
            half4 _PartsProperties[MaxCount];

            fixed4 frag(v2f IN) : SV_Target
            {
                fixed4 c = tex2D(_MainTex, IN.texcoord);
                float is_fill = step(_ControlColor.rgb, c.rgb);
                
                for(int i = 0; i < MaxCount; i++){
                    
                    float is_part = step(_Parts[i].x, IN.texcoord.x) * 
                                    step(IN.texcoord.x, _Parts[i].z) *
                                    step(_Parts[i].y, IN.texcoord.y) * 
                                    step(IN.texcoord.y, _Parts[i].w);
                                    
                    c.rgb    = is_fill  * tex2D(_ColorRamp, float2(_PartsProperties[i].x * pixelSize, 1)) + 
                          (1 - is_fill) * tex2D(_ColorRamp, float2(_PartsProperties[i].y * pixelSize, 1));
                }
                
                if(c.a < 1) discard;
                return c;
            }
        ENDCG
        }
    }
	FallBack "Diffuse"
}
