﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/RightScreen" {
	 Properties
    {
        _MainTex ("Sprite Texture", 2D) = "white" {}
        _ColorRamp ("Colour Palette", 2D) = "gray" {}
        [IntRange] _ColorOffsetBorder("Color Offset Border", Range(0,15)) = 0
        [IntRange] _ColorOffsetFill("Color Offset Fill", Range(0,15)) = 0
        
        _ControlColor ("Control Color", Color) = (104,169,65,1)
        
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }

    SubShader
    {
        Tags
        { 
            "Queue"="Transparent" 
            "IgnoreProjector"="True" 
            "RenderType"="Transparent" 
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Fog { Mode Off }
        Blend One OneMinusSrcAlpha

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile DUMMY PIXELSNAP_ON
            #include "UnityCG.cginc"

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                half2 texcoord  : TEXCOORD0;
            };

            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color;
                #ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif

                return OUT;
            }

            sampler2D _MainTex;
            sampler2D _ColorRamp;
            float _ColorOffsetBorder;
            float _ColorOffsetFill;
            
            fixed4 _ControlColor;
            float _CutOff;

            fixed4 frag(v2f IN) : SV_Target
            {
                
                //float spriteAlpha = tex2D(_MainTex, IN.texcoord).a;
                //
                // 1/width = 1/16pix = 0.0625 == 1 pixel size for color offset
                //fixed4 c = tex2D(_ColorRamp, float2(transparency * _ColorOffset * 0.0625, 1)) ;
                float pixelSize = 1.0 / 16.0;
                fixed4 c = tex2D(_MainTex, IN.texcoord);
                
                float transparency = step(0.1, c.a);
                
                float is_fill = step(_ControlColor.rgb, c.rgb);
                
                fixed4 color = is_fill * tex2D(_ColorRamp, float2(transparency * _ColorOffsetFill * pixelSize, 1)) + 
                (1 - is_fill) * tex2D(_ColorRamp, float2(transparency * _ColorOffsetBorder * pixelSize, 1));

                c.rgb = color.rgb;
                clip (c.a - 0.001);
                //if(c.a < 1) discard;
                return c;
            }
        ENDCG
        }
    }
	FallBack "Diffuse"
}
