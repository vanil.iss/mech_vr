﻿Shader "Custom/HologramShader" {
	Properties {
	    _MainTex ("Main Texture", 2D) = "white" {}
		_MainColor ("Main Color", Color) = (1,1,1,1)
		_Alpha ("Alpha", Range (0.0, 1.0)) = 1.0
		_Brightness("Brightness", Range(0.1, 6.0)) = 3.0
		
		_TrackHeight ("Track Height", Float) = 0.05
		_TrackTiling ("Track Tiling", Float) = 0.05
		_TrackSpeed ("Track Speed", Float) = 1.0
		
		_RimColor ("Rim Color", Color) = (1,1,1,1)
		_RimPower ("Rim Power", Float) = 0.05
	}
	SubShader {
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 100
		ColorMask RGB
        Cull Back
        
        Pass{
            CGPROGRAM
            #pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			struct vertInput {
			    float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
            };  
 
            struct vertOutput {
                float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 worldVertex : TEXCOORD1;
				float3 viewDir : TEXCOORD2;
				float3 worldNormal : NORMAL;
            };
            
            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _MainColor;
			float _Alpha;
			float _Brightness;
			
			float _TrackHeight;
			float _TrackTiling;
		    float _TrackSpeed;
			
			float4 _RimColor;
			float _RimPower;
			
			vertOutput vert(vertInput v) {
                vertOutput o;
                
                o.vertex = UnityObjectToClipPos(v.vertex);
				
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.worldVertex = mul(unity_ObjectToWorld, v.vertex);
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				o.viewDir = normalize(UnityWorldSpaceViewDir(o.worldVertex.xyz));
                return o;
            }
 
            fixed4 frag(vertOutput o) : SV_Target {
                fixed4 texColor = tex2D(_MainTex, o.uv);
                
                float track = 0.0;
                track = step(frac(o.uv.y * _TrackTiling + _Time.w * _TrackSpeed), _TrackHeight) * 0.65;
                
				half rim = saturate( dot(o.viewDir, o.worldNormal) * _Alpha );
				fixed4 rimColor = _RimColor * pow (track, _RimPower);
				
				fixed4 col = texColor * _MainColor + rimColor;
				col.a = texColor.a * (rim * track);
				col.rgb *= _Brightness;
				return col; 
            }
			
            ENDCG
        }
	}
	FallBack "Diffuse"
}
