﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TankExtra : MonoBehaviour
{


	private NavMeshAgent agent;
	private Animator animator;
	private ObjectHealth health;

	public Transform[] points;
	public Transform target;

	int pos;

	float distanceToPoint, _distanceToPoint;
	// Use this for initialization
	void Start()
	{
		agent = GetComponent<NavMeshAgent>();
		health = GetComponent<ObjectHealth>();

		pos = 0;

		agent.speed = Random.Range(35, 55);
		agent.angularSpeed = Random.Range(240, 300);

		distanceToPoint = Random.Range(10, 25);

		target = points[pos];
	}
	
	// Update is called once per frame
	void Update()
	{
		_distanceToPoint = Vector3.Distance(transform.position, target.position);
	
		if (_distanceToPoint <= distanceToPoint)
		{
			pos += 3;
			if (pos == points.Length)
				pos = 0;

			agent.speed = Random.Range(25, 45);
			agent.angularSpeed = Random.Range(200, 300);

			distanceToPoint = Random.Range(5, 15);

			target = points[pos + Random.Range(0, 2)];
		}
			

		agent.SetDestination(target.position);
	}
}
