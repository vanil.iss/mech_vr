﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HipperCubeScript : MonoBehaviour
{

	private GameObject player;

	public GameObject cube1;
	public GameObject cube2;
	public GameObject cube3;
	public GameObject cube4;
	public GameObject cube5;

	public Transform[] points;
	private Transform target;

	private Vector3 speedVector;
	public float speed;
	// Use this for initialization
	void Start()
	{
		player = GameObject.FindWithTag("Player");

		target = points[Random.Range(0, points.Length)];
	}
	
	// Update is called once per frame
	void Update()
	{
		if (Vector3.Distance(transform.position, target.position) < 5f)
		{
			target = points[Random.Range(0, points.Length)];
		}

		if (transform.position.y < target.position.y)
		{
			speedVector = new Vector3(0, speed, 0);
			transform.position += speedVector;
		}
		else if (transform.position.y > target.position.y)
		{
			speedVector = new Vector3(0, -speed, 0);

			transform.position += speedVector;
		}

		if (transform.position.x < target.position.x)
		{
			speedVector = new Vector3(speed, 0, 0);

			transform.position += speedVector;
		}
		else if (transform.position.x > target.position.x)
		{
			speedVector = new Vector3(-speed, 0, 0);

			transform.position += speedVector;
		}
	}
}
