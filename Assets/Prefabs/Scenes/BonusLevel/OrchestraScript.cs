﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class OrchestraScript : MonoBehaviour
{
	private NavMeshAgent agent;
	private Animator animator;
	private GameObject player;
	private ObjectHealth health;

	public Transform[] points;
	private Transform target;

	bool allReady, enemyFind, death;
	// Use this for initialization
	void Start()
	{
		agent = GetComponent<NavMeshAgent>();
		animator = GetComponent<Animator>();
		player = GameObject.FindWithTag("Player");
		health = GetComponent<ObjectHealth>();

		target = points[Random.Range(0, points.Length)];

		//allReady = false;
		enemyFind = false;
		death = false;

		//StartCoroutine(ready());
	}
	
	// Update is called once per frame
	void Update()
	{
		if (health.health > 0)
		{
			if (!enemyFind)
			{
				if (Vector3.Distance(player.transform.position, transform.position) < 100f)
					enemyFind = true;
			}

			if (enemyFind)
			{
				animator.SetTrigger("Running");
				agent.SetDestination(target.position);

				if (Vector3.Distance(target.position, transform.position) < 10f)
					target = points[Random.Range(0, points.Length)];
			}
			else
			{
				animator.SetTrigger("Shooting");
				animator.SetTrigger("Pokoy");
			}
		}
		else
		{
			if (!death)
			{
				animator.SetTrigger("Death");

				agent.enabled = false;
				GetComponent<CapsuleCollider>().enabled = false;

				death = false;
			}
		}
	}

	IEnumerator ready()
	{
		yield return new WaitForSecondsRealtime(3f);

		allReady = true;
	}
}
