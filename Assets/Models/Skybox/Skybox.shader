﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "MechVR/Skybox" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue"="Background" "RenderType"="Background" }
		//Cull Off ZWrite Off Fog { Mode Off }
        Pass {
                CGPROGRAM
                #pragma vertex vert  
                #pragma fragment frag 
        
                sampler2D _MainTex;
        
                struct vertexInput {
                    float4 vertex : POSITION;
                    float3 texcoord : TEXCOORD0;
                 };
                 struct vertexOutput {
                    float4 vertex : SV_POSITION;
                    float3 texcoord : TEXCOORD0;
                 };
                 
                 vertexOutput vert(vertexInput input) 
                 {
                    vertexOutput output;
         
                    output.vertex = UnityObjectToClipPos(input.vertex);
                    output.texcoord = input.texcoord;
                    
                    return output;
                 }
         
                 float4 frag(vertexOutput input) : COLOR
                 {
                    return tex2D (_MainTex, input.texcoord);
                 }
        
                ENDCG
            }
	}
	FallBack "Diffuse"
}
