﻿using UnityEngine;

public class MiniGame1Manager : MonoBehaviour
{

    public Transform myParent;

    public GameObject[] details;

    public PartScript[] parts;

    public Transform[] spawnPoints;

    public GameObject sceneCamera;

    private GameObject catchedGameObject;

    private int[] detailsBroken;

    public int minimumDetailBrake = 0;
    public int maximumDetailBrake = 3;

    public int limitOfDetails = 10;

    // Use this for initialization
    void Start()
    {
        maximumDetailBrake++; //Because in Unity's random function maximum is supremum :-/
        detailsBroken = new int[details.Length];
        foreach (var part in parts)
        {
            part.InitPartScript(details.Length);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Keyboard uses for a testing
        if (Input.GetKeyDown("q")) BreakAnother();
        if (Input.GetKeyDown("e")) CatchIt();
    }

    void BreakAnother()
    {
        foreach (var part in parts)
        {
            if (Random.Range(0, 2) == 1)
            {
                for (var i = 0; i < part.NeedDetails.Length; i++)
                {
                    var breakDetail = Random.Range(minimumDetailBrake, maximumDetailBrake);
                    //if quantity of detail is too more on part, just fill it as possible
                    breakDetail = breakDetail + part.NeedDetails[i] > limitOfDetails ? limitOfDetails - part.NeedDetails[i] : breakDetail;
                    part.NeedDetails[i] += breakDetail;
                    detailsBroken[i] += breakDetail;
                }
            }
        }
        for (var i = 0; i < detailsBroken.Length; i++)
        {
            while(detailsBroken[i] > 0)
            {
                var spawnPointIndex = Random.Range(0, spawnPoints.Length);
                var o = Instantiate(details[i], spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
                o.transform.parent = myParent;
                detailsBroken[i]--;
            }
        }
    }

    void CatchIt()
    {
        if (sceneCamera.transform.childCount == 1) //WARNING!!! If you will change the prefab, you will need to change this value too!
        {
            catchedGameObject.transform.parent = myParent;
            catchedGameObject.GetComponent<Rigidbody>().isKinematic = false;
            return;
        }
        RaycastHit hit;
        if (Physics.Raycast(sceneCamera.transform.position, sceneCamera.transform.forward, out hit))
        {
            catchedGameObject = hit.collider.gameObject;
            if (catchedGameObject.tag == "Detail")
            {
                catchedGameObject.transform.parent = sceneCamera.transform;
                catchedGameObject.GetComponent<Rigidbody>().isKinematic = true;
            }
        }
    }
}
