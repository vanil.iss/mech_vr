﻿using System;
using UnityEngine;

public class PartScript : MonoBehaviour {

    public int[] NeedDetails { get; set; }

    public TextMesh[] texts;

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        //For test
        for (int i = 0; i < NeedDetails.Length; i++)
        {
            texts[i].text = NeedDetails[i].ToString();
        }
	}

    //Calls by MiniGame1Manager
    public void InitPartScript(int quantityOfParts)
    {
        NeedDetails = new int[quantityOfParts];
    }

    //Calls by Unity
    private void OnTriggerEnter(Collider collisionObject)
    {
        if (collisionObject.gameObject.tag == "Detail")
        {
            //Is there any ideas, how optimize this?
            var numOfDetail = Int32.Parse(collisionObject.gameObject.name[6].ToString() + collisionObject.gameObject.name[7].ToString());
            if (NeedDetails[--numOfDetail] == 0) return;  //What is it? It's unreadable code :D
            NeedDetails[numOfDetail]--;
            Destroy(collisionObject.gameObject);
        }
    }
}
