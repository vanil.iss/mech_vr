﻿Shader "MechVR/SpriteShader" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_Detail ("Detail", 2D) = "grey" {}
	}
	SubShader {
		Tags { "RenderMode" = "Opaque" }
		CGPROGRAM
		#pragma surface surf Lambert
		struct Input {
			float2 uv_MainTex;
			float4 screenPos;
		};
		sampler2D _MainTex;
		sampler2D _Detail;
		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
			float2 screenUV = IN.screenPos.xy / IN.screenPos.w;
			screenUV *= float2(10,10);
			o.Albedo *= tex2D (_Detail, screenUV).rgb * 2;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
