﻿Shader "MechVR/JustWhiteWithLighting" {
	SubShader {
		Tags { "RenderType" = "Opaque" } //Can be deleted
		CGPROGRAM
		#pragma surface surf Lambert
		struct Input {
			float4 color : COLOR;
		};
		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = 1; //can be: o.Albedo = IN.color;
		}
		ENDCG
	}
	Fallback "Diffuse"
}
