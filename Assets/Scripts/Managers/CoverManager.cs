﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoverManager {
    
    private static CoverManager instance;
    public static CoverManager Instance
    {
        get
        {
            if (instance != null)
                return instance;

            return instance = new CoverManager();

        }
        private set { }
    }

   
    public List<Cover> coversList;

    
    public void Init()
    {        
        coversList = new List<Cover>(GameObject.FindObjectsOfType<Cover>());
    }
    
    public void deleteCover(Cover cover)
    {
        coversList.Remove(cover);

    }
}
