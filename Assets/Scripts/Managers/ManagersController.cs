﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagersController : MonoBehaviour
{
	public uint maxCountHedgehopper;
	public uint maxCountElectric;
	public uint maxCountKamikaze;
	public uint maxCountTank;

		// Use this for initialization
	private void Awake()
	{
		MobPoolManager.Instance.Init(maxCountHedgehopper, maxCountElectric, maxCountKamikaze, maxCountTank);
		//	CoverManager.Instance.Init();

	}
}
