﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public enum UniqueAttackType
{
    GRENADE,
    EMI
}

[Serializable]
public class UniqueAttack
{
    public UniqueAttackType name;

    public float reloadTime;
     public float _reloadTime;


    public int countAttack;
     public int _countAttack;

    [Header("Timer attack not work")] public bool isHellMode;
}

public class UniqueAttackManager : MonoBehaviour
{
    public UniqueAttack[] attacks;

    private static UniqueAttackManager instance;

    public static UniqueAttackManager Instance
    {
        get { return instance; }
        private set { }
    }

    private void Awake()
    {
        instance = this;
        
    }    

    public float timeUpdate = 5f;
    private float _timeUpdate;

    private void Update()
    {
        if (_timeUpdate >= timeUpdate)
        {
            foreach (var attack in attacks)
                UpdateAttack(attack);
            _timeUpdate = 0;

        }
        else
            _timeUpdate += Time.deltaTime;
    }

    private void UpdateAttack(UniqueAttack attack)
    {
        if (attack._countAttack >= attack.countAttack) return;

        if (attack._reloadTime >= attack.reloadTime)
        {
            attack._reloadTime = 0;
            attack._countAttack++;
        }
        else
            attack._reloadTime += _timeUpdate;
    }

    public bool AbilityUseAttack(UniqueAttackType name)
    {
        var count = attacks.First(item => item.name == name)._countAttack;

        if (count <= 0) return false;
       
        attacks.First(item => item.name == name)._countAttack--;
        return true;
    }
}