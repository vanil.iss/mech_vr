﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobPoolManager
{

    private static MobPoolManager instance;
    public static MobPoolManager Instance
    {
        get
        {
            if (instance != null)
                return instance;

            return instance = new MobPoolManager();

        }
        private set { }
    }

    private GameObject hedgehopperMob;
    private GameObject electricMob;
    public GameObject kamikazeMob;
    public GameObject tankMob;


    public GameObjectPool hedgehopperMobPool;
    public GameObjectPool electricMobPool;
    public GameObjectPool kamikazeMobPool;
    public GameObjectPool tankMobPool;

    public List<GameObject> mobList;

    public void Init(uint maxCountHedgehopper, uint maxCountElectric, uint maxCountKamikaze, uint maxCountTank)
    {
        mobList = new List<GameObject>();
        const string pathMobs = "Prefabs/Mobs/";
        
        hedgehopperMob = Resources.Load<GameObject>(pathMobs + "HedgehopperMob");
        electricMob = Resources.Load<GameObject>(pathMobs + "ElectricMob");
        kamikazeMob = Resources.Load<GameObject>(pathMobs + "KamikazeMob");
        tankMob = Resources.Load<GameObject>(pathMobs + "TankMob");

        InitPool(out hedgehopperMobPool, hedgehopperMob,  maxCountHedgehopper, "Hedgehopper");     
        InitPool(out electricMobPool, electricMob, maxCountElectric, "Electric");        
        InitPool(out kamikazeMobPool, kamikazeMob, maxCountKamikaze, "Kamikaze");
        InitPool(out tankMobPool, tankMob, maxCountTank, "Tank");
    }

    private void InitPool(out GameObjectPool pool, GameObject prefab, uint maxCount, string name)
    {
        pool = new GameObjectPool(prefab, maxCount, target => target.name = name, true);
        pool.prePopulate(checked((int) maxCount));
    }
    
    public void deleteMob(GameObject mob)
	{
		mobList.Remove(mob);
	}


}
