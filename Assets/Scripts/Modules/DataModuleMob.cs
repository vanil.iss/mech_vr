﻿using System.Collections;
using System.Collections.Generic;
using Unit;
using Unit.Mob;
using UnityEngine;
using UnityEngine.AI;

public class DataModuleMob : MonoBehaviour
{
    public MobState currentState = MobState.Wait;


    #region MyRegion

    public SimpleMovingModule movModule;
    public SimpleShotingModule shotModule;

    #endregion

    #region Cash

    public Animator animator;
    public Collider mobCollider;
    public NavMeshAgent agent;
    public AudioSource source;

    #endregion

    #region Moving

    public Transform target;
    public float distToTarget;
    float _distToTarget;

    #endregion

    #region Shooting

    public Transform facePoint;
    private RaycastHit hit;

    public float rateOfSpeed;

    public DamageType damageType;

    float _rateOfSpeed;
    float _animTimeBeforeHit;

    #endregion

//	protected float _timeRegeneration;
}