using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unit.Mob;

public class SimpleMovingModule : MonoBehaviour {

	public Transform target;

	protected float _distToTarget;
	public float distToTarget = 25f;


	public virtual void Init(){
	
	}

	public virtual void Activate(){

	}

	public virtual void UpdateModule (DataModuleMob data){

	}

	public virtual bool ChangeIf(DataModuleMob data){
		return false;
	}


//	public MobState getNewState(){
//		return state;
//	}
}
