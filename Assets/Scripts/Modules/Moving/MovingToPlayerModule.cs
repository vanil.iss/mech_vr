using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unit.Mob;

public class MovingToPlayerModule : SimpleMovingModule {


	public virtual void UpdateModule (DataModuleMob data){
//		_animTimeBeforeHit = 0f;
	}


	public virtual void Activate(){
//		agent.enabled = true;
	}

	public virtual bool ChangeIf(DataModuleMob data){
		if (getDistToTarget () < distToTarget) {
			if (LookAtSomething ()) {
//				state = MobState.Shoot;
//				agent.enabled = false;
				return true;
			}
		}
		return false;
	}

	private bool LookAtSomething()
	{
		return false;
//		return Physics.Raycast(facePoint.position, facePoint.transform.forward, out hit, 1000f) 
//			&& hit.transform.gameObject.tag.Equals("Player");
	}

	public float getDistToTarget(){
		return Vector3.Distance(transform.position, target.transform.position);
	}

}
