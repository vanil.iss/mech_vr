﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unit.Mob;

public class SimpleShotingModule : MonoBehaviour {

	public Transform target;

	public int damage;

	protected float _distToTarget;
	public float distToTarget = 25f;

	protected MobState state = MobState.Wait;

	public virtual void UpdateModule (){

	}

	public virtual bool ChangeIf(){
		return false;
	}

	public MobState getNewState(){
		return state;
	}
}
