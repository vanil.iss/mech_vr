﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonRotation : MonoBehaviour {

	[SerializeField]
	public float turnSpeed = 15f;


	public void RightRotation(){
		transform.Rotate(Vector3.up, -turnSpeed * Time.deltaTime);
		//camera.transform.Rotate(Vector3.up, -turnSpeed * Time.deltaTime);
		//camera.transform.localRotation = transform.rotation;
	}

	public void LeftRotation(){
		transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime);
		//camera.transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime);
		//camera.transform.localRotation = transform.rotation;
	}
}
