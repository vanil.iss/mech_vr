﻿using UnityEngine;
using System.Collections;


public class CrossHair : MonoBehaviour {
	
	public Texture2D crossHairTexture;
	public Rect position;

	// Use this for initialization
	void Start () {
		position = new Rect ((Screen.width - crossHairTexture.width) / 2,
			(Screen.height - crossHairTexture.height) / 2,
			crossHairTexture.width, crossHairTexture.height);
	}
	void OnGUI(){
		GUI.DrawTexture (position, crossHairTexture);
	}
}