﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildDestroying : MonoBehaviour
{

	private ObjectHealth objectHealth;
	private int health;
	private Animator buildAnimator;


	private GameObject mech;
	Transform mechTransform;
	float distance;
	HealthMech mechHealth;


	// Use this for initialization
	void Start ()
	{
		objectHealth = (ObjectHealth)GetComponent ("ObjectHealth");
		//health = objectHealth.health;

		buildAnimator = GetComponent<Animator> ();
		mech = GameObject.Find ("Mech");
		mechTransform = mech.transform;
		mechHealth = (HealthMech)mech.GetComponent<HealthMech> ();
	}
	// Update is called once per frame
	void Update ()
	{
		health = objectHealth.health;
		//Debug.Log ("Health:" + health);
		if (health == 0) {
			buildAnimator.SetTrigger ("LowHealthTrigger");
			GetComponent<BoxCollider> ().enabled = false;
			health -= 1;
			Destroy (gameObject, 4f);
		}
		distance = Vector3.Distance (transform.position, mech.transform.position);
		//Debug.Log("" + distance);
		if (distance < 121f) {
			//конец игры сюда плиз
			mechHealth.health = 0;
		}



	}
}

