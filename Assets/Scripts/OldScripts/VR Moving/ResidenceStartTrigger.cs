﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResidenceStartTrigger : MonoBehaviour {

    ObjectHealth objHealth;
    GameObject jumpingMech;
    GameObject player;

    Vector3 target_Pos;
    Vector3 inital_Pos;
    float inital_distance;
    private float speed_move = 2000f;
    private float rotationSpeed = 400f;

    private bool isDist;
    // Use this for initialization
    void Start () { 

        objHealth =(ObjectHealth) GetComponent<ObjectHealth>();
        jumpingMech = (GameObject)GameObject.Find("JumpingMech");
        player = (GameObject)GameObject.Find("Mech");
        inital_Pos = player.transform.position;
        inital_distance = Vector3.Distance(inital_Pos, jumpingMech.transform.position);

    }
	
	// Update is called once per frame
	void Update () {

        if(objHealth.health<=0)
        {
            //позиция точки
            target_Pos = player.transform.position;
            //SetRotation(points[i].transform.position);
            jumpingMech.transform.Translate(Vector3.Normalize(target_Pos - jumpingMech.transform.position) * Time.deltaTime * speed_move);
            //вычисляем расстояние до точки
            float distance = Vector3.Distance(target_Pos, jumpingMech.transform.position);

            //float parabola = -jumpingMech.transform.position.x * jumpingMech.transform.position.x - (jumpingMech.transform.position.z + 3000) * (jumpingMech.transform.position.z + 3000);
            //float parabola = jumpingMech.transform.position.z * jumpingMech.transform.position.z + 20;
            
            //Vector3 parabolaVektor = new Vector3(0, parabola, 0);
            ///if (Vector3.Distance(target_Pos, jumpingMech.transform.position) > inital_distance/2)
            //jumpingMech.transform.Translate(Vector3.Normalize(parabolaVektor - jumpingMech.transform.up) * Time.deltaTime * speed_move);
            //else
             //jumpingMech.transform.Translate(Vector3.Normalize(jumpingMech.transform.up - parabolaVektor) * Time.deltaTime * speed_move);

            if (distance < 30f){
                if (!isDist)
                {
                    isDist = true;
                    StartCoroutine(WaitAnLoad());
                }
            }
        }


        
        
    }


    void SetRotation(Vector3 lookAt)
    {
        Vector3 lookPos = lookAt - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        jumpingMech.transform.rotation = Quaternion.Lerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
    }

    IEnumerator WaitAnLoad() {
        yield return new WaitForSecondsRealtime(2f);
        Application.LoadLevel(5);

    }
}

