﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundList : MonoBehaviour {

	public Dictionary<string,AudioClip> soundDictionary;

	[SerializeField]
	public List<string> keys;
	[SerializeField]
	public List<AudioClip> values;

	AudioSource audioSource;
	void Start(){
		soundDictionary = new Dictionary<string,AudioClip> ();
		for (int i = 0; i < keys.Count; i++) {
			soundDictionary.Add (keys [i], values [i]);
		}

		audioSource = GetComponent<AudioSource> ();
	}

	public void PlaySound(string name){
		audioSource.clip = soundDictionary [name];
		audioSource.volume = 1;
		audioSource.Play();

	}

	public void PlaySound(string name, float volume){
		audioSource.clip = soundDictionary [name];
		audioSource.volume = volume;
		audioSource.Play();

	}
}
