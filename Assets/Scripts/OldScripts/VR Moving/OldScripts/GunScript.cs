﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour {

	public float distance = 100f;
	public float rateOfSpeed = 0.5f;

	private float _rateOfSpeed;

	public Transform metaHit;
	public Transform sparksMetal;

	private RaycastHit hit;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (_rateOfSpeed <= rateOfSpeed) 
			_rateOfSpeed += Time.deltaTime;
		
		if (Input.GetMouseButton (0) && _rateOfSpeed > rateOfSpeed) {
			_rateOfSpeed = 0;

			Vector3 DirectionRay = transform.TransformDirection (Vector3.forward);

			if (Physics.Raycast (transform.position, DirectionRay, out hit, distance)) {
				Quaternion hitRotation = Quaternion.FromToRotation (Vector3.up, hit.normal);

				if (hit.rigidbody)
					hit.rigidbody.AddForceAtPosition(DirectionRay*100, hit.point);

				//if (hit.collider.material.staticFriction == 0.2f) {
					Transform sparksMetalGo = Instantiate (sparksMetal, hit.point + (hit.normal * 0.0001f), hitRotation) as Transform;
				//}

				if (hit.collider) {	
					Transform metaHitGo = Instantiate (metaHit, hit.point + (hit.normal * 0.0001f), hitRotation) as Transform;
					metaHitGo.parent = hit.transform;
					Destroy ((metaHitGo as Transform).gameObject, 3);
				}
				if (hit.collider.tag == "Enemy") {
					Destroy (hit.collider.gameObject);
				}
			}
		} else {
		}
	}
}
