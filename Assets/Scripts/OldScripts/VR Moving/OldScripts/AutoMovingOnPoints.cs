﻿using UnityEngine;
using System.Collections;

public class AutoMovingOnPoints : MonoBehaviour {
	
	//private Transform mechTransform;
	[SerializeField]
	private Transform pointTransform;
	[SerializeField]
	private float moveSpeed = 0.005f;
	private Rigidbody mechRigidbody;

	// Use this for initialization
	void Start () {
		mechRigidbody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		mechRigidbody.transform.position = Vector3.Lerp(mechRigidbody.transform.position, pointTransform.position, moveSpeed);
	}
}
