﻿using UnityEngine;
using System.Collections;

public class AutoMoving : MonoBehaviour
{
	[SerializeField]
	public float moveSpeed = 0.05f;
	//public float startSpeed = 0;
	[SerializeField]
	public float turnSpeed = 15f;

	private Rigidbody playerRigidbody;


	void Start(){
		playerRigidbody = GetComponent<Rigidbody> ();
	}

	void Update ()
	{
		//float X = transform.position.x;
		//float Y =  transform.position.y;
		float Z = transform.position.z;
		Vector3 moveDirection = (transform.forward * Z).normalized;
		Vector3 moveVector = moveDirection * moveSpeed;
		Vector3 movePosition = transform.position + moveVector;
		playerRigidbody.MovePosition(movePosition);

	}
}