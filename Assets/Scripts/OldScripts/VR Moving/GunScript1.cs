﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript1 : MonoBehaviour {

//region ComponentCash
    private Pause pause;
//endregion


    public bool isShooting;


	#region EffectOnGun

	private float startShootingTime = 2f;
	[SerializeField]private float _startShootingTime;

	bool musicPlay;
	private AudioSource source;

	private GameObject sparksGun;
	//private GameObject faceLights;

	#endregion


	#region EffectOnHit

	private RaycastHit hit;
	//Объект в который произведенно попадание

	public float distance = 100f;
	//Допустимая дистанция

	public float rateOfSpeed = 0.5f;
	//Скорость стрельбы
	private float _rateOfSpeed;
	//Время с последнего выстрела

	public Transform metaHit;
	//объект при попадании в цель
	public Transform sparksMetal;
	//Искры при попадании

	#endregion

	StayAndFire stayAndFire;

	[SerializeField] private Transform allocPrefab;

    void Awake() {
        pause = GameObject.FindGameObjectWithTag("Pause").GetComponent<Pause>();

        //faceLights = GameObject.FindGameObjectWithTag("LightGun");
        sparksGun = GameObject.FindGameObjectWithTag("SparksGun");
        source = gameObject.GetComponent<AudioSource>();

    }

	void Start(){
		Cursor.lockState = CursorLockMode.Locked;
		isShooting = false;

		stayAndFire = (StayAndFire) transform.parent.GetComponentInChildren<StayAndFire> ();

	}

	// Update is called once per frame
	void Update(){

		if (pause.isPause == false){
			//Проверка на время последнего выстрела

			if (_rateOfSpeed <= rateOfSpeed)
				_rateOfSpeed += Time.deltaTime;

			if (_startShootingTime > startShootingTime) {
				if (!musicPlay){
					source.Play();
					musicPlay = true;
				}

				if (!Input.GetKey(KeyCode.Mouse0)) {
					source.Stop();
					musicPlay = false;
					_startShootingTime = 0;
					isShooting = false;
				}

				if (_rateOfSpeed > rateOfSpeed){
					isShooting = true;
					sparksGun.GetComponent<Light>().enabled = true;
					//faceLights.GetComponent<Light>().enabled = true;

                    ParticleSystem particleSystem = sparksGun.GetComponent<ParticleSystem>();
//                    LineRenderer lineRenderer = sparksGun.GetComponent<LineRenderer>();

                    particleSystem.Stop();
                    particleSystem.Play();

//                    lineRenderer.SetPosition(0, sparksGun.transform.position);
//                    lineRenderer.SetPosition(0, sparksGun.transform.TransformDirection(Vector3.forward));
//
//                    lineRenderer.enabled = true;

					_rateOfSpeed = 0;
					Vector3 DirectionRay = transform.TransformDirection(Vector3.forward);
					if (Physics.Raycast(transform.position, DirectionRay, out hit, distance)){
						Quaternion hitRotation = Quaternion.FromToRotation(Vector3.up, hit.normal);

						if(hit.collider.tag!="PlayerPart" && stayAndFire.canMove){

						if (hit.rigidbody)
							hit.rigidbody.AddForceAtPosition(DirectionRay * 100, hit.point);
//
						//if (hit.collider.material.staticFriction == 0.2f) {
						//Transform sparksMetalGo = Instantiate(sparksMetal, hit.point + (hit.normal * 0.0001f), hitRotation) as Transform;
						//}
//
						if (hit.collider) {
							Transform metaHitGo = Instantiate(metaHit, hit.point + (hit.normal * 0.0001f), hitRotation) as Transform;
							metaHitGo.parent = hit.transform;
							//metaHitGo.GetComponent<Renderer> ().material.color.a = 0.5f;
							metaHitGo.GetComponent<Collider>().enabled = false;
							Destroy((metaHitGo as Transform).gameObject, 3);
                        }
//
//						//Проверка на существование хит-поинтов у объект
                        ObjectHealth health = hit.collider.gameObject.GetComponent<ObjectHealth>();
						if (health)
							health.activisionHit(1);
//
                        ObjectHealth simpleObj = hit.collider.gameObject.GetComponent<ObjectHealth>();

//						if (hit.collider.gameObject.GetComponent<SimpleHitTest>())
//							hit.collider.gameObject.GetComponent<SimpleHitTest>().isHit = true;
//
//						//if (hit.collider.tag == "Enemy") {
//						//	Destroy (hit.collider.gameObject);
//						//}
						}
					}

				}
				else
				{

					//faceLights.GetComponent<Light>().enabled = false;
					sparksGun.GetComponent<Light>().enabled = false;
					isShooting = false;
					//sparksGun.GetComponent<LineRenderer> ().enabled = false;
				}
			}
			else if (Input.GetKey(KeyCode.Mouse0))
			{
				_startShootingTime += Time.deltaTime;
				isShooting = true;
			}
		}

	}

}
