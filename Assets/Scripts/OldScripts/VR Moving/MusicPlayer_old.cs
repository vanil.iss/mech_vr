﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class MusicPlayer_old : MonoBehaviour {


	public AudioClip[] musicMas;
	int musCount;
	int i = 0;
	int j = 0;
	bool isPause = false;
	bool isPlayPosition = false;

	AudioSource audioSource;

	Canvas musicPlayerCanvas;
	GameObject musicName;
	GameObject volume;
	float currentVolume = 0f;
	bool isVolumeOff = false;

	// Use this for initialization
	void Start () {

		musicMas = Resources.LoadAll<AudioClip>("Music");
		musCount = musicMas.Length;

		musicName = GameObject.Find ("MusicName");
		volume = GameObject.Find ("Volume");
		setText ();

		audioSource = GetComponent<AudioSource> ();
		audioSource.PlayOneShot (musicMas[0]);
		playPause ();

		//volume = transform.Find ("Volume").gameObject;
		audioSource.volume = 0.1f;
		currentVolume = 0.1f;
		setVolumeText ();
		musicName.GetComponent<Text> ().color = new Color(1F, 1F, 1F);
		volume.GetComponent<Text> ().color = new Color (1F, 1F, 1F);



	}
	
	// Update is called once per frame
	void Update () {
		if (!audioSource.isPlaying && !isPause) {
			if (i == musCount - 1) i = - 1;
			i++;
			audioSource.PlayOneShot (musicMas [i]);
			j = i;
			setText ();
		}

		if (i != j)
			isPlayPosition = true;
		else
			isPlayPosition = false;
	}

	public void nextComposition(){
		if (j == musCount - 1) j = - 1;
		j++;
		//audioSource.Stop ();
		//audioSource.PlayOneShot (clipsArray [i]);
		setText ();
	}

	public void previousComposition(){
		if (j == 0) j = musCount;
		j--;
		//audioSource.Stop ();
		//audioSource.PlayOneShot (clipsArray [i]);
		setText ();
	}

	public void playPause(){

		if (isPlayPosition) {
		
			i = j;
			audioSource.Stop ();
			audioSource.PlayOneShot (musicMas [i]);
			isPause = false;
			setText ();
		
		} else {
			
			if (audioSource.isPlaying) {
				audioSource.Pause ();
				isPause = true;
				setText ();
			} else {
				audioSource.UnPause ();
				isPause = false;
				setText ();
			}
		}

	}

	void setText(){
		musicName.GetComponent<Text>().text = musicMas[j].name+"";
		if (i == j && !isPause)
			musicName.GetComponent<Text> ().color = new Color(0F, 0F, 1F);
		if (i == j && isPause)
			musicName.GetComponent<Text> ().color = new Color(1F, 0F, 0F);
		if (i != j)
			musicName.GetComponent<Text> ().color = new Color(1F, 1F, 1F);
	}

	void setVolumeText(){
		volume.GetComponent<Text>().text = Mathf.CeilToInt(audioSource.volume*10) + "";

	}

	public void IncreaseVolume(){
		if (!isVolumeOff) {
			if (audioSource.volume != 1f)
				audioSource.volume += 0.1f;
				currentVolume = audioSource.volume;
		}else {
			if (currentVolume != 1f)
				currentVolume += 0.1f;
				audioSource.volume = currentVolume;
		}
		setVolumeText ();
	}


	public void DecreaseVolume(){
	
		if (!isVolumeOff) {
			if (audioSource.volume != 0f)
				audioSource.volume -= 0.1f;
				currentVolume = audioSource.volume;
		} else {
			if (currentVolume != 0f)
				currentVolume -= 0.1f;
				audioSource.volume = currentVolume;
		}
		setVolumeText ();
	}

	public void OffVolume(){
		if (audioSource.volume != 0) {
			currentVolume = audioSource.volume;
			audioSource.volume = 0;
			isVolumeOff = true;
		} else {
			audioSource.volume = currentVolume;
			isVolumeOff = false;
		}

		setVolumeText ();
	}
		
}
