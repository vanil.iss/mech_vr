﻿using UnityEngine;
using System.Collections;

public class GunCameraScript : MonoBehaviour {
	public float XSensevity =1f;
	public float YSensevity =1f;

	private float xRotation;
	private float yRotation;

	private GunScript1 gunScript1;

	void Start(){
		xRotation = transform.rotation.eulerAngles.x;
		yRotation = transform.rotation.eulerAngles.y;

		gunScript1 = (GunScript1) GetComponent ("GunScript1");
	}

	// Update is called once per frame
	void Update () {
		float xValue = Input.GetAxis("Mouse X")*XSensevity;
		float yValue = Input.GetAxis("Mouse Y")*YSensevity;
		yRotation += xValue;
		xRotation += yValue;

		if (xRotation < -90 || xRotation > 48) {
			gunScript1.enabled = false;
		}if (xRotation > -90 && xRotation < 48) {
			gunScript1.enabled = true;
		} 

		transform.rotation = Quaternion.Euler (-xRotation, yRotation, 0);

	}
}
