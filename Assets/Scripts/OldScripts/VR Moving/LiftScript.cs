﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LiftScript : MonoBehaviour
{
	public bool isFastStart;

	public int pos;
	bool[] isPos = new bool[10];
	string word;
	public AudioClip liftOpenSound;
	public AudioClip liftCloseSound;
	public AudioClip liftFinish;
	public AudioClip liftHidden;
	public AudioClip liftMusic;
	public GameObject guiTextLink;
	public AudioSource audioSource;
	public AudioSource audioGoSource;
	public AudioSource audioSourceMusic;
	public GameObject cameraLift;
	public GameObject cameraMech;
	public GameObject city;
	public GameObject playingNPS;
	public GameObject clouds;
	public GameObject hangar;
	public HangarManagement hangarManagement;
	public float speedOneLetter = 0.1f;
	bool isClose = true;
	private MonitorInLift monitor;
	private bool isPlaying;
	// True if LiftGOSound is playing
	private bool isEnd = false;
	// True if lift end all animation
	private bool isFadeOut = false;
	private bool isNeedFadeOutGoSound = false;
	private bool isNeedFadeOutLiftMusic = false;

	public string fastLiftStart;

	#region LoadLevel

	private AsyncOperation syncLevel;
	public Image whiteTexture;

	#endregion

	Animator animator;

	private enum LiftState
	{
		LoadGame,
		NewGame,
		Open,
		Close}

	;

	void liftOpen()
	{
		if (isClose)
		{

			animator.SetBool("isOpen", true);
			audioSource.clip = liftOpenSound;
			audioSource.volume = 1;
			audioSource.Play();
			isClose = false;
		}
	}

	void liftClose()
	{
		if (!isClose)
		{
			audioSource.clip = liftCloseSound;
			audioSource.Play();
			isClose = true;
			//Debug.Log("Close");
		}
	}

	void soundFadeOut(AudioSource source)
	{
		if (source.volume > 0.01f)
		{
			isFadeOut = false;
			source.volume -= (float)1 * Time.deltaTime;
		}
		else
		{
			source.Stop();
			isPlaying = false;
			isNeedFadeOutGoSound = false;
		}
	}

	void setSoundGO(bool Play)
	{
		//Debug.Log ("Play: " + Play + " isPlaying: " + isPlaying);
		if (Play && !isPlaying)
		{
			isNeedFadeOutGoSound = false;
			audioGoSource.volume = 1f;
			audioGoSource.Play();
			isPlaying = true;
		}
		else
		{
			if (!Play && isPlaying)
			{
				soundFadeOut(audioGoSource);
			}
		}
	}

	void setSoundMusic(bool Play)
	{
		if (Play)
		{
			Camera.main.GetComponent<MenuInput>().enabled = true;

			audioSourceMusic.clip = liftMusic;
			audioSourceMusic.volume = 0.1f;
			audioSourceMusic.Play();
		}
		else
		{
			if (audioSourceMusic.volume > 0.01f)
			{
				audioSourceMusic.volume -= (float)0.1 * Time.deltaTime;
			}
			else
			{
				audioSourceMusic.Stop();
				isNeedFadeOutLiftMusic = false;
			}
		}
	}

	void playLiftHidden()
	{
		audioSource.clip = liftHidden;
		audioSource.volume = 0.5f;
		audioSource.Play();
	}

	private LiftState liftState;

	void Start()
	{
		fastLiftStart = "";
		pos = -1;
		animator = GetComponent<Animator>();
		liftState = LiftState.LoadGame;
		playingNPS.SetActive(true);
		city.SetActive(false);
		clouds.SetActive(false);
		hangar.SetActive(false);
		whiteTexture.color = new Color(0, 0, 0, 0);
		monitor = GetComponentInChildren<MonitorInLift>();

		Camera.main.GetComponent<MenuInput>().enabled = false;
	}

	// Update is called once per frame
	void Update()
	{

		#region FadeSound
		if (isNeedFadeOutGoSound)
		{
			setSoundGO(false);
		}
		if (isNeedFadeOutLiftMusic)
		{
			setSoundMusic(false);
		}
		#endregion

		#region Animation
		switch (liftState)
		{
			case LiftState.LoadGame:
				animator.SetBool("isLoadGame", true);
				setSoundGO(true);
				if (animator.IsInTransition(0))
				{
					StartCoroutine(WaitForOpen());
					//audioGoSource.Stop();
				}

				break;
			case LiftState.NewGame:

				if (animator.IsInTransition(0))
				{
					liftState = LiftState.Close;
				}
				break;
			case LiftState.Open:

				isNeedFadeOutGoSound = true;
				if (isClose)
				{
					liftOpen();
				}
				break;
			case LiftState.Close:
			//setSoundGO(false);
				liftClose();
				break;
		}
		#endregion

		#region TheEndOfLiftScene
		if (isEnd)
		{
			if (Input.GetKey(KeyCode.Mouse0))
			{
				cameraLift.SetActive(false);
				cameraMech.SetActive(true);
				hangarManagement.isMechCameraActive = true;
				isEnd = false;
			}
		}
		#endregion

		#region Quest
		switch (pos)
		{
			case 0:
				if (isPos[pos] == false)
				{
					monitor.pos++;
					isPos[pos] = true;
				}
				break;
			case 1:
				if (isPos[pos] == false)
				{
					animator.enabled = true;
					isPos[pos] = true;
				}
				break;
			case 2:
				if (isPos[pos] == false)
				{
					monitor.pos++;
					isPos[pos] = true;
				}
				break;
			case 3:
				if (isPos[pos] == false)
				{
					animator.enabled = true;
					isPos[pos] = true;
				}
				break;
			case 4:
				if (isPos[pos] == false)
				{
					monitor.pos++;
					animator.enabled = false;
					isPos[pos] = true;
				}
				break;
			case 5:
				if (isPos[pos] == false)
				{
					animator.enabled = true;
					isPos[pos] = true;
				}
				break;

		}
		#endregion
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.name == "FirstStop")
		{
			if (!isFastStart)
			{
				animator.enabled = false;
				other.enabled = false;
				playingNPS.SetActive(false);
				city.SetActive(true);
				pos++;
			}
			else
			{
				isFastStart = false;
				if (fastLiftStart == "")
					SceneManager.LoadScene(PlayerPrefs.GetString("lastLevel", "TrainingGroundScene"));
				else
					SceneManager.LoadScene(fastLiftStart); 
			}
		}

		if (other.name == "SecondStop")
		{
			animator.enabled = false;
			city.SetActive(false);
			clouds.SetActive(true);
			other.enabled = false;
			pos++;
		}

		if (other.name == "ThirdStop")
		{
			animator.enabled = false;
			other.enabled = false;
			clouds.SetActive(false);
			hangar.SetActive(true);
			pos++;
		}
		if (other.name == "ForthStop")
		{
			isNeedFadeOutGoSound = true;
			audioSource.clip = liftFinish;
			audioSource.Play();
			other.enabled = false;
			StartCoroutine(WaitForRing());
		}
		if (other.name == "GOSoundBegin")
		{
			isNeedFadeOutLiftMusic = true;
			setSoundGO(true);
			other.enabled = false;
		}
		if (other.name == "LiftHidden")
		{
			other.enabled = false;
			playLiftHidden();
		}
	}

	/*
	IEnumerator OnTriggerEnter(Collider other)
	{
		//StartCoroutine (Wait (5));

		if (other.name == "FirstStop" || other.name == "SecondStop" || other.name == "ThirdStop")
		{
				animator.enabled = false;
				yield return new WaitForSeconds(5f);
			Debug.Log("Ouch!");
				animator.enabled = true;

			//Debug.Log ("Ouch!!!");
		}

	}

*/

	public void NewGame()
	{
		animator.SetBool("isOpen", false);
		animator.SetBool("isNewGame", true);
		isClose = false;
		whiteTexture.enabled = true;

//LoadLevel();
		liftState = LiftState.NewGame;
	}

	public void EndGame()
	{
		animator.SetBool("isOpen", false);
		animator.SetBool("isEndGame", true);
		isClose = false;
		whiteTexture.enabled = true;

		StartCoroutine(wait());

	}


	IEnumerator WaitForOpen()
	{
		yield return new WaitForSeconds(3);
		liftState = LiftState.Open;
		yield return new WaitForSeconds(3);
		setSoundMusic(true);
	}

	IEnumerator WaitForRing()
	{
		yield return new WaitForSeconds(2);
		liftOpen();
		isEnd = true;
	}

	private void LoadLevel()
	{
		StartCoroutine(EffectWhite());
	}

	IEnumerator EffectWhite()
	{

		while (whiteTexture.color.a <= 1f)
		{

			Color color = whiteTexture.color;
			color.a += 0.1f;
			whiteTexture.color = color;
			yield return true;

		}
		//syncLevel = SceneManager.LoadSceneAsync(2);

	}

	IEnumerator wait()
	{
		yield return new WaitForSecondsRealtime(1f);

		Application.Quit();
	}

}
