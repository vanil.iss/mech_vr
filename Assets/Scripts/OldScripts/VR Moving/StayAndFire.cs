﻿using UnityEngine;
using UnityEngine.VR;
using VRStandardAssets.Utils;

using System.Collections;

public class StayAndFire : MonoBehaviour {

    [SerializeField] private float m_Damping = 0.5f;                                // The damping with which this gameobject follows the camera.
    [SerializeField] private float m_GunContainerSmoothing = 10f;                   // How fast the gun arm follows the reticle.

    [SerializeField] private Reticle m_Reticle;                                     // This is what the gun arm should be aiming at.
    [SerializeField] private Transform m_GunContainer;                              // This contains the gun arm needs to be moved smoothly.

    private float XSensevity = 1f;
	private float YSensevity = 1f;
	private float xRotation;
	private float yRotation;

	private float xValue;
	private float yValue;

	private static float gunStartXrotation;
	private static float gunStartYrotation;

	public Transform gunCamera;
	private float cam_xRotation;
	private float cam_yRotation;

    private const float k_DampingCoef = -20f;                                       // This is the coefficient used to ensure smooth damping of this gameobject.

//    [SerializeField] private bool isVR = false;

	public bool canMove = true;

// Use this for initialization
	void Start () {
		//xRotation = transform.rotation.eulerAngles.x;
		xRotation = transform.rotation.x;
		//yRotation = transform.rotation.eulerAngles.y;
		yRotation = transform.rotation.y;

		gunStartXrotation = transform.rotation.eulerAngles.x;
		gunStartYrotation = transform.rotation.eulerAngles.y;

		//gunCamera = transform.Find ("Gun Camera");
		cam_xRotation = gunCamera.rotation.eulerAngles.x;
		cam_yRotation = gunCamera.rotation.eulerAngles.y;
	}
	
	// Update is called once per frame
	void Update () {


		//if(yRotation+xValue<... && xRotation+yValue<...) {}
		#if UNITY_ANDROID
			if (canMove) {
				transform.rotation = Quaternion.Slerp (transform.rotation, UnityEngine.XR.InputTracking.GetLocalRotation (UnityEngine.XR.XRNode.Head),
					m_Damping * (1 - Mathf.Exp (k_DampingCoef * Time.deltaTime)));

				transform.position = gunCamera.position;

				Quaternion lookAtRotation = Quaternion.LookRotation (m_Reticle.ReticleTransform.position - m_GunContainer.position);

				m_GunContainer.rotation = Quaternion.Slerp (m_GunContainer.rotation, lookAtRotation,
					m_GunContainerSmoothing * Time.deltaTime);

			}
        #else
			if (canMove) {
				xValue = Input.GetAxis ("Mouse X") * XSensevity;
				yValue = Input.GetAxis ("Mouse Y") * YSensevity * (-1);

				xRotation += yValue;
				yRotation += xValue;

				transform.rotation = Quaternion.Euler (xRotation, yRotation, 0);

				cam_xRotation = gunCamera.rotation.eulerAngles.x;
				cam_yRotation = gunCamera.rotation.eulerAngles.y;
				ReturningToGunCamera ();

			}
		#endif
	}

	public void setMove(bool value){
		canMove = value;
	}

	//если не равно стартовой позиции, то приближает поворот пушки на 0.5f к начальному повороту
	public void ReturningToBeginPosition(){

		if (xRotation != gunStartXrotation ||
		   yRotation != gunStartYrotation) {

			if (xRotation > gunStartXrotation) {
				xRotation -= 0.5f;
			}

			if (xRotation < gunStartXrotation) {
				xRotation += 0.5f;
			}

			if (yRotation > gunStartYrotation) {
				yRotation -= 0.5f;
			}

			if (yRotation < gunStartYrotation) {
				yRotation += 0.5f;
			}

			transform.rotation = Quaternion.Euler (xRotation, yRotation, 0);
		}
	}

	public void ReturningToGunCamera(){
		if (m_Reticle != null && m_GunContainer != null) {
			Quaternion lookAtRotation = Quaternion.LookRotation (m_Reticle.ReticleTransform.position - m_GunContainer.position);

			m_GunContainer.rotation = Quaternion.Slerp (m_GunContainer.rotation, lookAtRotation,
				m_GunContainerSmoothing * Time.deltaTime);
		}
	}
}
