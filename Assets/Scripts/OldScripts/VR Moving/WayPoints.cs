﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WayPoints : MonoBehaviour {

//region ComponentCash
    private Pause pause;
//endregion

	private GameObject[] points; 
	private Vector3 target_Pos;
	//private int i = 0;
	[SerializeField]
	private float speed_move = 20f;
	[SerializeField]
	private float rotationSpeed = 0.005f;
	float distance;

	//SetRotation
	Vector3 lookPos;
	Quaternion rotation;
	//

	public Queue<Vector3> wayQueue = new Queue<Vector3>();
	//Transform pointTransform;
	bool isAtPoint = true;

    void Awake() {
        try{
            pause = GameObject.FindGameObjectWithTag("Pause").GetComponent<Pause>();
        } catch{}
    }

	void Start () {
		target_Pos = transform.position;
		//получение массива всех точек
		//points = GameObject.FindGameObjectsWithTag("Point");
		//wayQueue ;
		//wayQueue.Enqueue (GameObject.Find ("Point1").transform.position);
		//Debug.Log (GameObject.Find ("Point1").transform.position + "");
		//получение каждой точки по имени и перезапись в тот же массив
		//сортировки по имени метод поиска объектов по тегу не предусматривает, и это печально
		//for (int i = 0; i <points.Length; i++){
		//GameObject wayPointGO = GameObject.Find( "Point" +  (i+1) );
		//points [i] = wayPointGO;
		//}
	}

	void FixedUpdate () {


		//позиция точки
		if (pause.isPause == false) {

		if (isAtPoint == true) {
			if (wayQueue.Count != 0) {

				target_Pos = (Vector3)wayQueue.Dequeue ();
				//Debug.Log ("Yes!!!");
				isAtPoint = false;

			}

		} else if(target_Pos!=transform.position) {


			SetRotation(target_Pos);
			transform.position = Vector3.MoveTowards (transform.position, target_Pos, Time.deltaTime * speed_move);

			distance = Vector3.Distance(target_Pos, transform.position);
			if (distance < 0.5f) isAtPoint = true;

		}

		//Debug.Log ("isAtPoint=" + isAtPoint);


		//target_Pos = points[i].transform.position;
		//поворот в сторону конечной чели(точки)
		//SetRotation(target_Pos);
		//движемся
		//transform.position = Vector3.MoveTowards (transform.position, target_Pos, Time.deltaTime*speed_move);

		//вычисляем расстояние до точки
		//float distance = Vector3.Distance(target_Pos, transform.position);

		//if (distance < 0.5f) {
		//if (i < points.Length - 1) {
		//i++;
		//} else {
		//поправить!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//speed_move = 0;
		//}
		//}
	}
	}


	void SetRotation(Vector3 lookAt)
	{

		lookPos = lookAt - transform.position;
		//lookPos.y = 0;

		if (lookPos.magnitude != 0) {
			rotation = Quaternion.LookRotation(lookPos);
			transform.rotation = Quaternion.Lerp(transform.rotation, rotation, rotationSpeed);
		}

		//this.transform.LookAtж
	}

	public void addWayQueue(Vector3 position){
		if(position!=null) wayQueue.Enqueue (position);
		isAtPoint = true;
	}

	public void Stop(){
		isAtPoint = true;
	//	wayQueue.Clear ();
	}
}


