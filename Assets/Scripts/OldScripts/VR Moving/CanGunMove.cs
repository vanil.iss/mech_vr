﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanGunMove : MonoBehaviour {

	private StayAndFire stayAndFire;

	void Start(){
		stayAndFire = GameObject.Find ("Gun").GetComponent<StayAndFire> ();
	}
		

	void OnTriggerEnter(Collider other) {
		stayAndFire.setMove (false);
	}

	void OnTriggerStay(Collider other) {
		stayAndFire.setMove (false);
	}

	void OnTriggerExit(Collider other)
	{
		stayAndFire.setMove (true);
	}
}
