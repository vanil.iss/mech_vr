﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HlthShldWayPoint : MonoBehaviour {


	public bool isHealthRestore;
	public bool isShieldRestore;

	//HealthMech healthMech;
	public int healthBank = 250;
	public int shieldBank = 250;

	int coefficient = 1;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider other) {

		if (other.tag == "Player") {
			if (isHealthRestore && healthBank != 0){
				if (coefficient <= healthBank) {
					healthBank -= coefficient;
					other.GetComponent<HealthMech> ().health += coefficient;
				} else {
					other.GetComponent<HealthMech> ().health += healthBank;
					healthBank = 0;
				}

				//чтобы коэффициент два раза не увеличивался
				if (!isHealthRestore || !isShieldRestore)coefficient += coefficient/10;
			}

			if (isShieldRestore && shieldBank != 0 && other.GetComponent<HealthMech> ().shieldWork){
				if (coefficient <= shieldBank) {
					shieldBank -= coefficient;
					other.GetComponent<HealthMech> ().powerShield += coefficient;
				} else {
					other.GetComponent<HealthMech> ().powerShield += shieldBank;
					shieldBank = 0;
				}

				coefficient += coefficient/10;
			}



		}
			
	}
}
