﻿using UnityEngine;
using System.Collections;

public class ObjectHealth : MonoBehaviour
{

	public Data data;
	public int health;
	public Vector3 bounds;

	public bool useParent = false;
	private ObjectHealth parentObjectHealth;

	void Start()
	{
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<Data>();
		bounds = GetComponent<Collider>().bounds.size;

		if (useParent)
		{
			parentObjectHealth = transform.parent.GetComponent<ObjectHealth>();
			health = parentObjectHealth.health;
		}
	}


	public void activisionHit(int value)
	{
		if (health > 0)
		{
			if (name == "head")
			{
				health -= value * 2;
			}
			else
				health -= value;
			
			if (useParent)
				parentObjectHealth.health = health;
		}


		if (health <= 0)
		{
			switch (tag)
			{
				case "Block":
					data.deleteCover(GetComponentInChildren<CoverScript>().gameObject);
					break;
				case "Shooter":
					StartCoroutine(UnspawnShooter());
					break;
				case "Electric":
					StartCoroutine(UnspawnElectric());
					break;
				case "Kamikaze":
					StartCoroutine(UnspawnKamikaze());
					break;
			}
		}
			
	}


	IEnumerator UnspawnShooter()
	{
		yield return new WaitForSecondsRealtime(8f);

		if (transform.name == "head")
		{
			Statistic.braincrack++;
			data.shooterPool.Unspawn(transform.parent.gameObject);
		}
		else
			data.shooterPool.Unspawn(gameObject);
	}

	IEnumerator UnspawnElectric()
	{
		yield return new WaitForSecondsRealtime(8f);
		if (transform.name == "head")
		{
			Statistic.braincrack++;

			data.electricPool.Unspawn(transform.parent.gameObject);

		}
		else
			data.electricPool.Unspawn(gameObject);
	}

	IEnumerator UnspawnKamikaze()
	{
		yield return new WaitForSecondsRealtime(8f);

		if (transform.name == "head")
		{
			Statistic.braincrack++;

			data.kamikazePool.Unspawn(transform.parent.gameObject);

		}
		else
			data.kamikazePool.Unspawn(gameObject);
	}
}