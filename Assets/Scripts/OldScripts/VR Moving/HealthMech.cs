﻿using UnityEngine;

public class HealthMech : MonoBehaviour
{
	//region ComponentCash
	private Pause pause;
	//endregion

	public int powerShield;
	public int health;

	public bool shieldWork;

	public float timeSub = 10;
	private float _timeSub;

	private bool alive;
	private DeathController deathController;

	void Awake()
	{
		try
		{
			pause = GameObject.FindGameObjectWithTag("Pause").GetComponent<Pause>();
		}
		catch
		{
		}
		deathController = GetComponent<DeathController>();
	}

	void Start()
	{
		alive = true;
		shieldWork = false;
	}

	// Update is called once per frame
	void Update()
	{
		if (pause.isPause == false)
		{
			if (shieldWork)
			{
				if (_timeSub < timeSub)
					_timeSub += Time.deltaTime;
			}
			else
			{
				_timeSub = 0;
			}

			if (_timeSub >= timeSub && powerShield > 0)
			{
				_timeSub = 0;
				powerShield -= 1;
			}
		}

		if (!alive)
		{

			deathController.enabled = true;

			
		}
	}

	//просто выстрел, бьёт по щиту, далее по механике
	public void justShot(int value)
	{
		if (alive)
		{
			if (health > 0)
			{
				if (powerShield > 0 && shieldWork)
					powerShield -= value;
				else
					health -= value;
			}
			else
			{
				alive = false;
				deathController.enabled = true;
				deathController.gameEnd();
				health = 0;
			}
		}
	}

	// бьёт только по щиту, в любом из случаев
	public void energeShot(int value)
	{
		if (shieldWork)
		{
			if (powerShield >= 0)
				powerShield -= value;
		}
	}

	//бьёт по здоровью невзирая на щит
	public void hardShot(int value)
	{
		if (alive)
		{
			if (health > 0)
			{
				if (shieldWork)
				{
					if (powerShield > 0)
					{
						int shieldDamage = value / 2;
						int healthDamage = value - shieldDamage;

						powerShield -= shieldDamage;
						health -= healthDamage;
					}
				}
				else if (!shieldWork || powerShield <= 0)
				{
					health -= value;
				}

			}
			else
			{
				alive = false;
				deathController.enabled = true;
				deathController.gameEnd();
				health = 0;
			}
		}

	}

}
