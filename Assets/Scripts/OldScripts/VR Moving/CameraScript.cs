﻿//#if UNITY_ANDROID

//#else
using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
	public float XSensevity =1f;
	public float YSensevity =1f;

	private float xRotation;
	private float yRotation;

	//private GunScript1 gunScript1;

	void Start(){


		xRotation = transform.rotation.eulerAngles.x;
		yRotation = transform.rotation.eulerAngles.y;

		//gunScript1 = (GunScript1) GetComponent ("GunScript1");
	}

	// Update is called once per frame
	void Update () {
		float xValue = Input.GetAxis("Mouse X")*XSensevity;
		float yValue = Input.GetAxis("Mouse Y")*YSensevity;
		yRotation += xValue;
		xRotation += yValue;

		//if (xRotation < -90 || xRotation > 48) {
			//.enabled = false;
		//} else {
			//gunScript1.enabled = true;
		//} 

		transform.localRotation = Quaternion.Euler (-xRotation, yRotation, 0);

	}
}
//#endif