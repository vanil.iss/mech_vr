﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestLastLevel : MonoBehaviour {


	bool[] isPos;
	public AudioSource source;
	public AudioClip newMessage;
	private static int pos;


	GameObject jumpingMech;
	//public GameObject aircraft;
	// Use this for initialization
	void Start() {
		pos = 0;
		isPos = new bool[10];

		jumpingMech = GameObject.Find ("JumpingMech");

	}

	// Update is called once per frame
	void Update() {
		LogCatController.quest = "\nУничтожить главный офис";
		switch (pos) {
		case 0:
			if (!isPos[pos]) {
				source.PlayOneShot(newMessage);
				if (MonitorControl.missionText != null) {
					MonitorControl.missionText.text = "Настало время расправиться с мозгом организации. Нечего делать вид, что ты не понял метафоры, вперед!";
					isPos[pos] = true;
				}
			}
			break;
		case 1:
			if (!isPos[pos]) {
				source.PlayOneShot(newMessage);
				if (MonitorControl.missionText != null) {
					MonitorControl.missionText.text = "Они думают, что нас можно остановить большой стеной? Как бы не так!";
					isPos[pos] = true;
				}
			}
			break;
		case 2:
			if (!isPos[pos]) {
				source.PlayOneShot(newMessage);
				if (MonitorControl.missionText != null) {
					MonitorControl.missionText.text = "Эти дома вызывают подозрение, надеюсь чертовы противники не научились телепортироваться.";
					isPos[pos] = true;
				}
			}
			break;
		case 3:
			if (!isPos[pos]) {
				source.PlayOneShot(newMessage);
				if (MonitorControl.missionText != null) {
					MonitorControl.missionText.text = "Это их малнькое поселение, как-будто специально создано, чтобы быть уничтоженным.";
					isPos[pos] = true;
				}
			}
			break;
		case 4:
			if (!isPos[pos]) {
				if (MonitorControl.missionText != null) {
					MonitorControl.missionText.text = "Взорви офис к чертям, а заодно вон ту странную штуку.";
					isPos[pos] = true;
				}
			}
			break;
		case 5:
			if (!isPos[pos]) {
				if (MonitorControl.missionText != null) {
					MonitorControl.missionText.text = "Это мех! АААААААА";
					isPos[pos] = true;
				}
			}
			break;
		case 6:
			if (!isPos[pos]) {
				if (MonitorControl.missionText != null) {
					MonitorControl.missionText.text = "Что ж, зато теперь мы знаем, что их разарботки это отстой.\n Готовься к эвакуации.";
					isPos[pos] = true;
					PlayerPrefs.SetInt ("gamePassed", 1);
				}
			}
			break;


		}

		if (pos >= 3 && jumpingMech.GetComponent<ObjectHealth> ().health == 0) {
			pos = 5;
		}
	}


	public static void NextMessage(){
		pos++;
	}

}
