﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseDestroy : MonoBehaviour
{

	private Transform mainHouse;
	private Transform destroyedHouse;
	private GameObject destroyedHouseGO;
	private Transform[] destroyedHouseGOs;

	private ObjectHealth objectHealth;
	private Animator buildAnimator;
	private Color alphaChannel;
	private int fracturesLength;
	private bool isDestroyed;

// Use this for initialization
	void Start ()
	{
		isDestroyed = false;
		mainHouse = transform.GetChild (0);
		destroyedHouse = transform.GetChild (1);
		destroyedHouseGO = transform.GetChild (1).gameObject;
		//destroyedHouseGO = transform.GetChild (1).gameObject.transform.GetChild(1).gameObject;
		destroyedHouseGOs = destroyedHouseGO.GetComponentsInChildren<Transform>();
		fracturesLength = destroyedHouseGOs.Length;
		buildAnimator = destroyedHouse.GetComponent<Animator> ();
		destroyedHouse.gameObject.SetActive (false);

		objectHealth = transform.GetChild (0).GetComponent<ObjectHealth>();
		//alphaChannel = destroyedHouseGOs[1].gameObject.GetComponent<MeshRenderer> ().material.color;
		alphaChannel.a = 0;
	}

	void Update ()
	{

		if (objectHealth.health == 0 && !isDestroyed) {
            Allocation.updateAllocs();
			Statistic.countDestroyed++;

            mainHouse.gameObject.SetActive (false);
			destroyedHouse.gameObject.SetActive (true);
			try {
			for (int i = 1; i < fracturesLength; i++) {
				for (int j = 0; j < destroyedHouseGOs [i].gameObject.GetComponent<MeshRenderer> ().materials.Length; j++) {
					destroyedHouseGOs[i].gameObject.GetComponent<MeshRenderer> ().materials[j].SetFloat ("_SliceAmount", destroyedHouseGOs[i].gameObject.GetComponent<MeshRenderer> ().materials[j].GetFloat("_SliceAmount") + Time.deltaTime * 0.3f);
				}
				//destroyedHouseGOs[i].gameObject.GetComponent<MeshRenderer> ().materials[0].SetFloat ("_SliceAmount", 0.5 + Mathf.Sin(Time.time)*0.5);
				//destroyedHouseGOs[i].gameObject.GetComponent<MeshRenderer> ().materials[0].color = Color.Lerp (destroyedHouseGOs[i].gameObject.GetComponent<MeshRenderer> ().materials[0].color, alphaChannel, 5 * Time.deltaTime);
				//destroyedHouseGOs[i].gameObject.GetComponent<MeshRenderer> ().materials[1].SetFloat ("_SliceAmount", 0.5 + Mathf.Sin(Time.time)*0.5);
				//destroyedHouseGOs[i].gameObject.GetComponent<MeshRenderer> ().materials[1].color = Color.Lerp (destroyedHouseGOs[i].gameObject.GetComponent<MeshRenderer> ().materials[1].color, alphaChannel, 5 * Time.deltaTime);
			}
			//Debug.Log (destroyedHouseGOs [1].gameObject.GetComponent<MeshRenderer> ().material.color.a);
			if (destroyedHouseGOs [1].gameObject.GetComponent<MeshRenderer> ().material.GetFloat("_SliceAmount") > 1f) {
				for (int i = 1; i < fracturesLength; i++) {
					Destroy (destroyedHouseGOs [i].gameObject);
				}
				isDestroyed = true;
			}
			} catch {
				Debug.Log (transform.name + " is undestroyable");
				isDestroyed = true;
			}
			//buildAnimator.SetTrigger ("LowHealthTrigger");

			//GetComponent<BoxCollider> ().enabled = false;
			//objectHealth.health -= 1;
			//Destroy (transform, 4f);
		}

	}
}

