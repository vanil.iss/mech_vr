﻿using System.IO;
using UnityEngine;

public class GameInfo : MonoBehaviour
{

	string lastLevel;

	int kill10;
	int stupidHero;
	int disign;
	int painLove;
	int brainCrack;
	int adDestoroy;


	private AudioSource source;

	private GameObject miniMap;
	private GameObject monitor;
	private GameObject logCat;

	private GameObject player;

	public AudioClip newMessage;
	private HealthMech healthMech;

	void Awake()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		miniMap = GameObject.Find("MiniMap");
		monitor = GameObject.Find("Monitor");
		logCat = GameObject.Find("LogCat");
	}


	// Use this for initialization
	void Start()
	{
		lastLevel = PlayerPrefs.GetString("lastLevel", "");

		Statistic.countKilled = PlayerPrefs.GetInt("countKill", 0);
		Statistic.countDestroyed = PlayerPrefs.GetInt("countDestroy", 0);
		Statistic.countKamikazeKilled = PlayerPrefs.GetInt("countKamikaze", 0);
		Statistic.braincrack = PlayerPrefs.GetInt("braincrack", 0);
		Statistic.adDestoroy = PlayerPrefs.GetInt("adDestoroy", 0);

		kill10 = PlayerPrefs.GetInt("kill10", 0);
		stupidHero = PlayerPrefs.GetInt("stupidHero", 0);
		disign = PlayerPrefs.GetInt("disign", 0);
		painLove = PlayerPrefs.GetInt("painLove", 0);
		brainCrack = PlayerPrefs.GetInt("brainCrack", 0);
		adDestoroy = PlayerPrefs.GetInt("adDestoroy", 0);

		try
		{
			healthMech = GameObject.FindGameObjectWithTag("Player").GetComponent<HealthMech>();
			source = GameObject.Find("CanvasMech").GetComponent<AudioSource>();
		}
		catch
		{

		}
	}
	
	// Update is called once per frame
	void Update()
	{
		if (kill10 == 0)
		{
			if (Statistic.countKilled == 10)
			{
				source.PlayOneShot(newMessage);
				if (monitor.activeSelf)
				{
					MonitorControl.missionText.text = "Достижение: Мех-Потрошитель\n" +
					"(Убито 10 врагов)\n" +
					"Да Вы маньяк! Но мне это нравится! Продолжайте кромсать их!";

					kill10 = 1;
					PlayerPrefs.SetInt("kill10", kill10);
				}
			}
		}
		else if (stupidHero == 0)
		{
			if (StupidHeroAchivment.heroDetected)
			{
				source.PlayOneShot(newMessage);

				if (monitor.activeSelf)
				{
					MonitorControl.missionText.text = "Достижение: Бесполезный герой \n" +
					"Поздравляю! Вы не внесли абсолютно никакой вклад в нашу великую цель.";

					stupidHero = 1;
					PlayerPrefs.SetInt("stupidHero", stupidHero);
				}
			}
		}
		else if (disign == 0)
		{
			if (Statistic.countDestroyed == 10)
			{
				source.PlayOneShot(newMessage);

				if (monitor.activeSelf)
				{
					MonitorControl.missionText.text = "Достижение: Дизайнер \n" +
					"(Разрушено 10 объектов)\n" +
					"Разрушать, конечно, плохо, но так весело! ";

					disign = 1;
					PlayerPrefs.SetInt("disign", disign);
				}
			}
		}
		else if (painLove == 0)
		{
			if (healthMech.health == 200 && healthMech.powerShield == 250)
			{
				source.PlayOneShot(newMessage);

				if (monitor.activeSelf)
				{
					MonitorControl.missionText.text = "Достижение: Любитель побольнее \n" +
					"(Принят урон без щита)\n" +
					"Пс-с-с, парень, мы для кого щит вообще вставляли?";

					painLove = 1;
					PlayerPrefs.SetInt("painLove", painLove);
				}
			}
		}
		else if (brainCrack == 0)
		{
			if (Statistic.braincrack == 25)
			{
				source.PlayOneShot(newMessage);

				if (monitor.activeSelf)
				{
					MonitorControl.missionText.text = "Достижение: Вынос Мозга \n" +
					"(25 убийств в голову)\n" +
					"Даже учить ничему не пришлось, сам догодался!";

					brainCrack = 1;
					PlayerPrefs.SetInt("brainCrack", brainCrack);
				}
			}
		}
		else if (adDestoroy == 0)
		{
			if (Statistic.adDestoroy == 4)
			{
				source.PlayOneShot(newMessage);

				if (monitor.activeSelf)
				{
					MonitorControl.missionText.text = "Достижение: Блокиратор Рекламы \n" +
					"(5 уничтоженных баннеров)\n" +
					"Все рекламные агенства в панике, кто-то ходит и срывает всю рекламу! А начинали-то всего-лишь с реклам в лифте...";

					adDestoroy = 1;
					PlayerPrefs.SetInt("adDestoroy", adDestoroy);
				}
			}
		}


	}

}
