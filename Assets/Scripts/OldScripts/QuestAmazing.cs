﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestAmazing : MonoBehaviour
{

	int bonusComplete;
	private AudioSource source;

	private GameObject monitor;
	public AudioClip newMessage;

	// Use this for initialization
	void Start()
	{
		monitor = GameObject.Find("Monitor");
		source = GameObject.Find("CanvasMech").GetComponent<AudioSource>();

		bonusComplete = PlayerPrefs.GetInt("bonusComplete", 0);
	}
	
	// Update is called once per frame
	void Update()
	{
		if (bonusComplete == 0)
		{
			if (GetComponent<ObjectHealth>().health == 0)
			{
				source.PlayOneShot(newMessage);
				if (monitor.activeSelf)
					MonitorControl.missionText.text = "Достижение: Сквозь Боль и Блевоту\n" +
					"(Пройти бонусную сцену)\n" +
					"Вы прошли все возможные миссии! Как можно выдерживать такой напор?";

				bonusComplete = 1;
				PlayerPrefs.SetInt("bonusComplete", bonusComplete);

			}
		}
	}
}
