﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shootingcontrol : MonoBehaviour {

    Animator anim;
    public GunScript1 gunScript1;
    int isShootHash = Animator.StringToHash("isShoot");
    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
//        gunScript1 = GameObject.FindObjectOfType(typeof(GunScript1)) as GunScript1;
    }
	
	// Update is called once per frame
	void Update () {
        //Debug.Log("" + gunScript1.isShooting);
        if (Input.GetKey(KeyCode.Mouse0) && gunScript1.isShooting)
        {
            anim.SetBool("isShoot", true);
            //anim.SetTrigger(isShootHash);
        }
        if (!Input.GetKey(KeyCode.Mouse0))
        {
            anim.SetBool("isShoot", false);
            //anim.SetTrigger(isShootHash);
        }
    }
}
