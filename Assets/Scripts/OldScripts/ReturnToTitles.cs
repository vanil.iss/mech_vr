﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace VRStandardAssets.Utils
{
	public class ReturnToTitles : MonoBehaviour
	{
		[SerializeField] private string m_MenuSceneName = "LiftScene";
		// The name of the main menu scene.
		[SerializeField] private VRInput m_VRInput;
		// Reference to the VRInput in order to know when Cancel is pressed.
		// [SerializeField] private VRCameraFade m_VRCameraFade;           // Reference to the script that fades the scene to black.

		void Start()
		{
			
		}

		private void OnEnable()
		{
			m_VRInput.OnCancel += HandleCancel;
		}


		private void OnDisable()
		{
			m_VRInput.OnCancel -= HandleCancel;

		}


		private void HandleCancel()
		{
			SceneManager.LoadScene("LiftScene");
		}



	}
}