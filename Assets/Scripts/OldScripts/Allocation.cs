﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Allocation : MonoBehaviour
{
	public static GameObject[] allocs;

	Vector3 DirectionRay;
	RaycastHit hit;

	[SerializeField] private Transform allocationPrefab;
	private MeshRenderer activeNow;
	private bool controlActive = false;


	// Use this for initialization
	void Start()
	{
		allocs = GameObject.FindGameObjectsWithTag("Allocation");
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		DirectionRay = Camera.main.transform.forward;

		if (!Physics.Raycast(Camera.main.transform.position, DirectionRay, out hit))
		{
			allocationPrefab.position = new Vector3(0f, -100000f, 0f);

			if (activeNow != null)
				activeNow.enabled = false;
			//if (GameObject.Find ("Wall_Alloc"))
			//	GameObject.Find ("Wall_Alloc").GetComponent<MeshRenderer> ().enabled = false;

			controlActive = false;
			return;
		}

		if (hit.collider.GetComponent<AllocActivate>())
		{
			allocationPrefab.position = new Vector3(0f, -100000f, 0f);

			
			activeNow = hit.collider.GetComponent<AllocActivate>().getRend();
			activeNow.enabled = true;
		}
		else
		{
			controlActive = false;
			foreach (GameObject rend in allocs)
			{
				rend.GetComponent<MeshRenderer>().enabled = false;
			}

			ObjectHealth health = hit.transform.GetComponent<ObjectHealth>();
			if (health == null)
			{
				allocationPrefab.position = new Vector3(0f, -100000f, 0f);

				return;
			}

			if (hit.collider.GetComponent<ObjectHealth>().useParent)
			{
				allocationPrefab.position = new Vector3(0f, -100000f, 0f);

				try
				{
					activeNow = hit.transform.parent.GetComponent<AllocActivate>().getRend();
					activeNow.enabled = true;
				}
				catch
				{
					
				}

			}
			else
			{
				allocationPrefab.position = hit.collider.bounds.center;
				allocationPrefab.rotation = hit.transform.rotation;
				allocationPrefab.localScale = health.bounds;
			}
		}


	}

	public static void updateAllocs()
	{
		allocs = GameObject.FindGameObjectsWithTag("Allocation");
	}
}
