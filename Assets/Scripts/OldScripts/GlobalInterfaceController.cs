﻿using System;
using UnityEngine;

public class GlobalInterfaceController : MonoBehaviour
{

	//region ComponentCash
	private Pause pause;
	//endregion


	public bool sf_active = true;
	private bool mv_active = true;
	//private bool mv_another = true;

	private WayPoints wayPoints;
	private GameObject gun;
	private StayAndFire stayAndFire;
	private GameObject mainCamera;
	Camera interfaceCamera;
	private GameObject gunCamera;

	private float distance = 1000f;
	RaycastHit hit;
	//Ray ray;
	public bool b1 = false, b2 = false, b3 = false;


	Quaternion miniMapRotation;
	[SerializeField]
	private Transform mapObj;
	private Collider[] radiusColliders;
	[SerializeField]
	private float radius = 200f;
	Vector3 mapStartPosition;

	GameObject healthBar;
	GameObject shieldBar;
	HealthMech mechHealth;

	GameObject monitor, miniMap, logCat;
	//int curHealth;
	//int curShield;

	MusicPlayer_old _musicPlayerOld;
	Shootingcontrol shootingcontrol;
	Animator gunAnimator;

	ButtonRotation buttonRotation;

	private AudioSource source;
	public AudioClip touch;
	public AudioClip touchWayPoint;

	//float timePassed = 0;

	void Awake()
	{
		try
		{
			pause = GameObject.FindGameObjectWithTag("Pause").GetComponent<Pause>();
			source = GameObject.Find("CanvasMech").GetComponent<AudioSource>();
		}
		catch
		{
		}

	}

	// Use this for initialization
	void Start()
	{
		wayPoints = (WayPoints)GetComponent("WayPoints");
		//wayPoints.addWayQueue (GameObject.Find ("Point1").transform.position);

		gun = GameObject.Find("Gun");
		stayAndFire = (StayAndFire)gun.GetComponent("StayAndFire");

		mainCamera = GameObject.Find("Main Camera");
		interfaceCamera = mainCamera.GetComponent<Camera>();
		gunCamera = GameObject.Find("Gun Camera");
		gunCamera.SetActive(false);

		miniMap = GameObject.Find("MiniMap");
		miniMap.SetActive(false);
		monitor = GameObject.Find("Monitor");
		logCat = GameObject.Find("LogCat");
		logCat.SetActive(false);
	

		healthBar = GameObject.Find("HealthBar");
		shieldBar = GameObject.Find("ShieldBar");
		mechHealth = (HealthMech)GetComponent<HealthMech>();
		//curHealth = mechHealth.health;
		//curShield = mechHealth.powerShield;
		healthBar.GetComponent<TextMesh>().text = mechHealth.health + "";
		shieldBar.GetComponent<TextMesh>().text = mechHealth.powerShield + "";

		_musicPlayerOld = (MusicPlayer_old)GameObject.Find("MusicPlayer_old").GetComponent<MusicPlayer_old>();
		buttonRotation = transform.GetComponent<ButtonRotation>();

		shootingcontrol = GetComponentInChildren<Shootingcontrol>();
		gunAnimator = shootingcontrol.GetComponent<Animator>();
		//shootingcontrol.enabled = false;

		OVRTouchpad.Create();
		OVRTouchpad.TouchHandler += HandleTouchHandler;
	}

	// Update is called once per frame
	void Update()
	{
		if (pause.isPause == false)
		{

			//if (interfaceCamera.isActiveAndEnabled && Input.GetMouseButtonDown (0)) {
			if (interfaceCamera.isActiveAndEnabled && (/*itIsTimeToTap()|| */Input.GetMouseButtonDown(0)))
			{
				Vector3 DirectionRay = interfaceCamera.transform.TransformDirection(Vector3.forward);
				if (Physics.Raycast(interfaceCamera.transform.position, DirectionRay, out hit, distance))
				{
					//Quaternion hitRotation = Quaternion.FromToRotation (Vector3.up, hit.normal);

					if (hit.collider)
					{
						switch (hit.collider.name)
						{

							case "Button1":
								b1 = !b1;
								source.PlayOneShot(touch);
								break;
							case "Button2":
								b2 = !b2;
								source.PlayOneShot(touch);
								break;
							case "Button3":
								b3 = !b3;
								source.PlayOneShot(touch);
								break;
							case "NextMusicButton":
								_musicPlayerOld.nextComposition();
								break;
							case "PreviousMusicButton":
								_musicPlayerOld.previousComposition();
								break;
							case "PlayPauseMusicButton":
								_musicPlayerOld.playPause();
								break;
							case "IncreaseVolume":
								_musicPlayerOld.IncreaseVolume();
								break;
							case "DecreaseVolume":
								_musicPlayerOld.DecreaseVolume();
								break;
							case "OffVolume":
								_musicPlayerOld.OffVolume();
								break;
							case "MonitorButton":
								miniMap.SetActive(false);
								monitor.SetActive(true);
								logCat.SetActive(false);

								source.PlayOneShot(touch);

								break;
							case "MapButton":
								miniMap.SetActive(true);
								monitor.SetActive(false);
								logCat.SetActive(false);

								source.PlayOneShot(touch);

								break;
							case "LogCatButton":
								miniMap.SetActive(false);
								monitor.SetActive(false);
								logCat.SetActive(true);

								source.PlayOneShot(touch);

								break;

						}
						switch (hit.collider.tag)
						{

							case "Point":

								if (!mv_active)
								{
									mv_active = !mv_active;
									wayPoints.enabled = mv_active;
								}

								source.PlayOneShot(touchWayPoint);

								wayPoints.addWayQueue(hit.collider.GetComponentInParent<Transform>().position);
								break;
						}
					}

				}
			}
			if (interfaceCamera.isActiveAndEnabled && (/*itIsTimeToTap()|| */Input.GetKey(KeyCode.Mouse0)))
			{
				Vector3 DirectionRay = interfaceCamera.transform.TransformDirection(Vector3.forward);
				if (Physics.Raycast(interfaceCamera.transform.position, DirectionRay, out hit, distance))
				{
					//Quaternion hitRotation = Quaternion.FromToRotation (Vector3.up, hit.normal);

					if (hit.collider)
					{
						switch (hit.collider.name)
						{

							case "RightRotation":
								buttonRotation.RightRotation();
								wayPoints.Stop();


								break;
							case "LeftRotation":
								buttonRotation.LeftRotation();
								wayPoints.Stop();


								break;

						}
					}

				}
			}
			if (interfaceCamera.isActiveAndEnabled)
			{
				//if (curHealth != mechHealth.health)
				//{
				healthBar.GetComponent<TextMesh>().text = mechHealth.health + "";
				//curHealth = mechHealth.health;
				//} 

				if (mechHealth.shieldWork == false)
				{
					shieldBar.GetComponent<TextMesh>().text = "OFF";
				}
				else
				{
					//if (curShield != mechHealth.powerShield)
					//{
					shieldBar.GetComponent<TextMesh>().text = mechHealth.powerShield + "";
					//curShield = mechHealth.powerShield;
					//}
				} 
				//MapChange();
			}

			//if (Input.GetKeyDown (KeyCode.Mouse0)) {
			//ray = camera.ScreenPointToRay (Input.mousePosition);
			//if (Physics.Raycast (transform.position, -Vector3.up, out hit, 100.0F)) {
			//if (hit.transform.name == "Button1") {  
			//b1 = true;
			//Debug.Log ("BUTTON!!!(1)");
			//}
			//if (hit.transform.name == "Button2") {  
			//b2 = true;
			//	Debug.Log ("BUTTON!!!(2)");
			//}
			//}

			//}



			if (b1 || Input.GetKeyDown(KeyCode.F))
			{
				changeCameras();
			}

			if (b2)
			{
				mv_active = !mv_active;
				wayPoints.enabled = mv_active;
				b2 = false;
			}

			if (b3)
			{
				
				GetComponent<HealthMech>().shieldWork = !GetComponent<HealthMech>().shieldWork;
				b3 = false;
			}

			//возвращение пушки в начальное положение(поворот)
			//if (!sf_active == false)
			///stayAndFire.ReturningToGunCamera ();

		}
	}

	void MapChange()
	{
		miniMapRotation = miniMap.transform.rotation;
		radiusColliders = Physics.OverlapSphere(transform.position, radius);
		mapStartPosition = miniMap.transform.position + miniMap.transform.up * 0.05f;
		foreach (Collider collider in radiusColliders)
		{
			if (collider.tag != "Interface" && collider.tag != "Ground")
			{
				Vector3 mapDistance = (collider.transform.position - transform.position) / 1000;
				Transform metaHitGo = Instantiate(mapObj, mapStartPosition + mapDistance, miniMapRotation) as Transform;
				metaHitGo.parent = miniMap.transform;
				Destroy((metaHitGo as Transform).gameObject, 0.02f);
			}

		}
		//metaHitGo.GetComponent<Renderer> ().material.color.a = 0.5f;
		// metaHitGo.GetComponent<Collider>().enabled = false;

		//+collider.transform.position / 100
	}

	private void HandleTouchHandler(object sender, EventArgs e)
	{
		if (pause.isPause == false)
		{
			OVRTouchpad.TouchArgs touchArgs = (OVRTouchpad.TouchArgs)e;
			OVRTouchpad.TouchEvent touchEvent = touchArgs.TouchType;
			if (touchArgs.TouchType == OVRTouchpad.TouchEvent.Up)
			{
				if (sf_active)
				{
					changeCameras();
				}
			}
			else if (touchArgs.TouchType == OVRTouchpad.TouchEvent.Down)
			{
				if (!sf_active)
					changeCameras();
			}
		}
		//if (!sf_active == false)
		//stayAndFire.ReturningToGunCamera ();
	}

	public void changeCameras()
	{
		sf_active = !sf_active;

		//wayPoints.enabled = active;
		mainCamera.SetActive(sf_active);
		healthBar.SetActive(sf_active);
		shieldBar.SetActive(sf_active);

		stayAndFire.setMove(!sf_active);
		stayAndFire.enabled = !sf_active;
		gunCamera.SetActive(!sf_active);
		shootingcontrol.enabled = !sf_active;
		gunAnimator.enabled = !sf_active;
		b1 = false;
	}

	/*bool itIsTimeToTap()
    {
        timePassed += Time.deltaTime;
        if (timePassed >= 3f)
        {
            timePassed = 0;
            return true;
        }
        else return false;
    }*/

	public Transform getPosMainCamera()
	{
		if (sf_active)
			return mainCamera.transform;
		else
			return gunCamera.transform;
	}

}
