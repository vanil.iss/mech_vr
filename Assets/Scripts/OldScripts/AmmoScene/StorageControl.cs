﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StorageControl : MonoBehaviour
{


	private ObjectHealth health;
	public ParticleSystem fireOnDeath;

	public float radiusExplosion = 30f;
	public AudioClip expl;

	private bool alive;
	// Use this for initialization
	void Start()
	{
		alive = true;
		health = GetComponentInChildren<ObjectHealth>();

		MissionAmmo.countStorage++;

	}
	
	// Update is called once per frame
	void Update()
	{
		if (alive)
		{
			
		
			if (health.health <= 0)
			{
				fireOnDeath.Play();

				Collider[] hitColliders = Physics.OverlapSphere(transform.position, radiusExplosion);
				for (int i = 0; i < hitColliders.Length; i++)
				{
					ObjectHealth healthObject = hitColliders[i].GetComponent<ObjectHealth>();
					HealthMech healthMech = hitColliders[i].GetComponent<HealthMech>();

					if (healthObject != null)
						healthObject.activisionHit(10);
					else if (healthMech != null)
						healthMech.hardShot(30);
				}
				MissionAmmo.countStorage--;

//				GetComponent<AudioSource>().PlayOneShot(expl);
				alive = false;
				//	GetComponent<StorageControl>().enabled = false;
			}
		}
	}
}
