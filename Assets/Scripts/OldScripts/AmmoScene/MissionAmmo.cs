﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class MissionAmmo : MonoBehaviour
{

	public static int countStorage;

	bool[] isPos;
	public AudioSource source;
	public StupidHeroAchivment stupidHeroAchivment;

	public AudioClip newMessage;
	int pos;

	public GameObject aircraft;
	// Use this for initialization
	void Start()
	{
		pos = 0;
		isPos = new bool[10];
//		MonitorControl.missionText.text = "Приготовьтесь, сэр! Бойня начинается!";
	}

	// Update is called once per frame
	void Update()
	{
		LogCatController.quest = "\nСкладов: " + countStorage;
		switch (pos)
		{
			case 0:
				if (!isPos[pos])
				{
					source.PlayOneShot(newMessage);
					if (MonitorControl.missionText != null)
					{
						MonitorControl.missionText.text = "Первая фаза.\n Вражеские силы скудны(да и ваши тоже), так что нужно просто разрушить вражеский боезапас!";
						isPos[pos] = true;
					}
				}

				if (countStorage == 3)
				{
					source.PlayOneShot(newMessage);

					pos++;

				}
				break;
			case 1:
				if (!isPos[pos])
				{
					if (MonitorControl.missionText != null)
					{
						MonitorControl.missionText.text = "Джеронимо! Враги от вас в ужасе, продолжайте в том же духе!";
						isPos[pos] = true;
					}
				}

				if (countStorage == 2)
				{
					source.PlayOneShot(newMessage);
					pos++;

				}
				break;
			case 2:
				if (!isPos[pos])
				{
					source.PlayOneShot(newMessage);

					if (MonitorControl.missionText != null)
					{
						MonitorControl.missionText.text = "Да не, не пойдёт он по и подозрительному коридору! Я ж не мог такого глупца взять?";
						isPos[pos] = true;
					}
				}

				if (countStorage == 0)
				{
					source.PlayOneShot(newMessage);
					pos++;

				}
				break;
			case 3:
				if (!isPos[pos])
				{
					if (MonitorControl.missionText != null)
					{
						MonitorControl.missionText.text = "Медленно, однако!\n" +
						"Как можно было столько возиться?\n" +
						"Ждите, мы вас заберём.";
						isPos[pos] = true;
						/* StartCoroutine(waitForNextLevel());*/
						AircraftExit();
					}
				}
				break;

		}
	}

	/* private IEnumerator waitForNextLevel() {
        yield return new WaitForSecondsRealtime(15f);
        PlayerPrefs.SetString("lastLevel","Level2(TreesZone)");

        PlayerPrefs.SetInt("countKill", Statistic.countKilled);
        PlayerPrefs.SetInt("countDestroy", Statistic.countDestroyed);
        PlayerPrefs.SetInt("countKamikaze",  Statistic.countKamikazeKilled);

        SceneManager.LoadScene("Level2(TreesZone)");

    }*/

	private void AircraftExit()
	{
	
		PlayerPrefs.SetString("lastLevel", "Level2(TreesZone)");

		PlayerPrefs.SetInt("countKill", Statistic.countKilled);
		PlayerPrefs.SetInt("countDestroy", Statistic.countDestroyed);
		PlayerPrefs.SetInt("countKamikaze", Statistic.countKamikazeKilled);
		PlayerPrefs.SetInt("braincrack", Statistic.braincrack);
		PlayerPrefs.SetInt("adDestoroy", Statistic.adDestoroy);

		aircraft.SetActive(true);
		aircraft.GetComponent<AircraftExit>().enabled = true;
	}

}
