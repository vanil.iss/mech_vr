﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllocActivate : MonoBehaviour {

	[SerializeField] private MeshRenderer rend;

	public void changeState(){
		rend.enabled = !rend.enabled;
	}

	public MeshRenderer getRend(){
		return rend;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
