﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class MissionTraining : MonoBehaviour
{

	private bool rightOK, leftOK;
	private int countCyl;
	private int countElectrics;
	public Transform wayPoint;
	public GameObject shooter;
	public GameObject electricGroup;

	bool[] isPos;
	public AudioSource source;

	public AudioClip newMessage;
	public AudioClip ghost;

	int pos;
	private GameObject miniMap;
	private GameObject monitor;
	private GameObject logCat;

	private GameObject player;
	public GunScript1 gunScript;
	public GameObject aircraft;
	// Use this for initialization
	void Awake()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		miniMap = GameObject.Find("MiniMap");
		monitor = GameObject.Find("Monitor");
		logCat = GameObject.Find("LogCat");
	}

	void Start()
	{
		pos = -1;
		isPos = new bool[11];

		rightOK = false;
		leftOK = false;
		countCyl = 0;
		countElectrics = 0;

		player.GetComponent<WayPoints>().enabled = false;
		gunScript.enabled = false;
		StartCoroutine(wait());
	}
	
	// Update is called once per frame
	void Update()
	{

//        LogCatController.quest = "\nСкладов: " + countStorage;
		switch (pos)
		{
			case 0:
				if (!isPos[pos])
				{
					source.PlayOneShot(newMessage);
					if (monitor.activeSelf)
					{
						MonitorControl.missionText.text = "Приветствую! Как тебя называть? Хотя-я-я, всё равно забуду. И так, это ваш монитор, а теперь бегом в борт-компьютер(кнопка под монитором)";
						isPos[pos] = true;
					}
				}

				if (logCat.activeSelf)
				{
					pos++;
					source.PlayOneShot(newMessage);
				}

				break;
			case 1:
				if (!isPos[pos])
				{
					if (logCat.activeSelf)
					{
						LogCatController.quest = "\nДа-да, сюда. Здесь все полученные шишки и задание. Далее, в миникарту c всякими точками(и обратно в монитор)";
						isPos[pos] = true;
					}
				}

				if (miniMap.activeSelf)
				{
					LogCatController.quest = "\nПоворот";
					pos++;
					source.PlayOneShot(newMessage);
				}

				break;
			case 2:
				if (!isPos[pos])
				{
					if (monitor.activeSelf)
					{
						MonitorControl.missionText.text = "Хоть что-то умеешь. Далее, цифры - здоровье - зеленое, щит(подробнее позже) - синий. И кнопки верчения - покрутились!";
						isPos[pos] = true;
					}
				}

				if (Input.GetKey(KeyCode.Mouse0))
				{
					RaycastHit hit;
					Vector3 DirectionRay = Camera.main.transform.TransformDirection(Vector3.forward);
					if (Physics.Raycast(Camera.main.transform.position, DirectionRay, out hit, 1000))
					{
						//Quaternion hitRotation = Quaternion.FromToRotation (Vector3.up, hit.normal);
						if (hit.collider.name == "RightRotation")
							rightOK = true;
						if (hit.collider.name == "LeftRotation")
							leftOK = true;
					}

				}

				if (rightOK && leftOK)
				{
					LogCatController.quest = "\nВключить музыку";

					pos++;
					source.PlayOneShot(newMessage);
				}
			

				break;
			case 3:
				if (!isPos[pos])
				{
					if (monitor.activeSelf)
					{
						MonitorControl.missionText.text = "А знаешь что может быть хуже уничтожения всего? Уничтожение без музыки! Включай мой любимый трек!(Пс-с-с, плеер сверху)";
						isPos[pos] = true;
					}
				}

				if (Input.GetKey(KeyCode.Mouse0))
				{
					RaycastHit hit;
					Vector3 DirectionRay = Camera.main.transform.TransformDirection(Vector3.forward);
					if (Physics.Raycast(Camera.main.transform.position, DirectionRay, out hit, 1000))
					{
						//Quaternion hitRotation = Quaternion.FromToRotation (Vector3.up, hit.normal);
						if (hit.collider.name == "PlayPauseMusicButton")
						{
							LogCatController.quest = "\nВключить щит";

							pos++;
							source.PlayOneShot(newMessage);
						}
					}

				}

				break;
			case 4:
				if (!isPos[pos])
				{
					if (monitor.activeSelf)
					{
						MonitorControl.missionText.text = "Щит - энергетическая оборона на биоразлагаемом топливе, увы тратится при использовании. Первая кнопка с края, пробуем!";
						isPos[pos] = true;
					}
				}

				if (Input.GetKey(KeyCode.Mouse0))
				{
					RaycastHit hit;
					Vector3 DirectionRay = Camera.main.transform.TransformDirection(Vector3.forward);
					if (Physics.Raycast(Camera.main.transform.position, DirectionRay, out hit, 1000))
					{
						//Quaternion hitRotation = Quaternion.FromToRotation (Vector3.up, hit.normal);
						if (hit.collider.name == "Button3")
						{
							LogCatController.quest = "\nПерейти в пулемёт";

							pos++;
							source.PlayOneShot(newMessage);
						}
					}

				}
					
				break;
			case 5:
				if (!isPos[pos])
				{
					if (monitor.activeSelf)
					{
						MonitorControl.missionText.fontSize = 16;
						MonitorControl.missionText.text = "Последняя нудность. Средняя кнопка - стоп/старт движка и третья - переход к пулемёту!(либо свайп). Тест перехода!";
						isPos[pos] = true;
					}
				}

				if (!player.GetComponent<GlobalInterfaceController>().sf_active)
				{

					gunScript.enabled = true;
					pos++;
					source.PlayOneShot(newMessage);
				}
			
				break;
			case 6:
				if (!isPos[pos])
				{
					if (monitor.activeSelf)
					{
						MonitorControl.missionText.text = "Ну как там? Понравилось? Теперь ешё интереснее, стрельба (удерживание тачпада) в бочки! Всего три цели, вперёд!";
						isPos[pos] = true;
					}
				}

				if (Input.GetKey(KeyCode.Mouse0) && !player.GetComponent<GlobalInterfaceController>().sf_active)
				{
					RaycastHit hit;
					Vector3 DirectionRay = Camera.main.transform.TransformDirection(Vector3.forward);
					if (Physics.Raycast(Camera.main.transform.position, DirectionRay, out hit, 1000))
					{
						//Quaternion hitRotation = Quaternion.FromToRotation (Vector3.up, hit.normal);
						if (hit.collider.GetComponent<SimpleHitTest>() && hit.collider.GetComponent<SimpleHitTest>().enabled)
						{
							hit.collider.GetComponent<SimpleHitTest>().enabled = false;
							countCyl++;
						}
					}

				}
				LogCatController.quest = "\nБочек подстрелено: " + countCyl;

				if (countCyl == 3)
				{
					player.GetComponent<WayPoints>().enabled = true;
					LogCatController.quest = "\nДвижение к вейпоинту";

					pos++;
					source.PlayOneShot(newMessage);
				}
				break;
			case 7:
				if (!isPos[pos])
				{
					if (monitor.activeSelf)
					{
						MonitorControl.missionText.text = "Движение. Двигайтесь к первой точке, для этого нужно из кабины выбрать зеленоватый ромб(вейпоинт). И кстати, всё что выделяется, можно уничтожить!";
						isPos[pos] = true;
					}
				}
					
				if (Vector3.Distance(player.transform.position, wayPoint.position) < 5f)
				{
					shooter.SetActive(true);
					shooter.GetComponent<NavMeshAgent>().enabled = true;
					shooter.GetComponent<MovingBot>().enabled = true;
					shooter.GetComponent<AudioSource>().enabled = true;


					pos++;
					source.PlayOneShot(newMessage);
				}

				break;
			case 8:
				if (!isPos[pos])
				{
					if (monitor.activeSelf)
					{
						MonitorControl.missionText.text = "Пора знакомиться с вражеским войском. Это Штурмовик(чуток усиленный), он немного умный, умеет прятаться. Цельсь - Огонь!";
						isPos[pos] = true;
					}
				}

				LogCatController.quest = "\nЗдоровье врага: " + shooter.GetComponent<ObjectHealth>().health;

				if (shooter.GetComponent<ObjectHealth>().health < 2)
					shooter.GetComponent<ObjectHealth>().health = 0;

				if (shooter.GetComponent<ObjectHealth>().health == 0)
				{
					electricGroup.SetActive(true);
					StartCoroutine(waitGhost());
					pos++;
					source.PlayOneShot(newMessage);
				}
		
				break;
			case 9:
				if (!isPos[pos])
				{
					if (monitor.activeSelf)
					{
						MonitorControl.missionText.text = "Теперь Электрик. Может показаться несильным, но ведь Ghostbusters! Нацелен на выкачивание энергии щита(если включен), рассчитывайте всё с моим умом!";
						isPos[pos] = true;
					}
				}

				foreach (ObjectHealth electric in electricGroup.GetComponentsInChildren<ObjectHealth>())
				{
					if (electric.health <= 2 && electric.health != -1)
					{
						countElectrics++;
						electric.health = -1;
					}

				}

				if (countElectrics == 3)
				{
					pos++;
					source.PlayOneShot(newMessage);
				}

				break;
			case 10:
				if (!isPos[pos])
				{
					if (monitor.activeSelf)
					{
						MonitorControl.missionText.text = "Поздравляю с успешным прохождением тренировочный пытки! Далее вас ждут адские муки! (необязательно на этих миссиях)";
						isPos[pos] = true;
					}
				}
				StartCoroutine(NextLevel());
				break;
		}
	}

	IEnumerator wait()
	{
		yield return new WaitForSecondsRealtime(2f);

		pos = 0;

	}

	IEnumerator waitGhost()
	{
		yield return new WaitForSecondsRealtime(2f);

		source.PlayOneShot(ghost);

	}

	IEnumerator NextLevel()
	{
		yield return new WaitForSecondsRealtime(5f);

		aircraft.SetActive(true);
		aircraft.GetComponent<AircraftExit>().enabled = true;
		pos = 0;

	}
}
