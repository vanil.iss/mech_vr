﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchivScene : MonoBehaviour
{


	int kill10;
	public GameObject killLock;
	public GameObject killActive;


	int stupidHero;

	public GameObject stupidLock;
	public GameObject stupidActive;

	int disign;

	public GameObject disignLock;
	public GameObject disignActive;

	int painLove;

	public GameObject painLock;
	public GameObject painActive;


	int bonusComplete;

	public GameObject bonusLock;
	public GameObject bonusActive;

	int brainCrack;

	public GameObject brainLock;
	public GameObject brainActive;

	int adDestroy;

	public GameObject adDestroyLock;
	public GameObject adDestroyActive;

	void Start()
	{
		kill10 = PlayerPrefs.GetInt("kill10", 0);
		stupidHero = PlayerPrefs.GetInt("stupidHero", 0);
		disign = PlayerPrefs.GetInt("disign", 0);
		painLove = PlayerPrefs.GetInt("painLove", 0);
		bonusComplete = PlayerPrefs.GetInt("bonusComplete", 0);


		if (kill10 == 1)
		{
			killLock.SetActive(false);
			killActive.SetActive(true);
		}

		if (stupidHero == 1)
		{
			stupidLock.SetActive(false);
			stupidActive.SetActive(true);
		}

		if (disign == 1)
		{
			disignLock.SetActive(false);
			disignActive.SetActive(true);
		}

		if (painLove == 1)
		{
			painLock.SetActive(false);
			painActive.SetActive(true);
		}

		if (bonusComplete == 1)
		{
			bonusLock.SetActive(false);
			bonusActive.SetActive(true);
		}

		if (brainCrack == 1)
		{
			brainLock.SetActive(false);
			brainActive.SetActive(true);
		}

		if (adDestroy == 1)
		{
			adDestroyLock.SetActive(false);
			adDestroyActive.SetActive(true);
		}
	}
		
}
