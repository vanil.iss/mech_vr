﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestSystem : MonoBehaviour {

	int pos;
	private int countFClick;
	bool[] isPos = new bool[10];

	public Text textWriter;
	public GameObject cylinderContainer;
	public GameObject mech;
	public GameObject banner;

	// Use this for initialization
	void Start () {
		pos = 0;	

		mech.GetComponent<GlobalInterfaceController>().enabled = false;
	//	mech.GetComponentInChildren<GunScript1> ().enabled = false;

		mech.GetComponent<WayPoints> ().enabled = false;

		banner.GetComponentInChildren<ParticleSystem> ().Stop ();
	}


	// Update is called once per frame
	void Update () {
		switch (pos) {
		case 0:
			if (isPos [pos] == false) {
				textWriter.GetComponent<Text> ().text = "Напоминание (код 4.51): \n" +
				"Вы – пилот большой механической машины «Мех-1».\n" +
				"Согласно предписанной инструкции, \n" +
				"вы обязаны пройти обучение заново.\n" +
				"Enter - продолжить";

				isPos [pos] = true;
			} else {
				if (Input.GetKeyDown (KeyCode.Return))
					pos++;
			}
			break;
		case 1:
			if (isPos [pos] == false) {
				textWriter.GetComponent<Text> ().text = "Cейчас вы в кабине пилота,\n " +
				"можете осмотреться c помощью движения мышью.\n" +
				"З.Ы.Даже кнопочки есть, но пока рано!\n" +
				"Enter - продолжить";

				isPos [pos] = true;
			} else {
				if (Input.GetKeyDown (KeyCode.Return))
					pos++;
			}
			break;
		case 2:
			if (isPos [pos] == false) {
				mech.GetComponent<GlobalInterfaceController> ().enabled = true;

				textWriter.GetComponent<Text> ().text = "Да-да! И пушка у нас есть,\n" +
				"F - переключение режима,\n" +
				" попробуй по переключаться, боец!";
				
				isPos [pos] = true;
			} else {
				if (Input.GetKeyDown (KeyCode.F))
					countFClick++;
				if (countFClick == 1)
					pos++;
			}

			break;
		case 3:
			if (isPos [pos] == false) {
				mech.GetComponentInChildren<GunScript1> ().enabled = true;

				textWriter.GetComponent<Text> ().text = "Просьба закрепиться в режиме стрельбы,\n" +
				" сейчас будет экшен!\n" +
				"Левая кнопка - стрельба, подбейте эти чёртовы бочки!";
				isPos [pos] = true;
			} else {
				SimpleHitTest[] cycl = cylinderContainer.GetComponentsInChildren<SimpleHitTest> ();
				if (cycl.Length == 0)
					pos++;
			}
			break;
		case 4:
			if (isPos [pos] == false) {
				textWriter.GetComponent<Text> ().text = "Ай да молодец! Какая меткость!\n" +
				"Заводите мотор, красная центральная кнопка на вашем кресле.\n"+
                "Переключиться - клавиша F";
				isPos [pos] = true;

			} else {
				if (mech.GetComponent<WayPoints> ().enabled == true)
					pos++;
			}
			break;
		case 5:
			if (isPos [pos] == false) {
				textWriter.GetComponent<Text> ().text = "Вы, практически, постигли тайну мироздания.\n" +
				"Наверняка заметили ещё пару кнопок?\n" +
				"Одна из них переключает в режим стрельбы,\n" +
				"функция третьей - пока тайна!\n" +
				"Enter - продолжить";
				isPos [pos] = true;

			} else {
				if (Input.GetKeyDown (KeyCode.Return))
					pos++;
			}
			break;
		case 6:
			if (isPos [pos] == false) {
				textWriter.GetComponent<Text> ().text = "А, точно! Рядом с кнопками прочность меха\n" +
				"Ниже - настоящая миникарта!\n" +
				"Правда, на ней отображаются все физические объекты,\n" +
				"но не суть.\n" +
				"Enter - продолжить";
				isPos [pos] = true;

			} else {
				if (Input.GetKeyDown (KeyCode.Return))
					pos++;
			}
			break;
		case 7:
			if (isPos [pos] == false) {
				textWriter.GetComponent<Text> ().text = "И последнее,\n осталось уничтожить истинное зло!\n" +
				"Рекламу!";
				isPos [pos] = true;

			}


            if (banner.GetComponent<ObjectHealth>().health <= 0){
                    textWriter.GetComponent<Text>().enabled = false;

                    banner.GetComponentInChildren<ParticleSystem>().Play();

                    textWriter.GetComponent<Text>().enabled = true;

                    textWriter.GetComponent<Text>().fontSize = 35;

                    textWriter.GetComponent<Text>().text = "Да начнётся же \nВЕЛИКОЕ ОСВОБОЖДЕНИЕ!";

                    StartCoroutine(WaitOnStart());

                    pos++;
            }
			break;	
		default:
			break;
		}
	}

	IEnumerator Waiter (){


		yield return new WaitForSecondsRealtime (10f);

		pos++;
	}
		
	IEnumerator WaitOnStart(){
		yield return new WaitForSecondsRealtime (10f);

		Application.LoadLevel (3);
	}
}
