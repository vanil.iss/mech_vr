﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HangarManagement : MonoBehaviour
{

	public bool isMechCameraActive = false;
	private bool isOpened = false;
	public Animator animator;
	private AsyncOperation syncLevel;

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		//Debug.Log (isMechCameraActive+" "+isOpened);
		if (Input.GetKey(KeyCode.Mouse0) && isMechCameraActive && !isOpened)
		{
			animator.SetTrigger("Open");
			isOpened = true;
		}
		if (animator.IsInTransition(0) && isMechCameraActive && isOpened)
		{
			StartCoroutine(WaitForOpen());
			//Debug.Log ("LOL");
			//syncLevel = SceneManager.LoadSceneAsync("Level1(City)");
		}
	}


	IEnumerator WaitForOpen()
	{
		yield return new WaitForSeconds(10);

		PlayerPrefs.SetString("lastLevel", "TrainingGroundScene");
		SceneManager.LoadScene(PlayerPrefs.GetString("lastLevel", "TrainingGroundScene"));
		//	syncLevel = SceneManager.LoadSceneAsync("AmmunitionLevel");
	}
}
