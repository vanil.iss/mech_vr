﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuInput : MonoBehaviour
{

	private Pause pause;
	RaycastHit hit;
	private LiftScript lift;

	public GameObject secretBtn;

	void Start()
	{
		lift = GameObject.Find("Lift").GetComponent<LiftScript>();

		if (PlayerPrefs.GetInt("gamePassed", 0) == 1)
		{
			secretBtn.SetActive(true);
		}
	}

	void Update()
	{
		
		if (Input.GetMouseButtonDown(0))
		{
			if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 1000f))
			{
				if (hit.collider.name == "New Game")
				{
					lift.NewGame();
				}
				else if (hit.collider.name == "Fast Start")
				{
					lift.isFastStart = true;
					lift.NewGame();
				}
				else if (hit.collider.name == "Exit")
				{
					lift.EndGame();
				}
				else if (hit.collider.name == "Secret")
				{
					lift.isFastStart = true;
					lift.fastLiftStart = "Level1JumpingMechCrazy";
					lift.NewGame();
				}
				else if (hit.collider.name == "Achivment")
				{
					lift.isFastStart = true;
					lift.fastLiftStart = "AchivmentScene";
					lift.NewGame();
				}

			}
		}

	}

	//    void HandleTouchHandler(object sender, System.EventArgs e) {
	//        OVRTouchpad.TouchArgs touchArgs = (OVRTouchpad.TouchArgs)e;
	//        OVRTouchpad.TouchEvent touchEvent = touchArgs.TouchType;
	//        if (touchArgs.TouchType == OVRTouchpad.TouchEvent.SingleTap) {
	//            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 1000f)) {
	//                if (hit.collider.name == "New Game") {
	//                    lift = GameObject.Find("Lift").GetComponent<LiftScript>();
	//                    lift.NewGame();
	//                } else if (hit.collider.name == "Fast Start") {
	//
	//                    lift = GameObject.Find("Lift").GetComponent<LiftScript>();
	//
	//                    lift.NewGame();
	//                    lift.isFastStart = true;
	//
	//                } else if (hit.collider.name == "Exit") {
	//                    lift = GameObject.Find("Lift").GetComponent<LiftScript>();
	//
	//                    lift.EndGame();
	//                } else if (hit.collider.name == "Resume") {
	//                    pause = GameObject.FindGameObjectWithTag("Pause").GetComponent<Pause>();
	//                    pause.isPause = false;
	//                    pause.pauseActivate();
	//                } else if (hit.collider.name == "Quit") {
	//                    Time.timeScale = 1f;
	//                    SceneManager.LoadScene("LiftScene");
	//                } else if (hit.collider.name == "Return") {
	//                    player = GameObject.FindGameObjectWithTag("Player");
	//                    gunAnim = GameObject.Find("Cube.105").GetComponent<Animator>();
	//
	//                    player.GetComponent<WayPoints>().enabled = true;
	//                    gunAnim.Rebind();
	//                    pause.GetComponent<Pause>().enabled = true;
	//                    pause.GetComponent<CrossHair>().enabled = true;
	//                    GetComponent<DeathController>().enabled = false;
	//
	//                    Time.timeScale = 1f;
	//                    Cursor.lockState = CursorLockMode.Locked;
	//
	//                    Application.LoadLevel(0);
	//                }
	//
	//            }

	//        }
	//    }

}
