﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerController : MonoBehaviour {

    private ObjectHealth health;
    private Data data;
    public Transform spawnPoint;

    private int maxHealth;

    int countShooters;
    int countElectric;
    int countKamikaze;

    void Start() {
        health = GetComponent<ObjectHealth>();
        data = GameObject.FindGameObjectWithTag("Data").GetComponent<Data>();

        maxHealth = health.health;
        MissionTree.serverLive = true;
    }

// Update is called once per frame
    void Update() {
        if (health.health == maxHealth / 4) {
            countKamikaze = 5;
            health.health -= 1;

            Spawn();
        } else if (health.health == maxHealth / 2) {
            countElectric = 5;
            health.health -= 1;

            Spawn();
        } else if (health.health == maxHealth / 2) {
            countKamikaze = 3;
            countElectric = 3;
            countShooters = 5;

            health.health -= 1;

            Spawn();
        } else if (health.health == 0) {
            MissionTree.serverLive = false;
        }
    }

    IEnumerator Spawn() {
        while (countShooters != 0 || countElectric != 0 || countKamikaze != 0) {

            if (countShooters > 0) {
                GameObject shooters = data.shooterPool.Spawn(spawnPoint.position, spawnPoint.rotation);

                shooters.GetComponent<MovingBot>().enabled = true;

                countShooters--;
//data.mobList.Add(shooters);
                yield return new WaitForSecondsRealtime(3f);
            }



            if (countElectric > 0) {
                GameObject electric = data.electricPool.Spawn(spawnPoint.position, spawnPoint.rotation);

                electric.GetComponent<ElectricBot>().enabled = true;


                countElectric--;
//data.mobList.Add(electrics);

                yield return new WaitForSecondsRealtime(3f);
            }

            if (countKamikaze > 0) {
                GameObject kamikaze = data.kamikazePool.Spawn(spawnPoint.position, spawnPoint.rotation);


                kamikaze.GetComponent<KamikazeBot>().enabled = true;

                countKamikaze--;
//data.mobList.Add(kamikazes);

                yield return new WaitForSecondsRealtime(3f);
            }
        }
    }
}
