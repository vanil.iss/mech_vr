﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MissionTree : MonoBehaviour {
    public static int countDish;
    public static bool serverLive;

    bool[] isPos;

    int pos;
// Use this for initialization
    void Start() {
        pos = 0;
        isPos = new bool[10];
        serverLive = true;
//		MonitorControl.missionText.text = "Приготовьтесь, сэр! Бойня начинается!";
    }

// Update is called once per frame
    void Update() {
        if (!isPos[9]) {
            if (serverLive == false) {
                if (MonitorControl.missionText != null) {
                    MonitorControl.missionText.text = "Ха! Вражеские базы разъединены! Не правда ли мой план хорош?";
                    if (countDish != 0)
                        MonitorControl.missionText.text += "Хотя, ты и попортил всё слегка, но да ладно, всё равно тебе разбираться c подкреплением!";
                    isPos[9] = true;
                    StartCoroutine(waitForNextLevel());
                }
            }
        }
        switch (pos) {
            case 0:
                if (!isPos[pos]) {
                    if (MonitorControl.missionText != null) {
                        MonitorControl.missionText.text = "Вы чуяте этот прекрасный лесной аромат? Ой, освежитель! Ну да ладно.\n Цель: раздавить (желательно) передатчики и уничтожить вражеский сервер.";
                        isPos[pos] = true;
                    }
                }

                if (countDish == 2)
                    pos++;
                break;
            case 1:
                if (!isPos[pos]) {
                    if (MonitorControl.missionText != null) {
                        MonitorControl.missionText.text = "Я вижу мы поняли друг друга. Такими темпами враги не смогут ничего постить в Инстаграм!";
                        isPos[pos] = true;
                    }
                }

                if (countDish == 0)
                    pos++;
                break;

            case 2:
                if (!isPos[pos]) {
                    if (MonitorControl.missionText != null) {
                        MonitorControl.missionText.text = "Вот и всё, осталась только святилище сетивиков. Освободим мир от рекламной заставки!";
                        isPos[pos] = true;
                    }
                }
                break;

        }

    }

    private IEnumerator waitForNextLevel() {
        yield return new WaitForSecondsRealtime(15f);
        PlayerPrefs.SetString("lastLevel","Level3(Mountains)");

        PlayerPrefs.SetInt("countKill", Statistic.countKilled);
        PlayerPrefs.SetInt("countDestroy", Statistic.countDestroyed);
        PlayerPrefs.SetInt("countKamikaze",  Statistic.countKamikazeKilled);

        SceneManager.LoadScene("Level3(Mountains)");
    }
}
