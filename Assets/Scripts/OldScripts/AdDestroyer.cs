﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdDestroyer : MonoBehaviour
{

	private ObjectHealth objectHealth;


	// Use this for initialization
	void Start()
	{
		objectHealth = GetComponentInChildren<ObjectHealth>();
	}
	
	// Update is called once per frame
	void Update()
	{
		if (objectHealth.health == 0)
		{
			Statistic.adDestoroy++;
		}
	}
}
