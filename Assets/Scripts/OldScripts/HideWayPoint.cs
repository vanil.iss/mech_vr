﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideWayPoint : MonoBehaviour
{

	private GameObject player;
	private bool hide;

	void Awake()
	{
		player = GameObject.FindGameObjectWithTag("Player");

	}
	// Use this for initialization
	void Start()
	{
		hide = false;
	}
	
	// Update is called once per frame
	void Update()
	{
		if (Vector3.Distance(player.transform.position, transform.position) < 50f)
		{
			GetComponent<BoxCollider>().enabled = false;
			GetComponentInChildren<MeshRenderer>().enabled = false;

			hide = true;
		}

		if (hide)
		{
			if (Vector3.Distance(player.transform.position, transform.position) > 50f)
			{
				GetComponent<BoxCollider>().enabled = true;
				GetComponentInChildren<MeshRenderer>().enabled = true;

				hide = false;
			}
		}
	}
}