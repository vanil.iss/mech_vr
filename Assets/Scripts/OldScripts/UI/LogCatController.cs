﻿using System.Text;
using UI;
using UnityEngine;
using UnityEngine.UI;

public class LogCatController : MonoBehaviour
{


	public static int[] damageHP = new int[5];
	public static int[] damageShield = new int[5];

	public static string quest;

	public static Text missionText;

	// Use this for initialization
	void Start()
	{
		missionText = GetComponentInChildren<Text>();

		missionText.text = "Борт-компьютер";
		damageHP[(int)MobsEnum.SHOOTER] = 0;
		damageHP[(int)MobsEnum.ELECTRIC] = 0;
		damageHP[(int)MobsEnum.KAMIKAZE] = 0;
		damageHP[(int)MobsEnum.SNIPER] = 0;
		damageHP[(int)MobsEnum.TANK] = 0;

		damageShield[(int)MobsEnum.SHOOTER] = 0;
		damageShield[(int)MobsEnum.ELECTRIC] = 0;
		damageShield[(int)MobsEnum.KAMIKAZE] = 0;
		damageShield[(int)MobsEnum.SNIPER] = 0;
		damageShield[(int)MobsEnum.TANK] = 0;

	}
	
	// Update is called once per frame
	void Update()
	{
		missionText.text = "";
		missionText.text = "Борт-компьютер";

		if (damageHP[(int)MobsEnum.SHOOTER] != 0 || damageShield[(int)MobsEnum.SHOOTER] != 0 || damageHP[(int)MobsEnum.ELECTRIC] != 0
		    || damageShield[(int)MobsEnum.ELECTRIC] != 0 || damageHP[(int)MobsEnum.SNIPER] != 0 || damageShield[(int)MobsEnum.SNIPER] != 0
		    || damageHP[(int)MobsEnum.KAMIKAZE] != 0 || damageShield[(int)MobsEnum.KAMIKAZE] != 0 || damageHP[(int)MobsEnum.TANK] != 0 || damageShield[(int)MobsEnum.TANK] != 0)
		{
			missionText.text = "\nЛог урона: ХП | Щит";
		}
		if (damageHP[(int)MobsEnum.SHOOTER] != 0 || damageShield[(int)MobsEnum.SHOOTER] != 0)
		{
			missionText.text += "\nШтурмовики: " + damageHP[(int)MobsEnum.SHOOTER] + " | " + damageShield[(int)MobsEnum.SHOOTER];
		}

		if (damageHP[(int)MobsEnum.ELECTRIC] != 0 || damageShield[(int)MobsEnum.ELECTRIC] != 0)
		{
			missionText.text += "\nЭлектрики: " + damageHP[(int)MobsEnum.ELECTRIC] + " | " + damageShield[(int)MobsEnum.ELECTRIC];
		}

		if (damageHP[(int)MobsEnum.SNIPER] != 0 || damageShield[(int)MobsEnum.SNIPER] != 0)
		{
			missionText.text += "\nСнайперы: " + damageHP[(int)MobsEnum.SNIPER] + " | " + damageShield[(int)MobsEnum.SNIPER];
		}

		if (damageHP[(int)MobsEnum.KAMIKAZE] != 0 || damageShield[(int)MobsEnum.KAMIKAZE] != 0)
		{
			missionText.text += "\nКамикадзе: " + damageHP[(int)MobsEnum.KAMIKAZE] + " | " + damageShield[(int)MobsEnum.KAMIKAZE];
		}

		if (damageHP[(int)MobsEnum.TANK] != 0 || damageShield[(int)MobsEnum.TANK] != 0)
		{
			missionText.text += "\nТанки: " + damageHP[(int)MobsEnum.TANK] + " | " + damageShield[(int)MobsEnum.TANK];
		}

		if (quest != "")
		{
			missionText.text += quest;
		}
	}

}
