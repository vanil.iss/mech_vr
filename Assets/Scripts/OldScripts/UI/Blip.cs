﻿using UnityEngine;
using System.Collections;

public class Blip : MonoBehaviour {

	public Transform target;
	public bool inBounds;
	public float minScale = 1;
	public float speed = 1;

	private RectTransform rect;
	private MiniMap_old map;

	void Start()
	{
		map = GetComponentInParent<MiniMap_old>();
		rect = GetComponent<RectTransform>();
	}

	void LateUpdate()
	{
		if (map == null)
			Debug.Log ("Map is NULL");

		Vector2 pos = map.TransformPosition(target.position);
		pos *= speed;
		if(inBounds) pos = map.MoveInside(pos);
		float scale = Mathf.Max(minScale, map.zoom);
		rect.localScale = new Vector3(scale, scale, 1);
		rect.localEulerAngles = map.TransformRotation(target.eulerAngles);
		rect.localPosition = pos;
	}
}
