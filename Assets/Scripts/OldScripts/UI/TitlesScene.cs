﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitlesScene : MonoBehaviour {

	public GameObject[] movingObject = new GameObject[5];

	private RectTransform[] elemets;
	Vector3 vector;
	public float speed = 0.5f;
	// Use this for initialization
	void Start () {
		vector = new Vector3 (0,0.35f*speed, 0);
	}


	// Update is called once per frame
	void Update () {
		foreach(GameObject obj in movingObject){
			obj.GetComponent<RectTransform>().position += vector;
		}

		if (Input.GetKeyDown (KeyCode.Escape))
			Application.LoadLevel (0);
	}

}
