﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Pause : MonoBehaviour
{

	private GameObject player;
	public float times;
	public bool isPause;
	public GameObject menu;
	public CrossHair crossHair;
	private RaycastHit hit;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player");

		isPause = false;
		times = 1f;
		crossHair = gameObject.GetComponent<CrossHair>();

		//Отрубить
//        OVRTouchpad.Create();
//        OVRTouchpad.TouchHandler += HandleTouchHandler;
		//End
	}

	// Update is called once per frame
	void Update()
	{
//        if (Input.GetKeyDown(KeyCode.Escape)) {
//            isPause = true;
//            pauseActivate();
//        }

		if (isPause)
		{
			if (Input.GetMouseButtonDown(0))
			{
				if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 1000f))
				{
					if (hit.collider.name == "Resume")
					{
						isPause = false;
						pauseActivate();
					}
					else if (hit.collider.name == "Quit")
					{
						Time.timeScale = 1.0f;
						SceneManager.LoadScene("LiftScene");
					}

				}
			}

		}

	}

	public void ResumeBtn(bool state)
	{
		isPause = state;
		pauseActivate();
	}

	public void QuitBtn()
	{
		Application.LoadLevel(0);
	}

	//Отрубить
	//    void HandleTouchHandler(object sender, System.EventArgs e) {
	//        OVRTouchpad.TouchArgs touchArgs = (OVRTouchpad.TouchArgs)e;
	//        OVRTouchpad.TouchEvent touchEvent = touchArgs.TouchType;
	//        if (touchArgs.TouchType == OVRTouchpad.TouchEvent.SingleTap) {
	//            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 1000f)) {
	//
	//                if (hit.collider.name == "Resume") {
	//                    isPause = false;
	//                    pauseActivate();
	//                } else if (hit.collider.name == "Quit") {
	//                    Application.LoadLevel(0);
	//                }
	//
	//            }
	//
	//        }
	//    }
	//End

	public void pauseActivate()
	{
		if (isPause)
		{
			if (crossHair != null)
				crossHair.enabled = false;
			Cursor.lockState = CursorLockMode.None;
			Time.timeScale = 0f;
			if (!player.GetComponent<GlobalInterfaceController>().sf_active)
				player.GetComponent<GlobalInterfaceController>().changeCameras();
			player.GetComponent<GlobalInterfaceController>().enabled = false;

			menu.SetActive(true);
		}
		else
		{
			if (crossHair != null)
				crossHair.enabled = true;

			Cursor.lockState = CursorLockMode.Locked;
			Time.timeScale = 1f;
			player.GetComponent<GlobalInterfaceController>().enabled = true;

			menu.SetActive(false);
		}
	}

	//TODO Затемнение
}
