﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour {


	public GameObject mapIcon;

	public Data data;
	int lastCount = 0;
	public List<GameObject> mobListWithIcons;

	// Use this for initialization
	void Start () {

		data = GameObject.FindGameObjectWithTag("Data").GetComponent<Data>();
		lastCount = data.mobList.Count;
		mobListWithIcons = data.mobList;

		foreach(GameObject obj in data.mobList){

			GameObject icon = Instantiate (mapIcon);
			icon.GetComponent<Blip> ().target = obj.transform;
			icon.transform.SetParent(transform);
		}
	}

	// Update is called once per frame
	void Update () {

		if (lastCount != data.mobList.Count) {
			lastCount = data.mobList.Count;

			foreach (GameObject obj in data.mobList) {
				if (!mobListWithIcons.Contains (obj)) {
					GameObject icon = Instantiate (mapIcon);
					icon.GetComponent<Blip> ().target = obj.transform;
					icon.transform.SetParent (transform);
					mobListWithIcons.Add (obj);
				}
			}
		}

		}



}
