﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndTrain : MonoBehaviour {
	
	int pos;
	bool[] isPos = new bool[10];
	string word;

	public GameObject guiTextLink;
	public float speedOneLetter = 0.1f;

	public AudioClip printMachine;


	void Start () {
		pos = 0;	
		GetComponent<AudioSource> ().Pause ();

	}


	// Update is called once per frame
	void Update () {
		switch (pos) {
		case 0:
			if (isPos [pos] == false) {
				GetComponent<AudioSource> ().clip = printMachine;

				Color colorStart = guiTextLink.GetComponent<Text>().color;
				colorStart.a = 1f;
				guiTextLink.GetComponent<Text> ().color = colorStart;

				guiTextLink.GetComponent<Text> ().text = "";
				guiTextLink.GetComponent<Text> ().fontSize = 26;
				word = "Поздравляем вас с успешным прохождением обучения.";

				GetComponent<AudioSource> ().Play ();

				isPos [pos] = true;
				StartCoroutine (Printer ());
			}
			break;
		case 1:
			if (isPos [pos] == false) {
				guiTextLink.GetComponent<Text> ().text = "";
				guiTextLink.GetComponent<Text> ().fontSize = 26;
				word = "Мы перебрасываем вас в City of Devil.";

				GetComponent<AudioSource> ().Play ();

				isPos [pos] = true;
				StartCoroutine (Printer ());

			}
			break;
		case 2:
			if (isPos [pos] == false) {
				guiTextLink.GetComponent<Text> ().text = "";
				guiTextLink.GetComponent<Text> ().fontSize = 26;
				word = "Ваша цель:";

				GetComponent<AudioSource> ().Play ();

				isPos [pos] = true;
				StartCoroutine (Printer ());

			}
			break;
		case 3:
			if (isPos [pos] == false) {
				guiTextLink.GetComponent<Text> ().text = "";
				guiTextLink.GetComponent<Text> ().fontSize = 26;

				word = "Спасти человечество от маркетингового порабощения";
				guiTextLink.GetComponent<Text> ().text = word;

				GetComponent<AudioSource> ().Pause ();

				isPos [pos] = true;
				StartCoroutine (EffectingAppearance ());
			}
			break;
		case 4:
			Application.LoadLevel (4);
			break;
		default:
			break;	
		}
	}

	IEnumerator Printer (){
		Color colorStart = guiTextLink.GetComponent<Text>().color;
		colorStart.a = 1f;
		guiTextLink.GetComponent<Text> ().color = colorStart;

		for (int i = 0; i < word.Length; i++) {
			yield return new WaitForSecondsRealtime (speedOneLetter);
			guiTextLink.GetComponent<Text>().text = guiTextLink.GetComponent<Text>().text + word[i];
		}	

		GetComponent<AudioSource> ().Pause ();

		yield return new WaitForSecondsRealtime (2f);

		pos++;
	}

	IEnumerator PrinterDumping (){
		Color colorStart = guiTextLink.GetComponent<Text>().color;
		colorStart.a = 1f;
		guiTextLink.GetComponent<Text> ().color = colorStart;

		for (int i = 0; i < word.Length; i++) {
			yield return new WaitForSecondsRealtime (speedOneLetter);
			guiTextLink.GetComponent<Text>().text = guiTextLink.GetComponent<Text>().text + word[i];

		}	
		GetComponent<AudioSource> ().Pause ();

		yield return new WaitForSecondsRealtime (2f);

		while (guiTextLink.GetComponent<Text> ().color.a > 0f) {
			yield return new WaitForSecondsRealtime (0.05f);
			Color color = guiTextLink.GetComponent<Text>().color;
			color.a -= 0.1f;
			guiTextLink.GetComponent<Text> ().color = color;
		}

		pos++;
	}

	IEnumerator EffectingAppearance (){
		Color colorStart = guiTextLink.GetComponent<Text>().color;
		colorStart.a = 0f;
		guiTextLink.GetComponent<Text> ().color = colorStart;

		while (guiTextLink.GetComponent<Text> ().color.a <= 1f) {
			yield return new WaitForSecondsRealtime (0.05f);
			Color color = guiTextLink.GetComponent<Text>().color;
			color.a += 0.1f;
			guiTextLink.GetComponent<Text> ().color = color;
		}



		while (guiTextLink.GetComponent<Text> ().color.a > 0f) {
			yield return new WaitForSecondsRealtime (0.05f);
			Color color = guiTextLink.GetComponent<Text>().color;
			color.a -= 0.1f;
			guiTextLink.GetComponent<Text> ().color = color;
		}


		while (guiTextLink.GetComponent<Text> ().color.a <= 1f) {
			yield return new WaitForSecondsRealtime (0.05f);
			Color color = guiTextLink.GetComponent<Text>().color;
			color.a += 0.1f;
			guiTextLink.GetComponent<Text> ().color = color;
		}

		while (guiTextLink.GetComponent<Text> ().color.a > 0f) {
			yield return new WaitForSecondsRealtime (0.05f);
			Color color = guiTextLink.GetComponent<Text>().color;
			color.a -= 0.1f;
			guiTextLink.GetComponent<Text> ().color = color;
		}


		while (guiTextLink.GetComponent<Text> ().color.a <= 1f) {
			yield return new WaitForSecondsRealtime (0.05f);
			Color color = guiTextLink.GetComponent<Text>().color;
			color.a += 0.1f;
			guiTextLink.GetComponent<Text> ().color = color;
		}


		pos++;
	}
}

