﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace VRStandardAssets.Utils
{
	// This class simply allows the user to return to the main menu.
	public class ReturnToMainMenu : MonoBehaviour
	{
		private Pause pause;
		[SerializeField] private string m_MenuSceneName = "LiftScene";
		// The name of the main menu scene.
		[SerializeField] private VRInput m_VRInput;
		// Reference to the VRInput in order to know when Cancel is pressed.
		// [SerializeField] private VRCameraFade m_VRCameraFade;           // Reference to the script that fades the scene to black.

		void Start()
		{
			try
			{
				pause = GameObject.FindGameObjectWithTag("Pause").GetComponent<Pause>();
			}
			catch
			{

			}
		}

		private void OnEnable()
		{
			m_VRInput.OnCancel += HandleCancel;
		}


		private void OnDisable()
		{
			m_VRInput.OnCancel -= HandleCancel;

		}


		private void HandleCancel()
		{
			pause.isPause = !pause.isPause;
			pause.pauseActivate();
		}



	}
}