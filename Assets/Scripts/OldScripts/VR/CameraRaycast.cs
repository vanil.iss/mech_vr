﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRaycast : MonoBehaviour
{

	RaycastHit hit;
	public LiftScript lift;
	// Use this for initialization
	void Start()
	{
		OVRTouchpad.Create();
		OVRTouchpad.TouchHandler += HandleTouchHandler;
	}

	void HandleTouchHandler(object sender, System.EventArgs e)
	{
		OVRTouchpad.TouchArgs touchArgs = (OVRTouchpad.TouchArgs)e;
		if (touchArgs.TouchType == OVRTouchpad.TouchEvent.SingleTap)
		{
			if (Physics.Raycast(gameObject.transform.position, gameObject.transform.forward, out hit, 1000f))
			{
				if (hit.collider.name == "ButtonOK")
					lift.NewGame();
			}
			//TODO: Insert code here to handle a single tap.  Note that there are other TouchTypes you can check for like directional swipes, but double tap is not currently implemented I believe.
		}	
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
}
