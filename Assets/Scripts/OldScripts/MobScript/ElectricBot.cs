﻿using DigitalRuby.LightningBolt;
using UI;
using UnityEngine;
using UnityEngine.AI;

public class ElectricBot : MonoBehaviour
{

	#region ComponentsCash

	private ObjectHealth health;
	private Animator animator;
	private CapsuleCollider mobCollider;
	private NavMeshAgent agent;
	private Pause pause;
	private HealthMech mechHealth;
	private bool isEndSlice = true;
	private GameObject npsElectric;

	#endregion

	private Data data;

	private enum StateBot
	{
		Moving,
		Attacking,
		Death}

	;

	//Управление движением

	[SerializeField]
	private StateBot currentState;
	//Текущее состояние моба
	public int maxHealth;

	#region Move

	private GameObject player;
	//Игрок
	public GameObject facePoint;
	//	public Transform playerHearth;
	//	public Transform gun;
	private float distance;
	//Текущая дистанция до игрока
	public float distanceToFire = 3f;
	//Дистанция необходимая для атаки

	#endregion

	#region Attached

	public LightningBoltScript sparksOnAttack;
	public LineRenderer lineOnAttak;


	private int absorbedEnergy;
	public float rateOfSpeed = 0.5f;
	//Скорострельность
	private float _rateOfSpeed;
	//Время с прошлого выстрела
	private RaycastHit hit;
	//Объект в который произведенно попадание
	private int unitHealth;
	//Здоровье моба
	private ParticleSystem particle;

	public float rateOfSpeedHealth = 1f;
	private float _rateOfSpeedHealth;
	//public GameObject particleSparks;
	//private Light faceLight;

	//public float animTimeBeforeHit = 1f;
	//[SerializeField] private float _animTimeBeforeHit;

	#endregion

	#region Raycast

	[Header("Raycast")]
	[SerializeField] LayerMask raycastMask;

	#endregion

	private bool death = false;
	public bool respawn = false;

	public ParticleSystem deathSparks;

	void Awake()
	{
		health = GetComponent<ObjectHealth>();
		animator = GetComponent<Animator>();
		mobCollider = GetComponent<CapsuleCollider>();
		agent = GetComponent<NavMeshAgent>();
		pause = GameObject.FindGameObjectWithTag("Pause").GetComponent<Pause>();

		maxHealth = health.health;
	}

	void Start()
	{
		respawner();

		data = GameObject.FindGameObjectWithTag("Data").GetComponent<Data>();
		npsElectric = GetComponent<Transform>().Find("NPSElectric").gameObject;
		sparksOnAttack.enabled = false;
		absorbedEnergy = 0;
	}

	// Update is called once per frame
	void Update()
	{
		if (death && !isEndSlice) {
			npsElectric.GetComponent<SkinnedMeshRenderer> ().material.SetFloat ("_SliceAmount", npsElectric.GetComponent<SkinnedMeshRenderer> ().material.GetFloat ("_SliceAmount") + Time.deltaTime * 0.2f);
			isEndSlice = false;
			//Debug.Log ("Slicing");
			if (npsElectric.GetComponent<SkinnedMeshRenderer> ().material.GetFloat ("_SliceAmount") > 1f) {
				isEndSlice = true;
				GetComponent<ElectricBot>().enabled = false;
			}
		} else {
			if (!death && !isEndSlice) {
				npsElectric.GetComponent<SkinnedMeshRenderer> ().material.SetFloat ("_SliceAmount", 0f);
				isEndSlice = true;
				//Debug.Log ("Slice not alpha");
			}
			if (death) {
				isEndSlice = false;
				//Debug.Log ("Slice death");
			}
		}
		if (respawn)
		{
			respawner();

			absorbedEnergy = 0;
			respawn = false;
		}

		if (pause.isPause == false)
		{
			unitHealth = gameObject.GetComponent<ObjectHealth>().health;

			if (currentState != StateBot.Death)
			{
				if (unitHealth <= 0)
				{
					currentState = StateBot.Death;
				}
			}

			if (_rateOfSpeed <= rateOfSpeed)
				_rateOfSpeed += Time.deltaTime;

			if (_rateOfSpeedHealth <= rateOfSpeedHealth)
				_rateOfSpeedHealth += Time.deltaTime;

			distance = Vector3.Distance(player.transform.position, transform.position);

			switch (currentState)
			{
				case StateBot.Moving:


					if (distance <= distanceToFire)
					{
						if (enemySee())
						{
							agent.Stop();
							currentState = StateBot.Attacking;
							sparksOnAttack.enabled = true;
							lineOnAttak.enabled = true;
							return;
						}
					}

					agent.SetDestination(player.transform.position);
					GetComponent<Animator>().SetTrigger("Running");

					break;

				case StateBot.Attacking:
					agent.Stop();

					if (distance > distanceToFire)
					{
						currentState = StateBot.Moving;

						sparksOnAttack.enabled = false;
						lineOnAttak.enabled = false;

						agent.updatePosition = true;
						agent.Resume();

						return;
					}


					if (!enemySee())
					{
						if (hit.collider != null)
						{
							if (hit.collider.tag == "Decoration" || hit.collider.tag == "Block")
							{
								currentState = StateBot.Moving;
								agent.updatePosition = true;
								agent.Resume();
								sparksOnAttack.enabled = false;
								lineOnAttak.enabled = false;

								return;
							}
						}

						agent.updatePosition = false;
						agent.SetDestination(player.transform.position);
						agent.Resume();

						return;
					}


					animator.SetTrigger("Shooting");
					Attached();
					break;

				case StateBot.Death:
					Clear();

					break;
			}
		}
	}


	void Attached()
	{
		if (_rateOfSpeed > rateOfSpeed && mechHealth.shieldWork && mechHealth.powerShield > 0)
		{
			_rateOfSpeed = 0;
			if (Physics.Raycast(facePoint.transform.position, player.transform.position - facePoint.transform.position, out hit, 1000f, raycastMask))
			{
				if (hit.collider.tag == "Player")
				{
					if (absorbedEnergy == 10)
					{
						GetComponent<ObjectHealth>().health += 2;
						absorbedEnergy = 0;
					}
					absorbedEnergy += 2;

					LogCatController.damageShield[(int)MobsEnum.ELECTRIC] += 2;
					mechHealth.energeShot(2);

					if (mechHealth.powerShield < 0)
						mechHealth.powerShield = 0;
				}
			}
		}

		if (_rateOfSpeedHealth > rateOfSpeedHealth && (!mechHealth.shieldWork || mechHealth.powerShield < 0))
		{
			_rateOfSpeedHealth = 0;
			if (Physics.Raycast(facePoint.transform.position, player.transform.position - facePoint.transform.position, out hit, 1000f, raycastMask))
			{
				if (hit.collider.tag == "Player")
				{
					LogCatController.damageHP[(int)MobsEnum.ELECTRIC] += 1;

					mechHealth.justShot(1);
				}
			}
		}


	}

	public bool enemySee()
	{
		RaycastHit hit;

		return (Physics.Raycast(facePoint.transform.position, facePoint.transform.forward, out hit, 1000f, raycastMask)) && (hit.transform.gameObject.tag == "Player");
	}

	private void Clear()
	{
		if (!death)
		{
			animator.SetTrigger("Death");
//data.mobList.Remove(gameObject);

			agent.Stop();
			agent.enabled = false;
			mobCollider.enabled = false;
			health.enabled = false;
			sparksOnAttack.enabled = false;
			lineOnAttak.enabled = false;
			deathSparks.Play();

			death = true;
			//GetComponent<ElectricBot>().enabled = false;
		}
	}

	void respawner()
	{
		player = GameObject.FindWithTag("Player");
		mechHealth = player.GetComponent<HealthMech>();

		agent = GetComponent<NavMeshAgent>();
		agent.enabled = true;
		mobCollider.enabled = true;
		health.enabled = true;
		animator.enabled = true;

		currentState = StateBot.Moving;
		death = false;
	}
}