﻿using UI;
using UnityEngine;
using UnityEngine.AI;

public class MovingBot : MonoBehaviour
{

	#region ComponentsCash

	private ObjectHealth health;
	private Animator animator;
	private CapsuleCollider mobCollider;
	private NavMeshAgent agent;
	private Pause pause;
	private AudioSource source;
	private HealthMech healthMech;
	private bool isEndSlice = true;
	private GameObject nps;

	#endregion


	private Data data;

	private enum StateBot
	{
		Moving,
		Attacking,
		Escaping,
		Death}

	;
	//Состояния моба

	private enum StateHit
	{
		Hit,
		Miss,
		Doubling}

	;
	//Состояния выстрела

	//Управление движением
	[SerializeField]
	private Transform target;
	//Цель движения

	[SerializeField]
	private StateBot currentState;
	//Текущее состояние моба

	#region Move

	[Header("Move")]
	private GameObject player;
	//Игрок
	public GameObject facePoint;
	//	public Transform playerHearth;
	//	public Transform gun;
	private float distance;
	//Текущая дистанция до игрока
	public float distanceToFire = 3f;
	//Дистанция необходимая для атаки

	#endregion


	//Attacking

	#region Attached

	[Header("Attached")]
	public float rateOfSpeed = 0.5f;
	//Скорострельность
	private float _rateOfSpeed;
	//Время с прошлого выстрела
	private RaycastHit hit;
	//Объект в который произведенно попадание
	public int maxHealth;
	//Максмум здоровья моба
	private int unitHealth;
	//Здоровье моба
	private ParticleSystem particle;

	public GameObject particleSparks;
	private Light faceLight;

	public float animTimeBeforeHit = 1f;
	[SerializeField]
	private float _animTimeBeforeHit;
	public AudioClip shootSing;

	#endregion

	#region Raycast

	[Header("Raycast")]
	[SerializeField] LayerMask raycastMask;

	#endregion


	//Escaping

	//public GameObject shelterGroup; //Группа укрытий
	//private Transform[] shelters; //Массив укрытий в группе
	//public Transform gunMech; //Точка выстрела пулемёта для проверки укрытия

	#region Escape

	[Header("Escape")]
	private CoverScript coverScript;
	[SerializeField]
	private bool shelterDestroy = false;
	[SerializeField]
	private int coverNum = -1;
	[SerializeField]
	private float timeHPRecovery;

	#endregion

	public AudioClip deathMob;

	private bool death = false;
	public bool respawn = false;
	// Use this for initialization

	void Awake()
	{
		try
		{
			health = GetComponent<ObjectHealth>();
			animator = GetComponent<Animator>();
			mobCollider = GetComponent<CapsuleCollider>();
			agent = GetComponent<NavMeshAgent>();
			pause = GameObject.FindGameObjectWithTag("Pause").GetComponent<Pause>();
			source = GetComponent<AudioSource>();

			maxHealth = health.health;
		}
		catch
		{
			
		}

	}

	void Start()
	{
		respawner();
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<Data>();
		nps = GetComponent<Transform>().Find("NPS").gameObject;
		particle = GetComponentInChildren<ParticleSystem>();
		particleSparks.GetComponent<ParticleSystem>().Stop();
		faceLight = particleSparks.GetComponentInChildren<Light>();
		faceLight.enabled = false;


	}

	// Update is called once per frame
	void Update()
	{


		if (death && !isEndSlice)
		{
			nps.GetComponent<SkinnedMeshRenderer>().material.SetFloat("_SliceAmount", nps.GetComponent<SkinnedMeshRenderer>().material.GetFloat("_SliceAmount") + Time.deltaTime * 0.2f);
			isEndSlice = false;
			//Debug.Log ("Slicing");
			if (nps.GetComponent<SkinnedMeshRenderer>().material.GetFloat("_SliceAmount") > 1f)
			{
				isEndSlice = true;
				GetComponent<MovingBot>().enabled = false;
			}
		}
		else
		{
			if (!death && !isEndSlice)
			{
				nps.GetComponent<SkinnedMeshRenderer>().material.SetFloat("_SliceAmount", 0f);
				isEndSlice = true;
				//Debug.Log ("Slice not alpha");
			}
			if (death)
			{
				isEndSlice = false;
				//Debug.Log ("Slice death");
			}
		}
		unitHealth = health.health;

		if (respawn)
		{
			respawner();
			respawn = false;
		}


		if (!pause.isPause)
		{

			if (currentState != StateBot.Death)
			{
				if (unitHealth <= 0)
				{
					agent.enabled = true;
					source.PlayOneShot(data.getDeathUnitAudioClip());
					currentState = StateBot.Death;
				}
				else if (unitHealth <= (maxHealth / 2))
				{
					if (TakeCover())
					{
						agent.enabled = true;
						agent.Stop();
						agent.SetDestination(data.coversList[coverNum].transform.position);
						agent.Resume();
						currentState = StateBot.Escaping;
					}
				}
			}

			if (_rateOfSpeed <= rateOfSpeed)
				_rateOfSpeed += Time.deltaTime;

			distance = Vector3.Distance(player.transform.position, transform.position);

			switch (currentState)
			{
				case StateBot.Moving:
					agent.updatePosition = true;

					_animTimeBeforeHit = 0f;

					if (distance <= distanceToFire && enemySee())
					{
						currentState = StateBot.Attacking;
						agent.enabled = false;
						//	agent.Stop();

						return;
					}


					agent.SetDestination(player.transform.position);
					animator.SetTrigger("Running");

					break;

				case StateBot.Attacking:
				//	agent.Stop();
					if (distance > distanceToFire)
					{
						currentState = StateBot.Moving;
						agent.enabled = true;
						return;
					}

					if (!enemySee())
					{

						if (hit.collider != null)
						{
							if (hit.collider.tag == "Decoration" || hit.collider.tag == "Block")
							{
								currentState = StateBot.Moving;

								agent.enabled = true;

								return;
							}
						}

						Vector3 relativePos = player.transform.position - transform.position;
						Quaternion rotation = Quaternion.LookRotation(relativePos);
						rotation.y = transform.position.y;
						transform.rotation = rotation;

//                        Vector3 lookPos =  player.transform.position - transform.position;
//
//                        if (lookPos.magnitude != 0) {
//                            Quaternion rotation = Quaternion.LookRotation(lookPos);
//
//                            rotation.y = 0;
//
//                            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 0.115f);
//                        }
//						return;
					}

                    //Приближение к убегающему врагу


					if (_animTimeBeforeHit >= animTimeBeforeHit)
					{
						faceLight.enabled = true;

						particleSparks.GetComponent<ParticleSystem>().Stop();
						particleSparks.GetComponent<ParticleSystem>().Play();
					}
					else
						_animTimeBeforeHit += Time.deltaTime;

					animator.SetTrigger("Shooting");
					Attached();

					faceLight.enabled = false;

					break;

				case StateBot.Escaping:
					_animTimeBeforeHit = 0f;
                    /*
                    if (distance <= distanceToFire) {
                        Attached ();
                    }
                    */

					if (coverNum >= 0)
					{
						GameObject cover = data.coversList[coverNum];
						ObjectHealth healthParent = cover.GetComponentInParent<ObjectHealth>();
						if (healthParent != null)
						{
							if (healthParent.health <= 1)
							{
								agent.Stop();
								agent.SetDestination(player.transform.position);
								agent.Resume();
								shelterDestroy = true;
								currentState = StateBot.Moving;
								break;
							}
						}


						if (Vector3.Distance(cover.transform.position, transform.position) <= 5)
						{
							if (timeHPRecovery <= 10)
							{
								health.health += 1;
								timeHPRecovery = 0;
							}
							else
								timeHPRecovery += Time.deltaTime;
						}

						if (health.health == maxHealth)
						{
							cover.GetComponent<CoverScript>().full = false;
							agent.Stop();
							agent.SetDestination(player.transform.position);
							agent.Resume();
							currentState = StateBot.Moving;
							return;

						}

						agent.SetDestination(cover.transform.position);
						animator.SetTrigger("Running");


					}
					else
					{
						agent.Stop();
						agent.SetDestination(player.transform.position);
						agent.Resume();
						shelterDestroy = true;
						currentState = StateBot.Moving;
						return;
					}
					break;

				case StateBot.Death:
					Clear();
					break;
				default:
					break;
			}


		}
	}

	private StateHit getAbleToHit()
	{
		int x = 0;
		for (int i = 0; i < 4; i++)
			x += Random.Range(0, 25);

		if (distance <= distanceToFire / 2)
			x += 30;
		else if (distance <= distanceToFire / 4)
			x += 20;
		else if (distance <= distanceToFire)
			x += 10;

		if (currentState == StateBot.Attacking)
			x += 20;

		int n = 0, count = 0;

		if (x >= 125)
			return StateHit.Doubling;
		else if (x >= 100)
			return StateHit.Hit;
		else if (x >= 90)
			n = 4;
		else if (x >= 70)
			n = 3;
		else if (x >= 50)
			n = 2;
		else
			n = 1;

		for (int i = 0; i < n; i++)
			count += Random.Range(-1, 2);

		if (count > 0)
		{
			return StateHit.Hit;
		}
		return StateHit.Miss;

	}

	void Attached()
	{
		if (_rateOfSpeed > rateOfSpeed)
		{
			_rateOfSpeed = 0;
			if (Physics.Raycast(facePoint.transform.position, player.transform.position - facePoint.transform.position, out hit, 1000f, raycastMask))
			{
				if (hit.collider.tag == "Player")
				{
					source.PlayOneShot(shootSing);

					StateHit stateShot = getAbleToHit();
					if (stateShot == StateHit.Hit)
					{
						if (healthMech.shieldWork && healthMech.powerShield > 0)
							LogCatController.damageShield[(int)MobsEnum.SHOOTER] += 1;
						else
							LogCatController.damageHP[(int)MobsEnum.SHOOTER] += 1;

						player.GetComponent<HealthMech>().justShot(1);
                                
                                					}
                                					else if (stateShot == StateHit.Doubling)
                                					{
                                						if (healthMech.shieldWork && healthMech.powerShield > 0)
                                							LogCatController.damageShield[(int)MobsEnum.SHOOTER] += 4;
                                						else
                                							LogCatController.damageHP[(int)MobsEnum.SHOOTER] += 4;
                                
                                						player.GetComponent<HealthMech>().justShot(4);
                                
                                					}
                                				}
                                			}
                                		}
                                	}
	
                                
                                	public bool enemySee()
                                	{
                                		return (Physics.Raycast(facePoint.transform.position, facePoint.transform.forward, out hit, 1000f, raycastMask)) && (hit.transform.gameObject.tag == "Player");
                                	}
                                
                                	private bool TakeCover()
                                	{
                                		if (data.coversList.Count == 0)
                                		{
                                			return false;
                                		}
                                
                                		float[] coverDistance = new float[data.coversList.Count];
                                
                                
                                		int i = 0;
                                		foreach (GameObject cover in data.coversList)
                                		{
                                			coverDistance[i] = Vector3.Distance(this.transform.position, cover.transform.position);
                                			i++;
                                		}
                                
                                		float[] sortedDistance = (float[])coverDistance.Clone();
                                		System.Array.Sort(sortedDistance);
                                
                                		if (sortedDistance[0] <= 200)
                                		{
                                			for (int k = 0; k < data.coversList.Count; k++)
                                			{
                                				if (sortedDistance[k] > 200)
                                					return false;
                                				int j = System.Array.IndexOf(coverDistance, sortedDistance[k]);
                                				coverScript = data.coversList[j].GetComponent<CoverScript>();
                                				if (coverScript.safe && !coverScript.full)
                                				{
                                					coverScript.full = true;
                                					coverNum = j;
                                					return true;
                                				}
                                			}
                                		}
                                
                                
                                		return false;
                                	}
                                
                                	private void Clear()
                                	{
                                		if (!death)
                                		{
                                			if (animator.GetBool("Running"))
                                				animator.SetBool("Running", false);
                                
                                
                                
                                			animator.SetTrigger("Death");
                                			//data.mobList.Remove(gameObject);
                                			if (coverNum > 0 && coverScript != null)
                                			{
                                				data.coversList[coverNum].GetComponent<CoverScript>().full = false;
                                				coverNum = -1;
                                			}
                                			agent.Stop();
                                
                                			agent.enabled = false;
                                			mobCollider.enabled = false;
                                			health.enabled = false;
                                			source.enabled = false;
                                			death = true;
                                			Statistic.countKilled++;
                                		}
                                	}
                                
                                	void respawner()
                                	{
                                		player = GameObject.FindWithTag("Player");
                                		healthMech = player.GetComponent<HealthMech>();
                                		agent = GetComponent<NavMeshAgent>();
                                		agent.enabled = true;
                                		mobCollider.enabled = true;
                                		health.enabled = true;
                                		animator.enabled = true;
		source.enabled = true;

		currentState = StateBot.Moving;
		death = false;
	}
}
