using System.Collections;
using UnityEngine;

public class RandomSpawnMobs : MonoBehaviour
{

	private Data data;

	public int countShooters;
	public int countTank;
	public int countElectric;
	public int countKamikaze;

	public Transform[] spawnPoint;

	// Use this for initialization
	void Start()
	{

		data = GameObject.FindGameObjectWithTag("Data").GetComponent<Data>();
	}

	void OnTriggerEnter(Collider player)
	{
		if (player.tag == "Player")
		{
			StartCoroutine(Spawn());

			GetComponent<BoxCollider>().enabled = false;

		}

	}

	void OnTriggerExit(Collider other)
	{
		StartCoroutine(ExitForTime());
	}

	IEnumerator Spawn()
	{
		while (countShooters != 0 || countTank != 0 || countElectric != 0 || countKamikaze != 0)
		{
			if (countShooters > 0)
			{
				GameObject shooters = data.shooterPool.Spawn(spawnPoint[Random.Range(0, spawnPoint.Length)].position, spawnPoint[Random.Range(0, spawnPoint.Length)].rotation);

				shooters.GetComponent<MobModule> ().deathCmd.Disable ();
				countShooters--;
				//data.mobList.Add(shooters);
				yield return new WaitForSecondsRealtime(3f);
			}

			if (countTank > 0)
			{
				GameObject tanks = Instantiate(data.tank);
				tanks.transform.position = spawnPoint[Random.Range(0, spawnPoint.Length)].position;

				countTank--;
				//data.mobList.Add(tanks);

				yield return new WaitForSecondsRealtime(3f);
			}

			if (countElectric > 0)
			{
				GameObject electric = data.electricPool.Spawn(spawnPoint[Random.Range(0, spawnPoint.Length)].position, spawnPoint[Random.Range(0, spawnPoint.Length)].rotation);

				electric.GetComponent<ElectricBot>().enabled = true;

				countElectric--;
				//data.mobList.Add(electrics);

				yield return new WaitForSecondsRealtime(3f);
			}

			if (countKamikaze > 0)
			{
				GameObject kamikaze = data.kamikazePool.Spawn(spawnPoint[Random.Range(0, spawnPoint.Length)].position, spawnPoint[Random.Range(0, spawnPoint.Length)].rotation);


				kamikaze.GetComponent<KamikazeBot>().enabled = true;

				countKamikaze--;
				//data.mobList.Add(kamikazes);

				yield return new WaitForSecondsRealtime(3f);
			}
		}
	}

	IEnumerator ExitForTime()
	{
		yield return new WaitForSecondsRealtime(60f);

		GetComponent<BoxCollider>().enabled = true;

	}

}
