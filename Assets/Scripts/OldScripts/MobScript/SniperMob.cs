﻿using UI;
using UnityEngine;

public class SniperMob : MonoBehaviour
{

	#region ComponentsCash

	private ObjectHealth health;
	private CapsuleCollider mobCollider;
	private Pause pause;

	#endregion

	private Data data;

	private enum StateBot
	{
		Leave,
		Death}

	;

	private GameObject player;
	private StateBot currentState;
	//Текущее состояние моба

	private float distance;
	//Текущая дистанция до игрока
	public float distanceToFire = 3f;
	//Дистанция необходимая для атаки
	private int unitHealth;
	//Здоровье моба

	public float speedRotation = 100.0f;
	public float shootAngleDistance = 10.0f;
	public float rateOfSpeed = 0.5f;
	//Скорострельность
	private float _rateOfSpeed;
	//Время с прошлого выстрела
	private RaycastHit hit;

	public GameObject facePoint;

	private bool death = false;

	void Awake()
	{
		health = GetComponent<ObjectHealth>();
		mobCollider = GetComponent<CapsuleCollider>();
		pause = GameObject.FindGameObjectWithTag("Pause").GetComponent<Pause>();

//  maxHealth = health.health;
	}

	// Use this for initialization
	void Start()
	{
		respawner();
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<Data>();
		data.mobList.Add(gameObject);

		player = GameObject.FindWithTag("Player");
		currentState = StateBot.Leave;

	}

	// Update is called once per frame
	void Update()
	{
		if (pause.isPause == false)
		{

			unitHealth = health.health;

			if (currentState != StateBot.Death)
			{
				if (unitHealth <= 0)
				{
					currentState = StateBot.Death;
				}
			}


			if (_rateOfSpeed <= rateOfSpeed)
				_rateOfSpeed += Time.deltaTime;


			distance = Vector3.Distance(player.transform.position, transform.position);
			switch (currentState)
			{
				case StateBot.Leave:
					Vector3 relativePos = player.transform.position - transform.position;
					Quaternion rotation = Quaternion.LookRotation(relativePos);
					transform.rotation = rotation;

					if (distance <= distanceToFire)
					{
						if (_rateOfSpeed > rateOfSpeed)
						{
							_rateOfSpeed = 0;

							if (player.GetComponent<HealthMech>().shieldWork && player.GetComponent<HealthMech>().powerShield > 0)
							{
								LogCatController.damageShield[(int)MobsEnum.SNIPER] += 2;
								LogCatController.damageHP[(int)MobsEnum.SNIPER] += 2;
							}
							else
								LogCatController.damageHP[(int)MobsEnum.SNIPER] += 4;

							player.GetComponent<HealthMech>().hardShot(4);
						}
					}

					break;
				case StateBot.Death:
					Clear();
					break;
			}
		}
	}

	private void Clear()
	{
		if (!death)
		{
			GetComponent<Animator>().SetTrigger("Death");
			//data.mobList.Remove(gameObject);

			mobCollider.enabled = false;
			health.enabled = false;

			death = true;

			GetComponent<SniperMob>().enabled = false;
		}
	}

	void respawner()
	{
		player = GameObject.FindWithTag("Player");

		mobCollider.enabled = true;
		health.enabled = true;

		currentState = StateBot.Leave;
		death = false;
	}
}
