﻿using UI;
using UnityEngine;
using UnityEngine.AI;

public class TankBot : MonoBehaviour
{

	#region ComponentsCash

	private ObjectHealth health;
	private Animator animator;
	private BoxCollider mobCollider;
	private NavMeshAgent agent;
	private Pause pause;
	private AudioSource source;

	#endregion

	private Data data;


	private enum StateBot
	{
		Moving,
		Attacking,
		Death}

	;
	//Состояния моба

	private StateBot currentState;
	//Текущее состояние моба

	//Moving
	private GameObject player;
	//Игрок

	private float distance;
	//Текущая дистанция до игрока
	private float addDist;
	//Приближение к убегающему противнику
	public float distanceToFire = 3f;
	//Дистанция необходимая для атаки

	//Attacking
	public float rateOfSpeed = 5.5f;
	//Скорострельность
	private float _rateOfSpeed;
	//Время с прошлого выстрела
	private RaycastHit hit;
	//Объект в который произведенно попадание
	private int maxHealth;
	//Максмум здоровья моба
	private int unitHealth;
	//Здоровье моба

	public GameObject particleSparks;
	public AudioClip[] shootClips;

	private bool death = false;
	public  ParticleSystem deathSparks;

	void Awake()
	{
		health = transform.GetChild(0).GetComponent<ObjectHealth>();
		animator = GetComponent<Animator>();
		mobCollider = transform.GetChild(0).GetComponent<BoxCollider>();
		agent = GetComponent<NavMeshAgent>();
		pause = GameObject.FindGameObjectWithTag("Pause").GetComponent<Pause>();
		source = GetComponent<AudioSource>();
		maxHealth = health.health;
	}

	void Start()
	{
		respawner();

		data = GameObject.FindGameObjectWithTag("Data").GetComponent<Data>();
		data.mobList.Add(gameObject);


		particleSparks.GetComponent<ParticleSystem>().Stop();

	}

	// Update is called once per frame
	void Update()
	{
		if (pause.isPause == false)
		{
			unitHealth = health.health;

			if (currentState != StateBot.Death)
			{
				if (unitHealth <= 0)
					currentState = StateBot.Death;
			}

			if (_rateOfSpeed <= rateOfSpeed)
				_rateOfSpeed += Time.deltaTime;

			distance = Vector3.Distance(player.transform.position, transform.position);
			switch (currentState)
			{
				case StateBot.Moving:

					if (distance <= distanceToFire)
					{
						if (enemySee())
						{
							agent.Stop();
							currentState = StateBot.Attacking;
							return;
						}
					}

					agent.SetDestination(player.transform.position);
					break;

				case StateBot.Attacking:
					agent.Stop();

					if (!enemySee())
					{
						agent.Resume();
						currentState = StateBot.Moving;
						return;
					}

					if (distance > distanceToFire)
					{
						if (agent.speed <= 30)
							agent.speed += 5;

						agent.Resume();
						currentState = StateBot.Moving;

						return;
					}

					Attached();
					break;
				case StateBot.Death:
					Clear();
					break;
			}

		}
	}


	void Attached()
	{
		if (_rateOfSpeed > rateOfSpeed)
		{
			_rateOfSpeed = 0;
			if (Physics.Raycast(particleSparks.transform.position, player.transform.position - particleSparks.transform.position, out hit, 1000f))
			{
				if (hit.collider.tag == "Player")
				{
					particleSparks.GetComponent<ParticleSystem>().Stop();
					particleSparks.GetComponent<ParticleSystem>().Play();

					if (player.GetComponent<HealthMech>().shieldWork && player.GetComponent<HealthMech>().powerShield > 0)
					{
						LogCatController.damageShield[(int)MobsEnum.TANK] += 5;
						LogCatController.damageHP[(int)MobsEnum.TANK] += 5;
					}
					else
						LogCatController.damageHP[(int)MobsEnum.TANK] += 10;

					player.GetComponent<HealthMech>().hardShot(10);
					source.PlayOneShot(getRandomShoot());
				}
			}
		}
	}

	public bool enemySee()
	{
		RaycastHit hit;
		return (Physics.Raycast(particleSparks.transform.position, particleSparks.transform.forward, out hit)) && (hit.transform.gameObject.tag == "Player");
	}

	private void Clear()
	{
		if (!death)
		{
//			animator.SetTrigger("Death");
			//data.mobList.Remove(gameObject);

			agent.Stop();
			agent.enabled = false;
			mobCollider.enabled = false;
			health.enabled = false;
			source.enabled = false;
			deathSparks.Play();
			death = true;
			GetComponent<TankBot>().enabled = false;
		}
	}

	void respawner()
	{
		player = GameObject.FindWithTag("Player");

		agent = GetComponent<NavMeshAgent>();
		agent.enabled = true;
		mobCollider.enabled = true;
		health.enabled = true;
//        animator.enabled = true;
		source.enabled = true;

		currentState = StateBot.Moving;
		death = false;
	}

	private AudioClip getRandomShoot()
	{
		return shootClips[Random.Range(0, shootClips.Length)];
	}
}
