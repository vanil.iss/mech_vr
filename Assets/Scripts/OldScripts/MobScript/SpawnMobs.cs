using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpawnMobs : MonoBehaviour {


	public int countShooters;
	public int countTank;
	public int countElectric;
	public int countKamikaze;

	public Transform spawnPoint;

	// Use this for initialization
	void Start() {
	}

	void OnTriggerEnter(Collider collider)
	{
		if (collider.tag.Equals("Player")) {
			StartCoroutine(Spawn());

			GetComponent<BoxCollider>().enabled = false;

		}

	}

	IEnumerator Spawn() {
		while (countShooters != 0 || countTank != 0 || countElectric != 0 || countKamikaze != 0)
		{
			if (countShooters > 0){
				GameObject mob = MobPoolManager.Instance.hedgehopperMobPool.Spawn(spawnPoint.position, spawnPoint.rotation);
				mob.GetComponent<Mob>().enabled = true;
				countShooters--;
				
				yield return new WaitForSecondsRealtime(3f);
			}

			if (countTank > 0) {
				GameObject mob = MobPoolManager.Instance.tankMobPool.Spawn(spawnPoint.position, spawnPoint.rotation);
				mob.GetComponent<Mob>().enabled = true;
				countTank--;

				yield return new WaitForSecondsRealtime(3f);
			}

			if (countElectric > 0) {
				GameObject mob = MobPoolManager.Instance.electricMobPool.Spawn(spawnPoint.position, spawnPoint.rotation);
				mob.GetComponent<Mob>().enabled = true;
				countElectric--;

				yield return new WaitForSecondsRealtime(3f);
			}

			if (countKamikaze > 0) {
				GameObject mob = MobPoolManager.Instance.kamikazeMobPool.Spawn(spawnPoint.position, spawnPoint.rotation);
				mob.GetComponent<Mob>().enabled = true;
				countKamikaze--;
				
				yield return new WaitForSecondsRealtime(3f);
			}
		}
	}

}
