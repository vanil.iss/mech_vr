﻿using UnityEngine;
using System.Collections;

public class CoverScript : MonoBehaviour {

	private GameObject player;
    RaycastHit hit;

    public float maxLength = 100f;
	public bool safe;
	public bool full;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {

		if (Vector3.Distance(player.transform.position, transform.position) <= 200f){
            if (Physics.Raycast (transform.position, (player.transform.position - transform.position), out hit, maxLength)) {
                if (hit.transform.gameObject.tag == "Player"){
                    safe = false;
                }
                else{
                    safe = true;
                }
            }
        }

	}
}
