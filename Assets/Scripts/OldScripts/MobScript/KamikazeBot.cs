﻿using UI;
using UnityEngine;
using UnityEngine.AI;

public class KamikazeBot : MonoBehaviour
{

#region ComponentsCash
    private ObjectHealth health;
    private Animator animator;
    private CapsuleCollider mobCollider;
    private NavMeshAgent agent;
    private Pause pause;
	private bool isEndSlice = true;
	private GameObject npsKamikaze;

#endregion

	private Data data;

	private enum StateBot{
		Moving,
		Explosion,
		Death
    };

	private StateBot currentState;
	//Текущее состояние моба
	public int maxHealth;

	#region Move

	private GameObject player;
	//Игрок
	public GameObject facePoint;
	private float distance;
	//Текущая дистанция до игрока
	public float distanceToFire = 3f;
	//Дистанция необходимая для атаки

	#endregion

	public float radiusExplosion = 30f;
	private int unitHealth;
	//Здоровье моба

	private bool death = false;
	public bool respawn = false;
    public  ParticleSystem deathSparks;

	// Use this for initialization
    void Awake() {
        health = GetComponent<ObjectHealth>();
        animator = GetComponent<Animator>();
        mobCollider = GetComponent<CapsuleCollider>();
        agent = GetComponent<NavMeshAgent>();
        pause = GameObject.FindGameObjectWithTag("Pause").GetComponent<Pause>();

        maxHealth = health.health;
    }

	void Start() {
        respawner();
		npsKamikaze = GetComponent<Transform>().Find("NPSKamikaze").gameObject;
        data = GameObject.FindGameObjectWithTag("Data").GetComponent<Data>();

	}
	
	// Update is called once per frame
	void Update()
	{
		if (death && !isEndSlice) {
			npsKamikaze.GetComponent<SkinnedMeshRenderer> ().material.SetFloat ("_SliceAmount", npsKamikaze.GetComponent<SkinnedMeshRenderer> ().material.GetFloat ("_SliceAmount") + Time.deltaTime * 0.2f);
			isEndSlice = false;
			//Debug.Log ("Slicing");
			if (npsKamikaze.GetComponent<SkinnedMeshRenderer> ().material.GetFloat ("_SliceAmount") > 1f) {
				isEndSlice = true;
				GetComponent<KamikazeBot>().enabled = false;
			}
		} else {
			if (!death && !isEndSlice) {
				npsKamikaze.GetComponent<SkinnedMeshRenderer> ().material.SetFloat ("_SliceAmount", 0f);
				isEndSlice = true;
				//Debug.Log ("Slice not alpha");
			}
			if (death) {
				isEndSlice = false;
				//Debug.Log ("Slice death");
			}
		}
		if (respawn){
			respawner();
			respawn = false;
		}

		if (pause.isPause == false)
		{
			unitHealth = health.health;

			if (currentState != StateBot.Death) {
				if (unitHealth <= 0) {
					currentState = StateBot.Death;
				}
			}


			distance = Vector3.Distance(player.transform.position, transform.position);

			switch (currentState)
			{
				case StateBot.Moving:
					if (distance <= distanceToFire)
					{
                        animator.SetTrigger("Death");
						agent.Stop();

						currentState = StateBot.Explosion;
                        return;
					}

                    agent.SetDestination(player.transform.position);
                    animator.SetTrigger("Running");
					break;
				case StateBot.Explosion:
					Collider[] hitColliders = Physics.OverlapSphere(transform.position, radiusExplosion);
					for (int i = 0; i < hitColliders.Length; i++)
					{
                        ObjectHealth healthObject = hitColliders[i].GetComponent<ObjectHealth>();
                        HealthMech healthMech = hitColliders[i].GetComponent<HealthMech>();

                        if (healthObject != null)
                            healthObject.activisionHit(5);
						else if (healthMech != null){

                            if (healthMech.shieldWork && healthMech.powerShield > 0) {
								LogCatController.damageShield[(int)MobsEnum.KAMIKAZE] += 15;
								LogCatController.damageHP[(int)MobsEnum.KAMIKAZE] += 15;
							} else
                                LogCatController.damageHP[(int)MobsEnum.KAMIKAZE] += 30;

                            healthMech.hardShot(30);

                        }
					}
					currentState = StateBot.Death;
					break;
				case StateBot.Death:
					Clear();
					break;
			}
		}
	}

	void OnTriggerEnter(Collider other) {
        if (other.tag == "Player")
        	currentState = StateBot.Explosion;
	}

	private void Clear() {
		if (!death){
            deathSparks.Play();
            animator.SetTrigger("Death");
			//data.mobList.Remove(gameObject);

			agent.Stop();
			agent.enabled = false;
            mobCollider.enabled = false;
			health.enabled = false;

            death = true;
            GetComponent<KamikazeBot>().enabled = false;

		}
	}

    void respawner(){
        player = GameObject.FindWithTag("Player");

        agent = GetComponent<NavMeshAgent>();
        agent.enabled = true;
        mobCollider.enabled = true;
        health.enabled = true;
        animator.enabled = true;

        currentState = StateBot.Moving;
        death = false;
    }
}
