﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;

public class DeathController : MonoBehaviour
{

	private GameObject player;
	private Transform center;

	private GameObject pause;
	public GameObject gameOver;
	public Animator gunAnim;

	RaycastHit hit;

	void Awake()
	{
		try
		{
			player = GameObject.FindGameObjectWithTag("Player");
			pause = GameObject.FindGameObjectWithTag("Pause");
		}
		catch
		{
			
		}
	}

	void Start()
	{
		
		GetComponent<DeathController>().enabled = false;

//        OVRTouchpad.Create();
//        OVRTouchpad.TouchHandler += HandleTouchHandler;
	}

	void Update()
	{
//        int health = player.GetComponent<HealthMech>().health;
//        if (health <= 0) {
//            player.GetComponent<WayPoints>().enabled = false;
//            player.GetComponent<PrimaryMoving>().enabled = false;
//            player.GetComponent<PrimaryMoving>().enabled = false;
//
////player.GetComponentInChildren<GunScript1>().enabled = false;
//            GameObject.FindGameObjectWithTag("Pause").GetComponent<Pause>().enabled = false;
//            GameObject.FindGameObjectWithTag("Pause").GetComponent<CrossHair>().enabled = false;
//            Cursor.lockState = CursorLockMode.None;
//
//            gameOver.SetActive(true);
//        }
		if (Input.GetMouseButtonDown(0))
		{

			if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 1000f))
			{
				if (hit.collider.name == "Return")
				{

					player.GetComponent<WayPoints>().enabled = true;
					gunAnim.Rebind();
					pause.GetComponent<Pause>().enabled = true;
					pause.GetComponent<CrossHair>().enabled = true;
					GetComponent<DeathController>().enabled = false;
					player.GetComponent<GlobalInterfaceController>().enabled = true;
					Camera.main.GetComponent<ReturnToMainMenu>().enabled = true;

					Time.timeScale = 1f;

					Cursor.lockState = CursorLockMode.Locked;

					SceneManager.LoadScene(PlayerPrefs.GetString("lastLevel", "AmmunitionLevel"));
				}
				else if (hit.collider.name == "Quit")
				{

					player.GetComponent<WayPoints>().enabled = true;
					gunAnim.Rebind();
					pause.GetComponent<Pause>().enabled = true;
					pause.GetComponent<CrossHair>().enabled = true;
					GetComponent<DeathController>().enabled = false;
					player.GetComponent<GlobalInterfaceController>().enabled = true;

					Camera.main.GetComponent<ReturnToMainMenu>().enabled = true;

					Time.timeScale = 1f;

					Cursor.lockState = CursorLockMode.Locked;

					SceneManager.LoadScene("LiftScene");
				}
			}
		}

	}

	public void RestartBtn()
	{
		player.GetComponent<WayPoints>().enabled = true;
		gunAnim.Rebind();
		pause.GetComponent<Pause>().enabled = true;
		pause.GetComponent<CrossHair>().enabled = true;
		GetComponent<DeathController>().enabled = false;

		Time.timeScale = 1f;
		Cursor.lockState = CursorLockMode.Locked;

		Application.LoadLevel(0);

	}

	public void QuitBtn()
	{
		Application.LoadLevel(0);
	}

	public void gameEnd()
	{

		player.GetComponent<WayPoints>().enabled = false;
		player.GetComponent<PrimaryMoving>().enabled = false;

		pause.GetComponent<Pause>().isPause = true;
		pause.GetComponent<CrossHair>().enabled = false;
		Cursor.lockState = CursorLockMode.None;
		if (!player.GetComponent<GlobalInterfaceController>().sf_active)
			player.GetComponent<GlobalInterfaceController>().changeCameras();

		Time.timeScale = 0f;
		Camera.main.GetComponent<ReturnToMainMenu>().enabled = false;
		player.GetComponent<GlobalInterfaceController>().enabled = false;
		gameOver.SetActive(true);
	}

	//    private void HandleTouchHandler(object sender, EventArgs e) {
	//        OVRTouchpad.TouchArgs touchArgs = (OVRTouchpad.TouchArgs)e;
	//        OVRTouchpad.TouchEvent touchEvent = touchArgs.TouchType;
	//        if (touchArgs.TouchType == OVRTouchpad.TouchEvent.SingleTap) {
	//            center = GetComponent<GlobalInterfaceController>().getPosMainCamera();
	//            if (Physics.Raycast(center.position, center.forward, out hit, 1000f))
	//            {
	//                if (hit.collider.name == "Restart") {
	//                    player.GetComponent<WayPoints>().enabled = true;
	//                    gunAnim.Rebind();
	//                    pause.GetComponent<Pause>().enabled = true;
	//                    pause.GetComponent<CrossHair>().enabled = true;
	//                    GetComponent<DeathController>().enabled = false;
	//
	//                    Time.timeScale = 1f;
	//                    Cursor.lockState = CursorLockMode.Locked;
	//
	//                    Application.LoadLevel(0);
	//                } else if (hit.collider.name == "Quit") {
	//                    player.GetComponent<WayPoints>().enabled = true;
	//                    gunAnim.Rebind();
	//                    pause.GetComponent<Pause>().enabled = true;
	//                    pause.GetComponent<CrossHair>().enabled = true;
	//                    GetComponent<DeathController>().enabled = false;
	//
	//                    Time.timeScale = 1f;
	//                    Cursor.lockState = CursorLockMode.Locked;
	//
	//                    Application.LoadLevel(0);
	//                }
	//            }
	//
	//        }
	//    }
}