﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrasherControl : MonoBehaviour
{

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}

	void OnTriggerEnter(Collider other)
	{
		ObjectHealth health = other.GetComponent<ObjectHealth>();
		if (other.tag == "Shooter")
			other.GetComponent<ObjectHealth>().activisionHit(health.health);
		else if (other.tag == "Electric")
			other.GetComponent<ObjectHealth>().activisionHit(health.health);
		else if (other.tag == "Kamikaze")
			other.GetComponent<ObjectHealth>().activisionHit(health.health);
		else if (other.tag == "Tank")
		{
			other.GetComponent<ObjectHealth>().activisionHit(health.health);
			GetComponentInParent<HealthMech>().hardShot(25);
		}
		else if (other.tag == "Dish")
		{
			other.GetComponent<ObjectHealth>().activisionHit(other.GetComponent<ObjectHealth>().health);
			MissionTree.countDish--;
		}
		else if (other.tag == "Tree")
		{
			other.GetComponent<ObjectHealth>().activisionHit(other.GetComponent<ObjectHealth>().health);
		}
		else if (other.tag == "Block")
		{
			other.GetComponent<ObjectHealth>().activisionHit(other.GetComponent<ObjectHealth>().health);
		}
	}
}
