﻿using System.Collections;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class MonitorInLift : MonoBehaviour {
    private LiftScript lift;
	private bool newMessageMayBePlay = true;
    public int pos;
    bool[] isPos = new bool[10];
    string word;

    public GameObject guiTextLink;
    public float speedOneLetter = 0.1f;


    public AudioClip printMachine;
	public AudioClip newMessage;

    public Font mechFont;
    public Font generalFont;
	private bool mayChangePosition = true;

    //public AudioClip[] audioList;

// Use this for initialization
    void Start () {
        pos = -1;
        GetComponent<AudioSource> ().Play ();
        lift = GetComponentInParent<LiftScript>();
//	guiTextLink.GetComponent<Text>().text = "42";
    }


// Update is called once per frame
    void Update () {
        switch (pos) {
            case 0:
                if (isPos [pos] == false) {
                    guiTextLink.GetComponent<Text> ().text = "";
                    word = "Новое сообщение от VLL:";
				if (newMessageMayBePlay) {
					GetComponent<AudioSource> ().clip = newMessage;
					GetComponent<AudioSource> ().volume = 1f;
					GetComponent<AudioSource> ().Play ();
					newMessageMayBePlay = false;
				}
				    GetComponent<AudioSource>().clip = newMessage;

                    guiTextLink.GetComponent<Text> ().text = word;
                    guiTextLink.GetComponent<Text> ().font = generalFont;
                    guiTextLink.GetComponent<Text> ().fontSize = 20;

                    isPos [pos] = true;
                    StartCoroutine (EffectingAppearance2 ());

                }
                break;
            case 1:
			    GetComponent<AudioSource> ().volume = 0.05f;
                if (isPos [pos] == false) {
                    GetComponent<AudioSource> ().clip = printMachine;
                    writeGeneralText("Вас не хватало, боец. Мы очень рады вашему возвращению.",16);
                }
                break;
            case 2:
                if (isPos [pos] == false) {
                    writeGeneralText("Ситуация сложилась непростая и нам нужна ваша помощь.", 16);
//                    Color colorStart = guiTextLink.GetComponent<Text>().color;
//                    colorStart.a = 1f;
//                    guiTextLink.GetComponent<Text> ().color = colorStart;
//                    guiTextLink.GetComponent<Text> ().font = generalFont;
                }
                break;
            case 3:
                if (isPos [pos] == false) {
                    writeMechVR();
                }
                break;
            case 4:
                if (isPos [pos] == false) {
                    writeGeneralText("Ваша цель - ослабить и уничтожить маркетинговых поработителей - корпорацию Good Inс.", 14);
                }
                break;
            case 5:
                if (isPos [pos] == false) {
                    writeMechVR();
                }
                break;
            case 6:
                if (isPos [pos] == false) {
                    writeGeneralText("Надеемся, что вы всё ещё за нас и за свободу.", 16);
                }
                break;
		case 7:
			if (mayChangePosition) {
				writeMechVR ();
				mayChangePosition = false;
				//Debug.Log ("Hello!");
			}
//                Application.LoadLevel (2);
                break;
            default:
                break;
        }
    }

    void writeMechVR(){
        word = "Mech VR";

        guiTextLink.GetComponent<Text> ().fontSize = 35;
        guiTextLink.GetComponent<Text> ().font = mechFont;

        guiTextLink.GetComponent<Text> ().text = word;
        isPos [pos] = true;
        lift.pos++;

    }

    void writeGeneralText(string _word, int fontSize) {
        guiTextLink.GetComponent<Text> ().font = generalFont;

        guiTextLink.GetComponent<Text> ().text = "";
        guiTextLink.GetComponent<Text> ().fontSize = fontSize;
        word = _word;

        GetComponent<AudioSource> ().Play ();

        isPos [pos] = true;
        StartCoroutine (Printer ());
    }

    IEnumerator Printer (){
        Color colorStart = guiTextLink.GetComponent<Text>().color;
        colorStart.a = 1f;
        guiTextLink.GetComponent<Text> ().color = colorStart;

        for (int i = 0; i < word.Length; i++) {
            yield return new WaitForSecondsRealtime (speedOneLetter);
            //GetComponent<AudioSource>().PlayOneShot(audioList[Random.Range(0, audioList.Length)]);
            guiTextLink.GetComponent<Text>().text = guiTextLink.GetComponent<Text>().text + word[i];
        }

        GetComponent<AudioSource> ().Pause ();

        yield return new WaitForSecondsRealtime (2f);

        pos++;
    }

    IEnumerator PrinterDumping (){
        Color colorStart = guiTextLink.GetComponent<Text>().color;
        colorStart.a = 1f;
        guiTextLink.GetComponent<Text> ().color = colorStart;

        for (int i = 0; i < word.Length; i++) {
            yield return new WaitForSecondsRealtime (speedOneLetter);
            guiTextLink.GetComponent<Text>().text = guiTextLink.GetComponent<Text>().text + word[i];

        }
        GetComponent<AudioSource> ().Pause ();

        yield return new WaitForSecondsRealtime (2f);

        while (guiTextLink.GetComponent<Text> ().color.a > 0f) {
            yield return new WaitForSecondsRealtime (0.05f);
            Color color = guiTextLink.GetComponent<Text>().color;
            color.a -= 0.1f;
            guiTextLink.GetComponent<Text> ().color = color;
        }

        pos++;
    }

    IEnumerator EffectingAppearance (){
        Color colorStart = guiTextLink.GetComponent<Text>().color;
        colorStart.a = 0f;
        guiTextLink.GetComponent<Text> ().color = colorStart;
        while (guiTextLink.GetComponent<Text> ().color.a <= 1f) {
            yield return new WaitForSecondsRealtime (0.05f);
            Color color = guiTextLink.GetComponent<Text>().color;
            color.a += 0.1f;
            guiTextLink.GetComponent<Text> ().color = color;
        }

        GetComponent<AudioSource> ().Play ();

        yield return new WaitForSecondsRealtime (3f);

        GetComponent<AudioSource> ().Stop ();

        while (guiTextLink.GetComponent<Text> ().color.a > 0f) {
            yield return new WaitForSecondsRealtime (0.05f);
            Color color = guiTextLink.GetComponent<Text>().color;
            color.a -= 0.1f;
            guiTextLink.GetComponent<Text> ().color = color;
        }

        pos++;
    }

    IEnumerator EffectingAppearance2 (){
        Color colorStart = guiTextLink.GetComponent<Text>().color;
        colorStart.a = 0f;
        guiTextLink.GetComponent<Text> ().color = colorStart;

        while (guiTextLink.GetComponent<Text> ().color.a <= 1f) {
            yield return new WaitForSecondsRealtime (0.05f);
            Color color = guiTextLink.GetComponent<Text>().color;
            color.a += 0.1f;
            guiTextLink.GetComponent<Text> ().color = color;
        }



        while (guiTextLink.GetComponent<Text> ().color.a > 0f) {
            yield return new WaitForSecondsRealtime (0.05f);
            Color color = guiTextLink.GetComponent<Text>().color;
            color.a -= 0.1f;
            guiTextLink.GetComponent<Text> ().color = color;
        }


        while (guiTextLink.GetComponent<Text> ().color.a <= 1f) {
            yield return new WaitForSecondsRealtime (0.05f);
            Color color = guiTextLink.GetComponent<Text>().color;
            color.a += 0.1f;
            guiTextLink.GetComponent<Text> ().color = color;
        }

        while (guiTextLink.GetComponent<Text> ().color.a > 0f) {
            yield return new WaitForSecondsRealtime (0.05f);
            Color color = guiTextLink.GetComponent<Text>().color;
            color.a -= 0.1f;
            guiTextLink.GetComponent<Text> ().color = color;
        }


        while (guiTextLink.GetComponent<Text> ().color.a <= 1f) {
            yield return new WaitForSecondsRealtime (0.05f);
            Color color = guiTextLink.GetComponent<Text>().color;
            color.a += 0.1f;
            guiTextLink.GetComponent<Text> ().color = color;
        }


        pos++;
    }
}
