﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierMoving : MonoBehaviour {

	public GameObject curve;
	private BezierCurves bezierCurves;
	public float movingSpeed = 10f;
	public float rotationSpeed = 0.005f;

	private Vector3[] bezierPath;
	private LineRenderer line;
	int i = 0;

	//SetRotation variables
	Vector3 lookPos;
	Quaternion rotation;
	///


	public void SetCurve(GameObject obj){
		curve = obj;
		bezierCurves = (BezierCurves) curve.GetComponent<BezierCurves>();
		line = bezierCurves.line;

		bezierPath = new Vector3[line.positionCount];
		//bezierPath = bezierCurves.bezierPath;
		line.GetPositions (bezierPath);
		i = 0;
	}

	public bool MoveTowards(){
		if (transform.position != bezierPath [i]) {
			transform.position = Vector3.MoveTowards (transform.position, bezierPath[i] , Time.deltaTime * movingSpeed);
			SetRotation (bezierPath [i]);
			CallMovingUpdate ();
			return true;
		}else {
			if (i != bezierPath.Length - 1) {
				i++;
				return true;
			} else {
				return false;
			}
		}
	}

	public bool MoveTowardsWithoutRotation(){
		if (transform.position != bezierPath [i]) {
			transform.position = Vector3.MoveTowards (transform.position, bezierPath[i] , Time.deltaTime * movingSpeed);
			CallMovingUpdate ();
			return true;
		}else {
			if (i != bezierPath.Length - 1) {
				i++;
				return true;
			} else {
				return false;
			}
		}
	}

	void CallMovingUpdate(){
		line.GetPositions (bezierPath);
	}

	public void nextPoint(){
		if (i != bezierPath.Length - 1) i++;
		MoveTowards ();
	}

	public void SetSpeed(float value){
		movingSpeed = value;
	}

	void SetRotation(Vector3 lookAt){
		lookPos = lookAt - transform.position;
		lookPos.y = 0;
		if (lookPos.magnitude != 0) {
			rotation = Quaternion.LookRotation(lookPos);
			transform.rotation = Quaternion.Lerp(transform.rotation, rotation, rotationSpeed);
		}
	}


	public void MovePointAt(int index, Vector3 position){
		if (curve.transform.childCount < index + 1)
			return;
		curve.transform.GetChild(index).position = position;
	}


	public void restartPath(){
		i = 0;
	}

	public Transform GetPoint(int index){
		return curve.transform.GetChild (index);

	}

	//public void SetRadius(int value){
	//}


}