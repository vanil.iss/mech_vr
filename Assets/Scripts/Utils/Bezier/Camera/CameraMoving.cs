﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraMoving : MonoBehaviour {

	BezierMoving bezierMoving;
	public int cameraSpeed  = 10;

	//Transform textPosition;
	//Transform canvas;

	// Use this for initialization
	void Start () {
		bezierMoving = (BezierMoving) GetComponent<BezierMoving> ();
		bezierMoving.SetCurve (GameObject.Find ("CameraCurve"));
		bezierMoving.SetSpeed (cameraSpeed);


		//textPosition = transform.Find ("TextPosition");
		//canvas = GameObject.Find ("Canvas").transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		//canvas.position = textPosition.position;

		if (!bezierMoving.MoveTowardsWithoutRotation ()) {
			SceneManager.LoadScene(PlayerPrefs.GetString("lift", "LiftScene"));
		}
	}
}
