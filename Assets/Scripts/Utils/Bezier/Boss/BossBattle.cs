﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBattle : MonoBehaviour {


	BezierMoving bezierMoving;

 	ObjectHealth bossHealth;
	GameObject player;
	Transform pointToJump;
	bool isMoving = false;
	bool isDistanceToAttack = false;
	Vector3 movingVector;
	SoundList soundList;

	bool isCoroutineRun = false;

	public GameObject aircraft;
	private enum StateBoss {
		StayingAtPoint,
		Attacking,
		Returning,
		ReturningMove,
		Dying
	};
	private StateBoss currentState = StateBoss.StayingAtPoint;

	// Use this for initialization
	void Start () {


		bossHealth = (ObjectHealth) GetComponent<ObjectHealth> ();
		player = GameObject.Find ("Mech");

		bezierMoving = (BezierMoving) GetComponent<BezierMoving> ();
		bezierMoving.SetCurve (GameObject.Find ("JumpingMechCurve"));
		bezierMoving.SetSpeed (550f);
		pointToJump = GameObject.Find ("BossPoint").transform;
		soundList = GetComponent<SoundList> ();
	}
	
	// Update is called once per frame
	void Update () {


			
			switch (currentState) {

		case StateBoss.StayingAtPoint:
			bezierMoving.MovePointAt (0, transform.position);
			bezierMoving.MovePointAt (1, player.transform.position);
			bezierMoving.restartPath ();
			if (!isCoroutineRun) {
				StartCoroutine (Wait ());
				currentState = StateBoss.Attacking;
				soundList.PlaySound ("MechJump", 0.09f);
			}
				break;

			case StateBoss.Attacking:
				if (Vector3.Distance (transform.position, player.transform.position) <= 0.5f) {
					player.GetComponent<HealthMech> ().justShot (25);
					currentState = StateBoss.Returning;
					return;
				}

				if (!bezierMoving.MoveTowards ()) {
					currentState = StateBoss.Returning;
				}

				break;

			case StateBoss.Returning:
				bezierMoving.MovePointAt (0, transform.position);
				bezierMoving.MovePointAt (1, pointToJump.position);
				bezierMoving.restartPath ();
				currentState = StateBoss.ReturningMove;
			soundList.PlaySound ("MechJump", 0.09f);
				break;

			case StateBoss.ReturningMove:
				if (!bezierMoving.MoveTowards ()) {
					currentState = StateBoss.StayingAtPoint;
				}
				break;


		case StateBoss.Dying:
			transform.gameObject.SetActive (false);
			aircraft.SetActive (true);
			aircraft.GetComponent<AircraftExit> ().enabled = true;
			PlayerPrefs.SetInt ("gamePassed", 1);
			QuestLastLevel.NextMessage ();
				break;


			}
				

			if (bossHealth.health <= 0) {
				currentState = StateBoss.Dying;
			}

			//Debug.Log (currentState + "");


			
		}



	IEnumerator Wait(){

		//if (currentState == StateBoss.StayingAtPoint) {
		isCoroutineRun = true;
			yield return new WaitForSeconds (3);
		isCoroutineRun = false;
			//currentState = StateBoss.Attacking;
		//}
	}
		
}
