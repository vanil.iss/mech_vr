﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossThrowing : MonoBehaviour {


	GameObject jumpingMechCurve;
	GameObject mechContainer;
	GameObject player;

	GameObject bossPoint;
	GameObject mechPoint;

	BezierMoving mechMoving;
	BezierMoving bossMoving;

	public ObjectHealth residenceHealth;

	SoundList soundList;

	private enum MechThrowing {
		PrepareToJump,
		JumpToMech,
		MechToPoint,
		MoveMechToPoint,
		MechCurve,
		ThrowMech,
		BossCurve,
		Jump
	};

	private MechThrowing bossState = MechThrowing.PrepareToJump;

	// Use this for initialization
	void Start () {
	

		jumpingMechCurve = GameObject.Find ("MechThrowingCurve");
		mechContainer = GameObject.Find ("MechContainer");
		player = GameObject.Find ("Mech");

		bossPoint = GameObject.Find ("BossPoint");
		mechPoint = GameObject.Find ("MechPoint");

		mechMoving = (BezierMoving) mechContainer.GetComponent<BezierMoving> ();
		mechMoving.SetCurve(jumpingMechCurve);
		bossMoving = (BezierMoving) GetComponent<BezierMoving> ();
		bossMoving.SetCurve(jumpingMechCurve);

		//residenceHealth = (ObjectHealth) GameObject.Find("RESIDENCE").GetComponent<ObjectHealth> ();
		GetComponent<Collider> ().enabled = false;
		soundList = GetComponent<SoundList> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (residenceHealth.health <= 0) {

			switch (bossState) {

			case MechThrowing.PrepareToJump:
				bossMoving.MovePointAt (0, transform.position);
				bossMoving.MovePointAt (1, player.transform.position);
				bossMoving.SetSpeed (300f);
				bossMoving.restartPath ();
				player.GetComponent<WayPoints> ().enabled = false;
				bossState = MechThrowing.JumpToMech;
				soundList.PlaySound ("MechJump", 0.09f);
				break;
		
			case MechThrowing.JumpToMech:
				if (!bossMoving.MoveTowards ()) {
					bossState = MechThrowing.MechToPoint;
				}
				break;

			case MechThrowing.MechToPoint:

				player.transform.position = Vector3.MoveTowards (player.transform.position, 
					mechContainer.transform.position, Time.deltaTime * 300f);

				if (player.transform.position == mechContainer.transform.position)
					bossState = MechThrowing.MechCurve;

				break;

			case MechThrowing.MechCurve:
				player.transform.parent = mechContainer.transform;
				mechMoving.MovePointAt (0, player.transform.position);
				mechMoving.MovePointAt (1, mechPoint.transform.position);	
				mechMoving.SetSpeed (300f);
				mechMoving.restartPath ();
				mechContainer.transform.parent = null;
				bossState = MechThrowing.ThrowMech;
				soundList.PlaySound ("MechJump", 0.09f);
				break;

			case MechThrowing.ThrowMech:
				if (!mechMoving.MoveTowards ()) {
					player.transform.parent = null;
					player.GetComponent<WayPoints> ().Stop ();
					player.GetComponent<WayPoints> ().enabled = true;
					bossState = MechThrowing.BossCurve;
					QuestLastLevel.NextMessage ();
				}
				break;

			case MechThrowing.BossCurve:
				bossMoving.MovePointAt (0, transform.position);
				bossMoving.MovePointAt (1, bossPoint.transform.position);	
				bossMoving.restartPath ();	
				bossState = MechThrowing.Jump;
				soundList.PlaySound ("MechJump", 0.09f);
				break;

			case MechThrowing.Jump:
				if (!bossMoving.MoveTowards ()) {
					transform.GetComponent<BossBattle> ().enabled = true;
					GetComponent<Collider> ().enabled = true;
					this.enabled = false;
				}
				break;

			}


			//Debug.Log (bossState + "");
		}

	}
}
