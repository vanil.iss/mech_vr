﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AircraftExit : MonoBehaviour {

	private GameObject player;
	public float height = 3;
	public float aircraftSpeed = 30f;
	//private Transform playerPoint;

	public string saveLevelName = "lastLevel";
	public string levelName = "AmmunitionLevel";
	private BezierMoving bezierMoving;

	bool isGrabed = false;
	Vector3 mechPosition;



	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Mech");
		//playerPoint = transform.FindChild ("PlayerPoint");
		bezierMoving = (BezierMoving) GetComponent<BezierMoving> ();
		bezierMoving.SetSpeed (aircraftSpeed);

		bezierMoving.SetCurve (GameObject.Find ("AircraftExitCurve"));

		bezierMoving.MovePointAt (1, player.transform.position);

		float z = bezierMoving.GetPoint (1).position.z;
		Transform bezierPoint_0 = bezierMoving.GetPoint (0);
		bezierPoint_0.position = new Vector3(bezierPoint_0.position.x, bezierPoint_0.position.y, z);
		transform.position = bezierPoint_0.position;
		transform.rotation = bezierPoint_0.rotation;

		Transform bezierPoint_2 = bezierMoving.GetPoint (2);
		bezierPoint_2.position = new Vector3(bezierPoint_2.position.x, bezierPoint_2.position.y, z);


		mechPosition = new Vector3 (0, 0, 0);
	}

	// Update is called once per frame
	void FixedUpdate () {

		if (!bezierMoving.MoveTowards ()) {
			//SceneManager.LoadScene("Level2(TreesZone)");

			PlayerPrefs.SetString(saveLevelName, levelName);
			SceneManager.LoadScene(PlayerPrefs.GetString(saveLevelName, levelName));
		}

		//Debug.Log (Vector3.Distance (player.transform.position, transform.position) + "");

		if (!isGrabed && Vector3.Distance(player.transform.position,transform.position) <= 30f) {
			//player.transform.position = playerPoint.position;
			player.transform.SetParent (transform);
			player.GetComponent<WayPoints> ().enabled = false;
			bezierMoving.nextPoint ();
			isGrabed = true;
		}

		if (!isGrabed && Vector3.Distance (player.transform.position, transform.position) > 30f) {
			bezierMoving.MovePointAt (1, player.transform.position);
		}

		if (isGrabed) {
			player.transform.Translate(mechPosition * Time.deltaTime);
		}

	}



}
