﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aircraft : MonoBehaviour {

	private GameObject player;
	public float height = 3;
	public float aircraftSpeed = 40f;
	//private Transform playerPoint;


	private BezierMoving bezierMoving;
	bool isThrowed = false;



	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Mech");
		player.GetComponent<WayPoints> ().enabled = false;
		GameObject.Find ("Button2").GetComponent<CapsuleCollider> ().enabled = false;
		//playerPoint = transform.FindChild ("PlayerPoint");
		//игрок должен двигаться вместе с самолетом
		//player.transform.position = playerPoint.position;
		player.transform.position = transform.position;
		player.transform.SetParent (transform);

		bezierMoving = (BezierMoving) GetComponent<BezierMoving> ();
		bezierMoving.SetCurve (GameObject.Find ("AircraftEnterCurve"));
		bezierMoving.SetSpeed (aircraftSpeed);
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (!bezierMoving.MoveTowards ()) {
			this.enabled = false;
			bezierMoving.curve.SetActive (false);
			transform.gameObject.SetActive (false);
		}
		//Debug.Log (player.transform.position.y + "");

		if (!isThrowed && player.transform.position.y <= height) {
			//Debug.Log ("THROWED!!!!");
			player.transform.position = new Vector3(player.transform.position.x, height, player.transform.position.z);
			bezierMoving.nextPoint ();
			bezierMoving.SetSpeed (bezierMoving.movingSpeed + 20);
			player.transform.SetParent (null);
			player.GetComponent<WayPoints> ().Stop ();
			player.GetComponent<WayPoints> ().enabled = true;
			GameObject.Find ("Button2").GetComponent<CapsuleCollider> ().enabled = true;

		}
		
		
	}
}
