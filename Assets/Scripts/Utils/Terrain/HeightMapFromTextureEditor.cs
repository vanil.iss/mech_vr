﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(HeighmapFromTexture))]
public class HeightMapFromTextureEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        HeighmapFromTexture hFTScript = (HeighmapFromTexture)target;
        if(GUILayout.Button("Create heightmap"))
        {
            if (hFTScript.heightmap == null) { EditorUtility.DisplayDialog("No texture selected", "Please select a texture.", "Cancel"); return; }
            hFTScript.SetTerrainData(Terrain.activeTerrain.terrainData);
            hFTScript.CreateHeightMap();
        }
    }
}
#endif
