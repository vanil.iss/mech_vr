﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HeighmapFromTexture : MonoBehaviour
{
	public Texture2D heightmap;
	private TerrainData terrain;


	public void SetTerrainData(TerrainData terrain)
	{
		this.terrain = terrain;
	}
	
	public void CreateHeightMap()
	{
		int hWidth = heightmap.width;
		int hHeight = heightmap.height;
		int tWidth = terrain.heightmapWidth;
		float[,] heightmapData = terrain.GetHeights(0, 0, tWidth, tWidth);
		Texture2D result = new Texture2D(tWidth, tWidth, TextureFormat.RGB24, false);


		if (hWidth != tWidth || hHeight != tWidth)
		{

			Color[] rpixels = heightmap.GetPixels(0);
			float incX = ((float) 1 / heightmap.width) * ((float) heightmap.width / tWidth);
			float incY = ((float) 1 / heightmap.height) * ((float) heightmap.height / tWidth);
			for (int px = 0; px < rpixels.Length; px++)
			{
				rpixels[px] = heightmap.GetPixelBilinear(incX * ((float) px % tWidth), incY * Mathf.Floor((float) px / tWidth));
			}
			result.SetPixels(rpixels, 0);
			result.Apply();
		}
		else
		{
			result.SetPixels(heightmap.GetPixels());
		}
		
		Color[] mapColors = result.GetPixels();
		for (int y = 0; y < tWidth; y++) {
			for (int x = 0; x < tWidth; x++) {
				heightmapData[y,x] = mapColors[y*tWidth+x].grayscale;
			}
		}
		terrain.SetHeights(0, 0, heightmapData);
	}
}
