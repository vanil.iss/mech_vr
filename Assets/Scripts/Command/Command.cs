﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Command : MonoBehaviour
{
	public GameObject spawner;
	public virtual void Execute(){

	}

	public virtual void Execute(Object comp){

	}

	public virtual void Disable(){

	}

	public virtual void Activate(){

	}

	public virtual void Sleep()
	{
		
	}

	public virtual void ReactionToAggression()
	{
	}
}
