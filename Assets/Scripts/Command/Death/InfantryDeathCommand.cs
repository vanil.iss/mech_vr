﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class InfantryDeathCommand : Command {

	public Animator animator;
	public NavMeshAgent agent;

	public override void Execute(){
		animator.SetBool ("Running", false);

		animator.SetTrigger("Death");

		if (agent)
			agent.isStopped = true;
		
		var rigidBody = gameObject.GetComponent<Rigidbody> ();
		
		if (rigidBody == null)
			rigidBody = gameObject.AddComponent<Rigidbody>();
		
		rigidBody.AddForce (Vector3.forward * -25);
	}

	public override void Disable()
	{
		Destroy(GetComponent<Rigidbody>());
		animator.Rebind();

		GetComponent<MobModule>().moveCmd.Sleep();
	}
}
