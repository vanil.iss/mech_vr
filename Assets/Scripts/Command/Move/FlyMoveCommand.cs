﻿using System.Collections.Generic;
using System.Diagnostics;
using Unit.Mob;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class FlyMoveCommand : Command {

	private Transform target;

	public List<PointProtection> list;
	public float minHeight = 10;
	public float maxHeight = 20;

	
	public float speed = 15f;
	public float distToTarget = 50f;
	private Vector3 pos;

	private MobModule _mobModule;

	private int state = 0;
	private void Start()
	{
		_mobModule = GetComponent<MobModule>();
		Activate();
	}

	public override void Execute()
	{
		switch (state)
		{
				case 0:
					pos = target.position;
					
					if (transform.position.y < target.position.y - Random.RandomRange(5, 50) 
					    || transform.position.y > target.position.y + Random.RandomRange(5, 50))
					{	
						pos.x = transform.position.x;
						transform.position = Vector3.Lerp(transform.position, pos, speed * Time.deltaTime);
					} else if (transform.position.x < target.position.x - Random.RandomRange(5, 50) 
					           | transform.position.x > target.position.x + Random.RandomRange(5, 50))
					{
						pos.y = transform.position.y;
						transform.position = Vector3.Lerp(transform.position, pos, speed * Time.deltaTime);
					}
					else
					{
						state = 1;
					}

					break;
				case 1:
					_mobModule.attackCmd.Activate();
					Disable();

					_mobModule.currentState = MobState.Shoot;
					state = 2;
					break;
				case 2:
					pos = target.position;
					transform.position = Vector3.Lerp(transform.position, pos, speed * Time.deltaTime);
					if (Vector3.Distance(transform.position, target.position) < Random.RandomRange(5, 25))
						state = 1;
					break;
		}
//		target = comp as Transform;
//		pos = target.position;
//		pos.y = transform.position.y;	
//		transform.position = Vector3.Lerp(transform.position, pos, speed * Time.deltaTime);

//		if (Condition())
//		{
//			Disable();
//			_mobModule.attackCmd.Activate();
//
//			_mobModule.mech = target.GetComponent<MainUnit>();
//			_mobModule.currentState = MobState.Shoot;
//		}
	}


	public override void Activate()
	{
		target = list[Random.RandomRange(0, list.Count)].transform;
	}

	public override void ReactionToAggression()
	{
		target = list[Random.RandomRange(0, list.Count)].transform;
		state = 2;
		base.ReactionToAggression();
	}

	public override void Disable(){
//		agent.isStopped = true;		
	}

	public override void Sleep(){
//		agent.enabled = false;		
	}
}
