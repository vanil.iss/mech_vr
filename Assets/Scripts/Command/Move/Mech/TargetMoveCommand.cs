﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;
using UnityEngine.AI;
using VRStandardAssets.Utils;

public class TargetMoveCommand : Command
{
    public Transform target;
    public Transform mechPos;
    public Transform mainPosition;
    private TriggerZone zone;
    
    public float distToTarget = 1;

    public NavMeshAgent agent;
    private MobModule _mobModule;

    public int state = 0;
    public bool busy;
    
    public float delayTimeMain = 30f;
    private float _delayTimeMain;

    private void Start()
    {
        _mobModule = GetComponent<MobModule>();
//        delayTimeMain = Random.RandomRange(0, delayTimeMain);
        zone = mainPosition.GetComponent<TriggerZone>();
    }

    public override void Execute(Object comp)
    {
        switch (state)
        {
            //Move to pos about mech
            case 0:
                if (!HasProblem())
                    MoveToMainPosition();
                break;
            case 1:
                //Move To Target
                MoveToTarget();
                break;
            case 2:
                if (!busy){
                    transform.localEulerAngles = mechPos.localEulerAngles;
                    if (GetDist(mainPosition.position, transform.position) > distToTarget)
                        ChangeState(0);

                }
                break;
        }
    }


    private void MoveToMainPosition()
    {
        if (_delayTimeMain < delayTimeMain)
        {
            _delayTimeMain += Time.deltaTime;
            return;
        }
         
        agent.SetDestination(mainPosition.position);
        delayTimeMain = 0;
        
        if (GetDist(mainPosition.position, transform.position) <= distToTarget)
            ChangeState(2);
    }

    private void ChangeState(int state)
    {
        _delayTimeMain = 0;
        this.state = state;
    }

    private void MoveToTarget()
    {
        agent.SetDestination(target.position);
        if (GetDist(mainPosition.position, transform.position) <= distToTarget)
            ChangeState(2);
    }

    private bool HasProblem()
    {
        return zone.HasProblem != 0;
    }

    public override void Disable()
    {
        target = null;
    }

    private float GetDist(Vector3 a, Vector3 b) => Vector3.Distance(a, b);
}