﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters;
using Unit.Mob;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.XR.WSA.Input;

public class MoveProtectionPosCommand : Command
{
    public NavMeshAgent agent;
    public Animator animator;

    public List<PointProtection> list;

    public float distToTarget = 3f;

    public Transform target;
    private Transform player;

    private MobModule _mobModule;


    private RaycastHit hit;

    //TODO WRITE ACTIONS IF NOT HAS PROTECTIONS POINT
    // Use this for initialization
    void Start()
    {
        _mobModule = GetComponent<MobModule>();
    }

    public override void Disable()
    {
        agent.isStopped = true;
    }

    public override void Sleep()
    {
        agent.enabled = false;
    }

    private bool pointSelected;

    public override void Activate()
    {
        list = spawner.GetComponent<ProtectionMobsSpawner>().list;

        if (!agent.enabled)
            agent.enabled = true;

        agent.isStopped = false;

        if (pointSelected) return;

        var i = 0;
        while (i < list.Count)
        {
            var x = Random.RandomRange(0, list.Count);
            if (!list[x].isBusy)
            {
                target = list[x].transform;
                list[x].isBusy = true;
                pointSelected = true;
                return;
            }

            i++;
        }

        for (i = 0; i < list.Count; i++)
        {
            if (list[i].isBusy) continue;

            target = list[i].transform;
            list[i].isBusy = true;
            pointSelected = true;


            return;
        }
    }

//TODO WRITE ACTIONS IF UNIT DEATH
    // Update is called once per frame
    void Update()
    {
        if (_mobModule.health <= 0)
        {
            
            Debug.Log("Mob Protection Death");
        }
    }

    private int state = 0;

    public override void Execute()
    {
        switch (state)
        {
            case 0:
                agent.SetDestination(target.transform.position);

                animator.SetTrigger("Running");
                if (ConditionPos())
                    state = 1;
                break;
            case 1:
                Vector3 relativePos = _mobModule.target.position - transform.position;
                Quaternion rotation = Quaternion.LookRotation(relativePos);

                rotation.y = transform.position.y;
                transform.rotation = rotation;
                animator.SetTrigger("Shooting");

                if (ConditionVisible())
                    state = 2;
                break;
            case 2:
                state = 1;
                _mobModule.currentState = MobState.Shoot;

                Disable();
                _mobModule.attackCmd.Activate();
                break;
        }
    }

    private bool ConditionPos()
    {
        return Vector3.Distance(transform.position, target.position) < distToTarget;
    }

//    private bool ConditionVisible()
//    {
//        return Physics.Raycast(_mobModule.facePoint.transform.position, _mobModule.facePoint.transform.forward, out hit,
//                   250f)
//               && hit.transform.gameObject.tag.Equals(_mobModule.target.tag);
//    }

    private bool ConditionVisible()
    {
        return Physics.Linecast(_mobModule.facePoint.transform.position, _mobModule.target.position, out hit)
               && hit.transform.gameObject.tag.Equals(_mobModule.target.tag);
    }
}