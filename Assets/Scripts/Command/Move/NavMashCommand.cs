﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unit.Mob;
using UnityEngine;
using UnityEngine.AI;

public class NavMashCommand : Command {

	public NavMeshAgent agent;
	public Animator animator;

	private Transform target;

	public float distToTarget = 25f;
	
	private RaycastHit hit;
	private MobModule _mobModule;

	private void Start()
	{
		_mobModule = GetComponent<MobModule>();
	}

	public override void Execute(){
		agent.SetDestination(target.transform.position);

		animator.SetTrigger("Running");

		if (Condition())
		{
			_mobModule.currentState = MobState.Shoot;
			
			Disable();
			_mobModule.attackCmd.Activate();
	    	
			_mobModule.mech = hit.collider.GetComponent<MainUnit> ();
		}
	}
	
	public override void Activate(){
		if (!agent.enabled)
			agent.enabled = true;
		
		agent.isStopped = false;
	}

	public override void Disable(){
		agent.isStopped = true;		
	}

	public override void Sleep(){
		agent.enabled = false;		
	}
	
	private bool Condition()
	{
		if (!(Vector3.Distance(transform.position, _mobModule.target.position) < distToTarget)) return false;
		
		if (!TargetVisible()) return false;
		
		

		return true;
	}
	
	private bool TargetVisible()
	{
		return Physics.Raycast(_mobModule.facePoint.transform.position, _mobModule.facePoint.transform.forward, out hit, 250f)
		       && hit.transform.gameObject.tag.Equals(_mobModule.target.tag);
	}
	
	
}
