﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unit.Mob;
using UnityEngine;
using Unit;
using Object = UnityEngine.Object;

public class BobaFetAttackCommand : Command {

	public DamageType damageType;
	public int damage;

//	public float distToTarget = 50f;
	public Animator animator;


	public float rateOfSpeed = 0.5f;
	protected float _rateOfSpeed;

	private MobModule _mobModule;
	public MainUnit mech;
	private Vector3 target;
	private RaycastHit hit;
	
	private void Start()
	{
		_mobModule = GetComponent<MobModule>();
	}

	public override void Execute(){
		animator.SetTrigger("Shooting");

		_rateOfSpeed += Time.deltaTime;
		Attached(mech);
		
		if (Condition(mech))
		{
			Disable();

			_mobModule.moveCmd.Activate();
  
			_mobModule.currentState = MobState.Move;
		}
	}
	
	private void Attached(MainUnit mech)
	{
		if (_rateOfSpeed < rateOfSpeed) return;

		_rateOfSpeed = 0;

		if (mech)
			mech.Hit(damage, damageType);
	}

	public override void ReactionToAggression()
	{
		_mobModule.moveCmd.Activate();
		Disable();
            
		_mobModule.currentState = MobState.Move;
		
		base.ReactionToAggression();
	}

	private bool Condition(MainUnit unit)
	{
		target = mech.transform.position;
		
		
		if (TargetVisible(unit.tag)) return false;
		return false;
	}
	
	private bool TargetVisible(String tag)
	{
		return Physics.Raycast(_mobModule.facePoint.transform.position, target, out hit, 250f)
		       && hit.transform.gameObject.tag.Equals(tag);
	}
}
