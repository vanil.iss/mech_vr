﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Unit;
using Unit.Mob;
using UnityEngine;
using Object = UnityEngine.Object;

public class HedgehopperAttackCommand : Command {

	public float distToTarget = 25f;

	public DamageType damageType;
	public int damage;
	
	[Header("Время начала анимации")]
	public float animTimeBeforeHit = 1f;
	private float _animTimeBeforeHit;

	[Header("Частота стрельбы")]
	public float rateOfSpeed = 0.5f;
	protected float _rateOfSpeed;

	[Header("Частота проверок для смены состояния")]
	private float raycastCheck = 1f;
	private float _raycastCheck;

	[Header("Grenade")] 
	private GameObject prefabGrenade;
	public int countGrenade = 2;
	private UniqueAttackType attackType = UniqueAttackType.GRENADE;

	[Header("Время ожидания следующего броска гранаты")]
	public float timeGrenadeCheck = 5f;
	private float _timeGrenadeCheck;
	
	public Animator animator;

	public ParticleSystem particleSparks;
	public Light faceLight;

	private RaycastHit hit;
	private Vector3 target;
	private MobModule _mobModule;
	

	private void Start()
	{
		_mobModule = GetComponent<MobModule>();
		prefabGrenade = Resources.Load<GameObject>("Prefabs/Ammunition/Grenade");
	}

	private int state = 0;
	public override void Execute()
	{

		_rateOfSpeed += Time.deltaTime;
		

		switch (state)
		{
			case 0:
				animator.SetTrigger("Shooting");

				Attached(_mobModule.mech);
				if (_animTimeBeforeHit >= animTimeBeforeHit)
				{
					faceLight.enabled = true;

					particleSparks.Stop();
					particleSparks.Play();
				}
				else
					_animTimeBeforeHit += Time.deltaTime;

				faceLight.enabled = false;
				
				break;
			case 1:
				animator.SetTrigger("Running");

				UniqueAttached(_mobModule.mech);
				countGrenade--;
				ChangeState(0);
				break;
		}
		
		
		if (Condition(_mobModule.mech))
		{
			_mobModule.moveCmd.Activate();
			Disable();
            
			_mobModule.currentState = MobState.Move;
		} 
		else if (ConditionUniqueAttack()) {
						
			ChangeState(1);
		}
	}


	private void Attached(MainUnit mech)
	{
		if (_rateOfSpeed < rateOfSpeed) return;

		_rateOfSpeed = 0;

		if (mech)
			mech.Hit(damage, damageType);
	}

	private void UniqueAttached(MainUnit mech)
	{
		GameObject grenade = Instantiate(prefabGrenade, _mobModule.facePoint.transform.position, _mobModule.facePoint.transform.rotation);
		grenade.GetComponent<Grenade>().Settings(mech.transform.position);
	}
	
	public override void Activate(){
		
	}

	public override void Disable()
	{
		_animTimeBeforeHit = 0;
	}
	
	private bool Condition(MainUnit unit)
	{

		target = unit.transform.position;
		
		if (GetDist(transform.position, target) > distToTarget)
			return true;

		if (_raycastCheck <= raycastCheck) {
			_raycastCheck += Time.deltaTime;
			return false;
		}

		if (ConditionVisible()) return false;
        
		if (hit.collider != null)
		{
			if (hit.collider.tag.Equals("Decoration") || hit.collider.tag.Equals("Block"))
			{
				return true;
			}
		}

		Vector3 relativePos = target - transform.position;
		Quaternion rotation = Quaternion.LookRotation(relativePos);

		rotation.y = transform.position.y;
		transform.rotation = rotation;

		return false;
	}
	
	private bool ConditionUniqueAttack()
	{
		if (countGrenade <= 0) return false;
		
		if (_timeGrenadeCheck >= timeGrenadeCheck)
		{

			if (!(GetDist(transform.position, _mobModule.target.position) <= distToTarget / 2)) return false;
			if (UniqueAttackManager.Instance.AbilityUseAttack(attackType))
			{
				ChangeState(1);
			}
			else
				_timeGrenadeCheck = 0;


		}
		else
			_timeGrenadeCheck += Time.deltaTime;

		return false;
	}
	
	private bool ConditionVisible()
	{
		return Physics.Linecast(_mobModule.facePoint.transform.position, _mobModule.target.position, out hit)
		       && hit.transform.gameObject.tag.Equals(_mobModule.target.tag);
	}
	
	private void ChangeState(int state)
	{
		animTimeBeforeHit = 0;
		this.state = state;
	}
	
	private float GetDist(Vector3 a, Vector3 b) => Vector3.Distance(a, b);
}
