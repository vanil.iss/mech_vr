﻿using System.Collections;
using System.Collections.Generic;
using Unit;
using UnityEngine;

public class KamikazeAttackCommand : Command {
	
	public DamageType damageType;
	public int damage;

	public float radiusExplosion = 30f;
	public  ParticleSystem deathSparks;

	public override void Execute(){
		var hitColliders = Physics.OverlapSphere(transform.position, radiusExplosion);
		foreach (var collider in hitColliders)
		{
			var unit = collider.GetComponent<MainUnit>();
			Attached(unit);
		}
		
	}


	private void Attached(MainUnit unit)
	{
		if (unit)
			unit.Hit(damage, damageType);
	}
	
	public override void Activate(){
		deathSparks.Play();
	}

	public override void Disable()
	{
		
	}
}
