﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitalRuby.LightningBolt;
using Unit;
using Unit.Mob;
using UnityEngine;
using Object = UnityEngine.Object;

public class ElectricAttackCommand : Command {
	public float distToTarget = 25f;

	public DamageType damageType;
	public int damage;

	
	public float animTimeBeforeHit = 1f;
	private float _animTimeBeforeHit;

	public float rateOfSpeed = 0.5f;
	protected float _rateOfSpeed;

	public Animator animator;

	public LightningBoltScript sparksOnAttack;
	public LineRenderer lineOnAttak;
	
	private float raycastCheck = 1f;
	private float _raycastCheck;
	
	private RaycastHit hit;
	private Vector3 target;
	private MobModule _mobModule;
	
	private void Start()
	{
		_mobModule = GetComponent<MobModule>();
	}

	public override void Execute(){
		animator.SetTrigger("Shooting");

		_rateOfSpeed += Time.deltaTime;
		Attached(_mobModule.mech);
		
		if (Condition(_mobModule.mech))
		{
			Disable();

			_mobModule.moveCmd.Activate();
            
			_mobModule.currentState = MobState.Move;
		}
	}


	private void Attached(MainUnit mech)
	{
		if (_rateOfSpeed < rateOfSpeed) return;

		_rateOfSpeed = 0;

		if (mech)
			mech.Hit(damage, damageType);
	}
	
	public override void Activate()
	{
		StartCoroutine(ElectroLineActvate());
	}

	private IEnumerator ElectroLineActvate()
	{
		yield return new WaitForSeconds(animTimeBeforeHit);
		sparksOnAttack.enabled = true;
		lineOnAttak.enabled = true;
	}

	public override void Disable()
	{
		sparksOnAttack.enabled = false;
		lineOnAttak.enabled = false;
			
	}
	
	private bool Condition(MainUnit unit)
	{
		target = unit.transform.position;
		if (Vector3.Distance(transform.position, target) > distToTarget)
		{
			return true;
		}

		if (_raycastCheck <= raycastCheck) {
			_raycastCheck += Time.deltaTime;
			return false;
		}

		if (TargetVisible(unit.tag)) return false;
        
		if (hit.collider != null)
		{
			if (hit.collider.tag.Equals("Decoration") || hit.collider.tag.Equals("Block"))
			{
				return true;
			}
		}

//		Vector3 relativePos = target - transform.position;
//		Quaternion rotation = Quaternion.LookRotation(relativePos);

//		rotation.y = transform.position.y;
		transform.LookAt(target);
//		transform.rotation = rotation;

		return false;
	}
	
	private bool TargetVisible(String tag)
	{
		return Physics.Raycast(_mobModule.facePoint.transform.position, _mobModule.facePoint.transform.forward, out hit, 250f)
		       && hit.transform.gameObject.tag.Equals(tag);
	}
}
