﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Unit.Mob;
using UnityEngine;

public class SimpleTestSpawner : MonoBehaviour
{


	public int count;
	// Use this for initialization
	void Start () {
		StartCoroutine(Spawn());
	}
	
	// Update is called once per frame
	void Update ()
	{
	}

	private IEnumerator Spawn()
	{
		
		while (count > 0)
		{
			count--;
			GameObject mob = MobPoolManager.Instance.electricMobPool.Spawn(transform.position, transform.rotation);
			MobModule mobModule = mob.GetComponent<MobModule>();
			mobModule.target = GameObject.FindGameObjectWithTag("Player").transform;
			mobModule.moveCmd.Activate();
			mobModule.currentState = MobState.Move;

			yield return new WaitForSecondsRealtime(3f);

		}

	}
}
