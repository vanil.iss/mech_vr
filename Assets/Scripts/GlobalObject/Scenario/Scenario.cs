﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum TypeItem
{
	TEXTMSG, ACTION		
}


public struct Description
{
	public TypeItem type;
	public String value;
}

public class Scenario : MonoBehaviour {

	public List<UnityEvent> actionList;

	public String actName;
	public String fileName;

	protected List<Description> scenarioList;
	// Use this for initialization
	void Start()
	{
		scenarioList = new List<Description>();
		TextAsset textFile = Resources.Load<TextAsset>("Scenario/" + actName + "/" + fileName);
		if (textFile)
		{
			ScenarioData data = JsonUtility.FromJson<ScenarioData>(textFile.text);

			foreach (var item in data.items)
			{
				SortingItem(item.type, item.value);
			}
		}
		else
			Debug.Log("Not Found Scenario " + actName + "/" + fileName);

		count = 0;
	}
	
	private int count = 0;

	private void SortingItem(String type, String value)
	{
		var desc = new Description();
		switch (type)
		{
			case "TEXTMSG":
				desc.type = TypeItem.TEXTMSG;
				desc.value = value;
				break;
			case "ACTION":
				desc.type = TypeItem.ACTION;
				break;
		}
		
		scenarioList.Add(desc);
	}
}
