﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ScenarioData {
	public ScenarioItem[] items;
}

[System.Serializable]
public class ScenarioItem {
	public string type;
	public string value;
}
