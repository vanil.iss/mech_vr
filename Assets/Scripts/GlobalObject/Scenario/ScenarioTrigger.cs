﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using VRStandardAssets.Examples;


public class ScenarioTrigger : Scenario
{




	// Use this for initialization




	private bool onceTrigger;

	private int number;

	private void OnTriggerEnter(Collider other)
	{
		if (onceTrigger) return;

		onceTrigger = true;
		foreach (var scenario in scenarioList)
		{
			switch (scenario.type)
			{
				case TypeItem.TEXTMSG:
					IEnumerator func = PrintTextMsg(scenario);
					TaskManager.Instance.AddTask(func, this);
					break;
				case TypeItem.ACTION:
					actionList[number++].Invoke();
					break;
			}
		}

		TaskManager.Instance.NextTask();
	}

	private IEnumerator PrintTextMsg(Description desc)
	{
		Debug.Log(desc.value);
		yield return new WaitForSeconds(3f);
		TaskManager.Instance.NextTask();
	}
}
	