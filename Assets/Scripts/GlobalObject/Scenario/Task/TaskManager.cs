﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;


public struct Task
{
    public IEnumerator func;
    public MonoBehaviour comp;
}
public class TaskManager
{
    private static TaskManager instance;
    public static TaskManager Instance
    {
        get
        {
            if (instance != null)
                return instance;

            return instance = new TaskManager();

        }
        private set { }
    }

    private Queue<Task> tasks = new Queue<Task>();

    public void AddTask(IEnumerator func, MonoBehaviour comp)
    {
        var task = new Task{
            func = func,
            comp = comp
        };

        tasks.Enqueue(task);
    }

    public void NextTask()
    {
        if (tasks.Count > 0)
        {
            var task = tasks.Dequeue();
            task.comp.StartCoroutine(task.func);
        }
    }



}