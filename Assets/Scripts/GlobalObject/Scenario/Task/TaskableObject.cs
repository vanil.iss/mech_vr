﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskableObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public virtual void Action()
	{
		IEnumerator func = Execute();
		TaskManager.Instance.AddTask(func, this);
	}

	public virtual IEnumerator Execute()
	{
		yield return null;
		NextTask();
	}
	
	
	public void NextTask()
	{
		TaskManager.Instance.NextTask();
	}
}
