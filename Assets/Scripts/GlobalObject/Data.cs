﻿using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour
{
	public List<GameObject> coversList;
	public List<GameObject> mobList;
	public AudioClip[] unitDeath;

	public GameObjectPool shooterPool{ get; private set; }

	public uint maxCountShooters;

	public GameObjectPool electricPool{ get; private set; }

	public uint maxCountElectrics;

	public GameObjectPool kamikazePool{ get; private set; }

	public uint maxCountKamikazes;

	public GameObject tank;
	public GameObject shooter;
	public GameObject electric;
	public GameObject kamikaze;

	void Start()
	{
		coversList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Shelter"));

		shooterPool = new GameObjectPool(shooter, maxCountShooters, shooterHandler, true);
		shooterPool.prePopulate(checked((int)maxCountShooters));

		electricPool = new GameObjectPool(electric, maxCountElectrics, electricHandler, true);
		electricPool.prePopulate(checked((int)maxCountElectrics));


		kamikazePool = new GameObjectPool(kamikaze, maxCountKamikazes, kamikazeHandler, true);
		kamikazePool.prePopulate(checked((int)maxCountKamikazes));



	}

	void shooterHandler(GameObject target)
	{     
		target.name = "Shooter"; 
	}

	void electricHandler(GameObject target)
	{     
		target.name = "Electric"; 
	}

	void kamikazeHandler(GameObject target)
	{
		target.name = "Kamikaze"; 
	}

	public void deleteCover(GameObject cover)
	{
		coversList.Remove(cover);

	}

	public void deleteMob(GameObject mob)
	{
		mobList.Remove(mob);
	}

	public AudioClip getDeathUnitAudioClip()
	{
		return unitDeath[Random.Range(0, unitDeath.Length)];
	}

	void Update()
	{

	}
}
