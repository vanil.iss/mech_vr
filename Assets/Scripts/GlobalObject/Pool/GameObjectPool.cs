﻿using System.Collections;
using System;
using System.Collections.Generic;
using Unit.Mob;
using UnityEngine;

public class GameObjectPool
{


	private GameObject _prefab;
	private Animator animator;
	private Stack available;
	private ArrayList all;

	private Action<GameObject> initAction;
	private bool setActiveRecursively;

	private GameObject prefab => _prefab;

	public int numActive => all.Count - available.Count;

	public int numAvailable => available.Count;


	public GameObjectPool(GameObject prefab, uint initialCapacity, Action<GameObject> initAction, bool setActiveRecursively)
	{ 

		this._prefab = prefab;
		this.initAction = initAction;
		this.setActiveRecursively = setActiveRecursively;

		available = (initialCapacity > 0) ? new Stack((int)initialCapacity) : new Stack(); 
		all = (initialCapacity > 0) ? new ArrayList((int)initialCapacity) : new ArrayList(); 
	}

	public GameObject Spawn(Vector3 position, Quaternion rotation)
	{ 
		GameObject result; 
		if (available.Count == 0)
		{

			// create an object and initialize it.
			result = GameObject.Instantiate(prefab, position, rotation) as GameObject;

			// run optional initialization method on the object 
			if (initAction != null)
				initAction(result); 
			
			result.GetComponent<MobModule>().pool = this;

			all.Add(result);
			MobPoolManager.Instance.mobList.Add(result);

		}
		else
		{ 
			result = available.Pop() as GameObject; 
			// get the result's transform and reuse for efficiency. 
			// calling gameObject.transform is expensive. 
			Transform resultTrans = result.transform; 
			resultTrans.position = position; 
			resultTrans.rotation = rotation;
			result.GetComponent<MobModule>().pool = this;
			SetActive(result, true); 
		} 

		return result; 
	}

	private bool Destroy(GameObject target)
	{ 
		if (!available.Contains(target))
		{ 
			available.Push(target); 

			SetActive(target, false); 
			return true; 
		} 

		return false; 
	}

	// Unspawns all the game objects created by the pool.
	private void DestroyAll()
	{ 
		for (int i = 0; i < all.Count; i++)
		{ 
			GameObject target = all[i] as GameObject; 

			if (target.active)
				Destroy(target); 
		} 
	}

	// Unspawns all the game objects and clears the pool.
	public void Clear()
	{ 
		DestroyAll(); 
		available.Clear(); 
		all.Clear(); 
	}


	public bool Unspawn(GameObject obj)
	{ 
		if (!available.Contains(obj))
		{ 
			// Make sure we don't insert it twice. 
			available.Push(obj); 
			Clear(obj);
			obj.transform.position = new Vector3(-100000, 0, 0);
			return true; 
			// Object inserted back in stack. 
		} 
		return false; 
		// Object already in stack. 


	}

	// Applies the provided function to some or all of the pool's game objects.
	public void ForEach(Action<GameObject> action, bool activeOnly)
	{
		foreach (var element in all)
		{
			var target = element as GameObject; 

			if (!activeOnly || target.active)
				action(target);
		}
	}


	public void prePopulate(int num)
	{ 
		var array = new GameObject[num]; 
		for (int i = 0; i < num; i++)
		{ 
			array[i] = Spawn(new Vector3(-10000, 0, 0), Quaternion.identity); 
			SetActive(array[i], true); 
		} 
		
		for (int j = 0; j < num; j++)
		{ 
			Unspawn(array[j]); 
		} 

	}

	// ---- protected methods ----

	#region protected methods

	// Activates or deactivates the provided game object using the method
	// specified by the setActiveRecursively flag.
	protected void SetActive(GameObject target, bool value)
	{ 
		if (setActiveRecursively)
			target.SetActiveRecursively(value);
		else
			target.active = value; 
	}

	public void Clear(GameObject gameObject)
	{
		
//		if (gameObject.GetComponent<CapsuleCollider>())
//			gameObject.GetComponent<CapsuleCollider>().enabled = false;
//		else
//			gameObject.GetComponent<BoxCollider>().enabled = false;
		
//		gameObject.GetComponent<ObjectHealth>().enabled = false;

//		gameObject.GetComponent<Animator>().Rebind();
//		gameObject.GetComponent<Animator>().enabled = false;


		var mob = gameObject.GetComponent<MobModule>();
		
		if (mob)
		{
//			mob.isRespawn = true;
			mob.currentState = MobState.Wait;
			mob.health = mob.maxHealth;
		}
		
//		else if (gameObject.GetComponent<ElectricBot>())
//		{
//			gameObject.GetComponent<ElectricBot>().respawn = true;
//			gameObject.GetComponent<ObjectHealth>().health = gameObject.GetComponent<ElectricBot>().maxHealth;
//
//		}
//		else if (gameObject.GetComponent<KamikazeBot>())
//		{
//			gameObject.GetComponent<KamikazeBot>().respawn = true;
//			gameObject.GetComponent<ObjectHealth>().health = gameObject.GetComponent<KamikazeBot>().maxHealth;
//
//		}
	}

	#endregion

}
