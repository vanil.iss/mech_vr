﻿using System.Collections;
using UnityEngine;

public class MechAnimator : MonoBehaviour
{
	[Header("Global")] 
	[SerializeField] private float delta = 0.0001f;
	[Header("Interface")] 
	[SerializeField] private float button_path = 0.05f;
	[SerializeField] private float button_step = 0.01f;
	[Space]
	[SerializeField] private float small_button_path = 0.015f;
	[SerializeField] private float small_button_step = 0.005f;
	[Space]
	[SerializeField] private float rod_angle = 20;
	[SerializeField] private float rod_step = 1;
	[Space]
	[SerializeField] private float lever_diagonal_path = 0.15f;
	[SerializeField] private float lever_diagonal_step = 0.01f;
	[Space] 
	[SerializeField] private float lever_forward_path = 0.0008f;
	[SerializeField] private float lever_forward_step = 0.0001f;
	[SerializeField] private float lever_forward_clear = 0.01f;
	
	private Vector3 XAxis = new Vector3(1, 0, 0);
	private Vector3 YAxis = new Vector3(0, 1, 0);
	private Vector3 ZAxis = new Vector3(0, 0, 1);

	public void AnimButton(Transform transform)
	{
		IEnumerator callback = Move(transform,  ZAxis, button_path, button_step);
		StartCoroutine(        Move(transform, -ZAxis, button_path, button_step, callback));
	}
	
	public void AnimSmallButton(Transform transform)
	{
		IEnumerator callback = Move(transform,  ZAxis, small_button_path, small_button_step);
		StartCoroutine(        Move(transform, -ZAxis, small_button_path, small_button_step, callback));
	}

	public int AnimRod(Transform transform, int state)
	{
		if (state == 0)
		{
			StartCoroutine(Rotate(transform, -YAxis, rod_angle, rod_step));
			return 1;
		}
		
			StartCoroutine(Rotate(transform,  YAxis, rod_angle, rod_step));
			return 0;	
	}

	public int AnimLeverDiagonal(Transform transform, int state)
	{
		if (state == 0)
		{
			StartCoroutine(Move(transform,  YAxis, lever_diagonal_path, lever_diagonal_step));
			return 1;
		}
		
			StartCoroutine(Move(transform, -YAxis, lever_diagonal_path, lever_diagonal_step));
			return 0;
	}

	public int AnimLeverForward(Transform transform, int quantity, int maxQuantity = 100)
	{
		if (quantity < maxQuantity)
		{
			StartCoroutine(Move(transform, -XAxis, lever_forward_path, lever_forward_step));
			quantity++;
		}
		
		return quantity;
	}

	public void AnimClearLeverForward(Transform transform, Vector3 target)
	{
		StartCoroutine(LocalMoveTo(transform, target, lever_forward_clear));
	}
	
	private IEnumerator Move(Transform transform, Vector3 axis, float quantity, float step, IEnumerator callback = null)
	{
		Vector3 translation = axis * step;
		
		while (quantity > 0)
		{
			transform.Translate(translation);
			quantity -= step;
			yield return null;
		}

		if (callback != null) StartCoroutine(callback);
	}

	private IEnumerator Rotate(Transform transform, Vector3 axis, float quantity, float step, IEnumerator callback = null)
	{
		Vector3 rotation = axis * step;

		while (quantity > 0)
		{
			transform.Rotate(rotation);
			quantity -= step;
			yield return null;
		}
		
		if (callback != null) StartCoroutine(callback);
	}

	private IEnumerator LocalMoveTo(Transform transform, Vector3 target, float step, IEnumerator callback = null)
	{
		while (Vector3.Distance(transform.localPosition, target) > delta)
		{
			transform.localPosition = Vector3.MoveTowards(transform.localPosition, target, step);
			yield return null;
		}
		
		if (callback != null) StartCoroutine(callback);
	}
}
