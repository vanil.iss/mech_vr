﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPointsAllocation : MonoBehaviour
{

	[SerializeField] private GameObject wayPointObject;
	[SerializeField] private float radius = 1000f; 
	[SerializeField] private float interval = 100f;
	[SerializeField] private float angleInterval = 30f;
	[SerializeField] private float delta = 0.001f; /*погрешность движения*/
	//[SerializeField] private float height = 30f;
	[SerializeField] private Transform parent;
	[SerializeField] private Transform cabCamera;
	
	
	private Stack<GameObject> availableWayPoints;
	private List<GameObject> wayPointsList;
	
	private Vector3 lastIntervalPosition;
	// Use this for initialization
	void Start ()
	{
		availableWayPoints = new Stack<GameObject>();
		wayPointsList = new List<GameObject>();
		lastIntervalPosition = transform.position;
		SpawnWayPoints(lastIntervalPosition);
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance(transform.position, lastIntervalPosition) + delta >= interval)
		{
			lastIntervalPosition = transform.position;
			foreach (var wayPoint in wayPointsList) { availableWayPoints.Push(wayPoint); }
			SpawnWayPoints(lastIntervalPosition);
		}
	}

	void SpawnWayPoints(Vector3 position)
	{
		for (float i = interval; i < radius; i += interval)
		{
			for (float angle = 0; angle < 360; angle += angleInterval)
			{
				Vector3 wayPointPosition;
				wayPointPosition.x = (float) (position.x + i * Math.Cos(angle * Mathf.Deg2Rad));
				wayPointPosition.y = transform.position.y;
				wayPointPosition.z = (float) (position.z + i * Math.Sin(angle * Mathf.Deg2Rad));
				SpawnWayPoint(wayPointPosition);
			}
		}
	}

	void SpawnWayPoint(Vector3 position)
	{
		if (availableWayPoints.Count == 0)
		{

			GameObject wayPoint = Instantiate(wayPointObject, position, Quaternion.identity);
			wayPoint.transform.parent = parent;
			//sprite position
			//wayPoint.transform.GetChild(0).position = heightVector;
			//collider position
			//wayPoint.GetComponent<SphereCollider>().center = heightVector;
			availableWayPoints.Push(wayPoint);
			wayPointsList.Add(wayPoint);
		}
		else
		{
			GameObject wayPoint = availableWayPoints.Pop();
			wayPoint.transform.position = position;
		}
	}
}
