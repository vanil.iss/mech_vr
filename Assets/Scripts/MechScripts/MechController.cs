﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unit;
using Unit.Mech;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class MechController : Mech {

	[Header("Components")]
	[SerializeField]
	private CabMode cabMode;
	[SerializeField]
	private GunMode gunMode;
	
	private Pause pause;
	private MechMoving mechMoving;
	private ObjectHighlighting objectHighlighting;
	
	private Transform raycTransform;
	private float raycastLength = 1000f;
	private RaycastHit hit;

	// Use this for initialization
	void Start () {
		base.Start();
		Cursor.lockState = CursorLockMode.Locked;
		mechMoving = GetComponent<MechMoving>();
		objectHighlighting = GetComponent<ObjectHighlighting> ();
		cabMode.ActivateMonitor ("MiniMap");
		ChangeModeTo(GetCurrentMode());
		UpdateBars();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Physics.Raycast (raycTransform.position, 
			    			 raycTransform.TransformDirection (Vector3.forward), out hit, raycastLength) &&
		                     hit.collider) {
			
			switch (GetCurrentMode()) {
				case MechMode.CabMode:
					if (Input.GetMouseButton (0)) {
						switch (hit.collider.tag) {
							case "WayPoint":
								if (Input.GetMouseButtonDown(0))
								{
									mechMoving.AddWayPoint (hit.collider.GetComponentInParent<Transform> ().position);
									cabMode.OffRods();
								}
								break;
							case "InterfaceObject":
								//если кнопка мыши нажата один раз 
								if (Input.GetMouseButtonDown (0))
									cabMode.OnInterfaceObjectClicked(hit.collider.gameObject);
								//иначе, то есть если кнопка зажата
								else 
									cabMode.OnInterfaceObjectClamped(hit.collider.gameObject);
								break;
						}
					}
					else /* for auto way points moving */
					{
						if (hit.collider.tag.Equals("WayPoint") &&
						    mechMoving.MovingState.Equals(MovingState.WayPoint))
						{
							mechMoving.AddWayPoint(hit.collider.GetComponentInParent<Transform>().position);
							cabMode.OffRods();
						}
					}
					break;

				case MechMode.GunMode:
					gunMode.UpdateGunMoving(hit);
					break;
					
				default:
					Debug.Log("MechController update error: invalid mech mode");
					break;
			}
		
			objectHighlighting.HighlightGameObject(hit.collider.transform);
			
		} else /*if raycast not hit object*/ { objectHighlighting.ClearHighlighting(); }
		
		if (Input.GetKeyDown (KeyCode.F)) { ChangeMode(); }

		if (!IsAlive ()) { Debug.Log ("Dead"); }
	}

	public void ChangeMode()
	{
		ChangeModeTo(GetNextMode());
	}

	public void ChangeModeTo(MechMode mechMode)
	{
		
		switch (mechMode) {
		case MechMode.CabMode:
			cabMode.SetActive (true);
			gunMode.SetActive (false);
			raycTransform = cabMode.GetCameraTransform ();
			break;
		case MechMode.GunMode:
			cabMode.SetActive (false);
			gunMode.SetActive (true);
			raycTransform = gunMode.GetCameraTransform ();
			break;
		default:
			Debug.Log("MechController: change mode to " + Enum.GetName(typeof(MechMode), mechMode) + " error.");
			break;
		}
	}

	public override void OnHit()
	{
		UpdateBars();
	}

	public override void OnPartHit(MechPart part)
	{
		
		cabMode.UpdateStats(part);
		
//		switch (part.type)
//		{
//			case MechPartType.Body:
//				break;
//			case MechPartType.Bottom:
//				break;
//			case MechPartType.Cabine:
//				break;
//			case MechPartType.Gun:
//				break;
//			case MechPartType.LeftHand:
//				break;
//			case MechPartType.RightHand:
//				break;
//			default:
//				throw new ArgumentOutOfRangeException();
//		}
	}
	
	public void ToggleShield() { is_shield_work = !is_shield_work; UpdateBars(); cabMode.UpdateStats(is_shield_work); }
	
	private void UpdateBars(){ cabMode.UpdateBars (health, is_shield_work, shield_power); }
//	void TogglePause(bool value){
//	}
}
