﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Unit.Mech;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public enum MeterType
{
	Speedo, Tacho, Battery, Battery2, 
	Chemical, Radiation, Thermo
}

[Serializable]
public class Meter
{
	[Header("Object")] 
	public Transform arrow;
	public Vector3 axis;
		
	[Header("Properties")]
	public MeterType type;
	public float minAngle = 0f;
	public float maxAngle = 20f;
	public float step = 0.1f;
	
	public void UpdateArrow(float value)
	{
		float angle = value * (maxAngle - minAngle) + minAngle;
		
		if (angle > maxAngle) angle = maxAngle;
		if (angle < minAngle) angle = minAngle;
		
		arrow.localRotation = Quaternion.Lerp(arrow.localRotation, Quaternion.Euler(axis * angle), step);
	}
}

public class MetersController : MonoBehaviour
{
	[SerializeField] private Meter[] meters;
	
	[Header("Components")] 
	[SerializeField] private Mech mech;
	[SerializeField] private MechMoving mechMoving;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		foreach(Meter meter in meters)
		{
			float value = 0;
			
			switch (meter.type)
			{
				case MeterType.Speedo:
					value = mechMoving.GetSpeed() / mechMoving.GetMaxSpeed();
					break;
				case MeterType.Tacho:
					value = mechMoving.GetTacho() / mechMoving.GetMaxTacho();
					break;
				case MeterType.Battery:
					break;	
				case MeterType.Battery2:
					break;
				case MeterType.Chemical:
					break;
				case MeterType.Radiation:
					break;
				case MeterType.Thermo:
					break;
				default:
					Debug.Log("MetersController: wrong meter type");
					break;
			}
			meter.UpdateArrow(value);
		}
	}

}
