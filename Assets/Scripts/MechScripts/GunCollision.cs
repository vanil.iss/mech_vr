﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunCollision : MonoBehaviour
{
	public bool hasHit = false;


	private void OnTriggerEnter(Collider other)
	{
		hasHit = true;
	}
	
	private void OnTriggerStay(Collider other)
	{
		hasHit = true;
	}
	
	private void OnTriggerExit(Collider other)
	{
		hasHit = false;
	}
}
