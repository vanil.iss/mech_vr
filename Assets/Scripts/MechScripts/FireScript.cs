﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;
using VRStandardAssets.Utils;

public class FireScript : MonoBehaviour {

	#region EffectOnGun
	[SerializeField] private float time_before_shoot = 2f;
	private float _time_before_shoot = 0f;

	private AudioSource source;
	private GameObject sparksGun;
	private ParticleSystem sparksGunParticleSystem;
	private Light light;
	#endregion

	#region Animation
	Animator anim;
	#endregion


	#region EffectOnHit

	private RaycastHit hit;
	//Объект в который произведенно попадание

	public float shoot_distance = 1000f;
	//Допустимая дистанция

	public float rateOfSpeed = 0.5f;
	//Скорость стрельбы
	private float _rateOfSpeed;
	//Время с последнего выстрела

	public Transform metaHit;
	//объект при попадании в цель
	public Transform sparksMetal;
	//Искры при попадании
	public Transform firePoint; 
	#endregion
	

	void Awake() {
//		sparksGun = GameObject.FindGameObjectWithTag("SparksGun");
		sparksGunParticleSystem = GetComponentInChildren<ParticleSystem>();
		light = GetComponentInChildren<Light>();
		source = gameObject.GetComponent<AudioSource>();
	}

	void Start(){
		anim = GetComponentInChildren<Animator>();
	}

	// Update is called once per frame
	void Update(){
		if (_rateOfSpeed <= rateOfSpeed) { _rateOfSpeed += Time.deltaTime; light.enabled = false; }
		if (Input.GetKey(KeyCode.Mouse0)) { _time_before_shoot += Time.deltaTime;  anim.SetBool("isShoot", true); }
		if (Input.GetKeyUp(KeyCode.Mouse0)) { source.Stop (); _time_before_shoot  = 0;  anim.SetBool("isShoot", false); }

		if (_time_before_shoot > time_before_shoot && _rateOfSpeed > rateOfSpeed) {
				if (!source.isPlaying) { source.Play (); }
				light.enabled = true;
				anim.SetBool ("isShoot", true);
				sparksGunParticleSystem.Stop ();
				sparksGunParticleSystem.Play ();
				_rateOfSpeed = 0;
				MakeShot();
		}

	}
		

	public void MakeShot(){
		Vector3 DirectionRay = firePoint.TransformDirection (Vector3.forward);
		
		if (Physics.Raycast (firePoint.position, DirectionRay, out hit, shoot_distance)) {
			Quaternion hitRotation = Quaternion.FromToRotation (Vector3.up, hit.normal);

			//if (hit.collider.tag != "PlayerPart") {

				if (hit.rigidbody)
					hit.rigidbody.AddForceAtPosition (DirectionRay * 100, hit.point);
				if (hit.collider) {


					Transform metaHitGo = Instantiate (metaHit, hit.point + (hit.normal * 0.0001f), hitRotation);
					metaHitGo.parent = hit.transform;
					metaHitGo.GetComponent<Collider> ().enabled = false;
					Destroy (metaHitGo.gameObject, 3);
				}
				MainUnit unit = hit.collider.gameObject.GetComponent<MainUnit> ();

				if (unit) unit.Hit (1);
			//}
		}
	}

}




