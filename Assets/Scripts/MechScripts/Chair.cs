﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chair : MonoBehaviour
{

	[SerializeField] private Transform cabCamera;
	[SerializeField] private float rotationSpeed = 15f;
	
	void Update ()
	{
		transform.rotation = Quaternion.Lerp(transform.rotation, cabCamera.rotation, rotationSpeed * Time.deltaTime);
		Vector3 rotation = transform.localEulerAngles;
		rotation.Set(0, rotation.y, 0);
		transform.localEulerAngles = rotation;
	}
}
