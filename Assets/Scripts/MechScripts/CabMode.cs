﻿using Unit.Mech;
using UnityEngine;
using UnityEngine.UI;

public class CabMode : MonoBehaviour {

	[SerializeField] private GameObject cabCamera;
	[SerializeField] private Text healthBar, shieldBar;
	[SerializeField] private StatsMonitor statsMonitor;
	[SerializeField] private GameObject[] monitors;
	
	private Transform raycastTransform;
	private float raycastLength = 1000f;
	
	private MechInterface mechInterface;
	
	void Start()
	{
		mechInterface = GetComponent<MechInterface>();
	}
	
	public void OnInterfaceObjectClicked(GameObject gameObject){ mechInterface.OnClick(gameObject); }
	
	public void OnInterfaceObjectClamped(GameObject gameObject){ mechInterface.OnClick(gameObject, true); }
	
	public void UpdateBars(int health, bool is_shield_work, int shield_power){
		healthBar.text = health + "";
		if (is_shield_work) shieldBar.text = shield_power + "";
		else shieldBar.text = "OFF";
	}

	public void SetActive(bool value){
		if (enabled == value) return;
		enabled = value;
		cabCamera.SetActive(value);
	}

	public void ActivateMonitor(string monitorName){
		foreach(GameObject monitor in monitors)
		{
			if(monitor.name.Equals(monitorName)){
				monitor.SetActive(true);
			}else{
				monitor.SetActive(false);
			}
		}
	}
	
	public Transform GetCameraTransform() { return cabCamera.transform; }

	public void UpdateStats(MechPart part)
	{
		statsMonitor.SetValue(part);
		statsMonitor.Blink(part.type);
	}

	public void UpdateStats(bool is_shield_work) { statsMonitor.SetShieldValue(is_shield_work); }

	public void OffRods() { mechInterface.OffOtherRods(""); }
	
}
