﻿using System.Collections.Generic;
using UnityEngine;

public class MechInterface : MonoBehaviour {

	[SerializeField] private List<InterfaceElement> interfaceElements = new List<InterfaceElement>();
	[SerializeField] private MechAnimator mechAnimator;

	private List<InterfaceElement> rods;
	private InterfaceElement fuelSupply;
	private Vector3 fS_StartPos = new Vector3();
	
	private void Start()
	{
		rods = new List<InterfaceElement>();
	    //initialize special elements
		foreach (InterfaceElement e in interfaceElements)
		{
			if (e.type == InterfaceElementType.Rod) { rods.Add(e); }

			if (e.type == InterfaceElementType.LeverForward)
			{
				fuelSupply = e;

				Vector3 fS_Pos = fuelSupply.gameObject.transform.localPosition;
				fS_StartPos.Set(fS_Pos.x, fS_Pos.y, fS_Pos.z);
			}
		}
	}

	public void OnClick(GameObject gameObject, bool is_clamped = false)
	{
		foreach (var e in interfaceElements)
		{
			if (e.gameObject.Equals(gameObject))
			{
				if (!is_clamped || e.can_be_clamped)
				{
					e.elementEvent.Invoke();
					Animate(e);
				}
			}
		}
	}

	private void Animate(InterfaceElement e)
	{
		switch (e.type)
		{
			case InterfaceElementType.Button:
				mechAnimator.AnimButton(e.gameObject.transform);
				break;
			case InterfaceElementType.SmallButton:
				mechAnimator.AnimSmallButton(e.gameObject.transform);
				break;
			case InterfaceElementType.Rod:
				e.animationState = mechAnimator.AnimRod(e.gameObject.transform, e.animationState);
				break;
			case InterfaceElementType.LeverDiagonal:
				e.animationState = mechAnimator.AnimLeverDiagonal(e.gameObject.transform, e.animationState);
				break;
			case InterfaceElementType.LeverForward:
				e.animationState = mechAnimator.AnimLeverForward(e.gameObject.transform, e.animationState);
				break;
		}
	}

	/* UnityEvent function */
	public void OffOtherRods(string exclusionName)
	{
		foreach (InterfaceElement rod in rods)
		{
			if (!rod.gameObject.name.Equals(exclusionName) && rod.animationState > 0) { Animate(rod); }
		}
	}

	/* UnityEvent function */
	public void OffRod(string name)
	{
		foreach (InterfaceElement rod in rods)
		{
			if(rod.gameObject.name.Equals(name)&& rod.animationState > 0) Animate(rod);
		}
	}

	/* UnityEvent function */
	public void OffFuelSupply()
	{
		if (fuelSupply == null) return;
		fuelSupply.animationState = 0;
		mechAnimator.AnimClearLeverForward(fuelSupply.gameObject.transform, fS_StartPos);
		
	}
}
