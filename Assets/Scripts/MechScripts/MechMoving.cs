﻿using System;
using System.Collections.Generic;
using Unit.Mech;
using UnityEngine;
using UnityEngine.AI;

[Serializable]
public enum MovingState { None = 0, Direction = 1, WayPoint = 2 }

public class MechMoving : MonoBehaviour
{

	private NavMeshAgent agent;
	
	private Vector2 direction = new Vector2(0, 0);
	[SerializeField] private float max_move_speed = 20f;
	[SerializeField] private float step = 0.1f;
	private float move_speed = 0;
	[SerializeField] private float way_points_rotation_speed = 0.005f;
	[SerializeField] private bool saveWayPoints = false;
	
	private MovingState movingState = MovingState.None;
	public MovingState MovingState => movingState;
	
	private Vector3 targetPosition;
	private float buttons_rotation_speed = 15f;

	private float delta = 4f;
	//SetRotation
	private Vector3 lookPos;
	private Quaternion rotation;

	private readonly Queue<Vector3> wayPointsQueue = new Queue<Vector3>();

	private Vector3 lastPosition;

	void Start(){
		targetPosition = transform.position;
		agent = GetComponent<NavMeshAgent>();
	}

	void FixedUpdate ()
	{

		lastPosition = transform.position;
		if (movingState != MovingState.None && move_speed < max_move_speed) move_speed += step;
		
		switch (movingState)
		{
			case MovingState.WayPoint:
				if (Vector3.Distance(targetPosition, transform.position) > delta)
				{
					Rotate(targetPosition);
					Vector3 vector = targetPosition - transform.position;
					agent.Move(vector.normalized * move_speed * Time.deltaTime);
				}
				else
				{
					if(wayPointsQueue.Count != 0) { targetPosition = wayPointsQueue.Dequeue(); }
					else SetZeroMovingState(MovingState.None);
				}
				break;
				
			case MovingState.Direction:
				agent.Move(direction.y * transform.forward * move_speed * Time.deltaTime);
				transform.Rotate(Vector3.up, direction.x * buttons_rotation_speed * Time.deltaTime);
				break;
				
			case MovingState.None:
				move_speed = 0;
				break;	
		}

		if (movingState != MovingState.WayPoint)
		{
			targetPosition = transform.position;
		}
	}


	void Rotate(Vector3 lookAt)
	{
		lookPos = lookAt - transform.position;
		lookPos.y = 0;
		if (Math.Abs(lookPos.magnitude) > delta) {
			rotation = Quaternion.LookRotation(lookPos);
			transform.rotation = Quaternion.Lerp(transform.rotation, rotation, way_points_rotation_speed);
		}
		//this.transform.LookAt
	}

	public void AddWayPoint(Vector3 position)
	{

		if (position != targetPosition)
		{
			if(saveWayPoints) wayPointsQueue.Enqueue(position);
			else targetPosition = position;
		}
		
		SetZeroMovingState(MovingState.WayPoint);
	}
	
	/* UnityEvent function */
	public void SetMovingState(int state)
	{
		if (state > (int) MovingState.WayPoint)
		{
			Debug.Log("MechMoving SetMovingState error: wrong state.");
			return;
		}
		movingState =(MovingState) state;
	}

	/* UnityEvent function */
	public void ToggleXDirection(int x)
	{
		direction.x = direction.x == x ? 0 : x; 
		// x^2 + y^2 > 0
		if (direction.sqrMagnitude > 0) SetMovingState((int) MovingState.Direction);
		else SetMovingState((int) MovingState.None);

	}
	
	/* UnityEvent function */
	public void ToggleYDirection(int y)
	{
		direction.y = direction.y == y ? 0 : y;
		// x^2 + y^2 > 0
		if (direction.sqrMagnitude > 0) SetMovingState((int) MovingState.Direction);
		else SetMovingState((int) MovingState.None);
	}

	public void SetZeroMovingState(MovingState state)
	{
		movingState = state;
		direction.Set(0, 0);
	}

	public float GetSpeed() { return move_speed; }

	public float GetMaxSpeed() { return max_move_speed; }

	private float max_tacho = 90;
	public float GetTacho()
	{
		float distance = Vector3.Distance(transform.position, lastPosition);
		if (distance < delta) return max_tacho;
		return move_speed / distance;
	}
	
	public float GetMaxTacho(){ return max_tacho; }

	private int speedUpQuantity;
	/* UnityEvent function */
	public void SpeedUp(int maxQuantity)
	{
		if (speedUpQuantity < maxQuantity)
		{
			if (move_speed >= max_move_speed)
			{
				speedUpQuantity++;
				move_speed = max_move_speed + speedUpQuantity * step;
			}	
		}
	}
	/* UnityEvent function */
	public void ClearSpeedUp()
	{
		if (move_speed > max_move_speed) move_speed = max_move_speed;
		speedUpQuantity = 0;
	}
	
}
