﻿using System.Collections;
using System.Collections.Generic;
using Unit.Mech;
using UnityEngine;
using UnityEngine.VR;
using VRStandardAssets.Utils;

public class GunMode : MonoBehaviour {

	[Header("Components")]
	[SerializeField]
	private GameObject gunCamera;
	[SerializeField]
	private Transform gunContainer;
	
	private GunMoving gunMoving;
	private FireScript fireScript;

	//TODO если Start не успевает найти компоненты и бросает NullPointerException
	void Awake () {
		gunMoving = gunContainer.GetComponent<GunMoving> ();
		fireScript = gunContainer.GetComponent<FireScript> ();
	}
		
	public void SetActive(bool value){
		if (enabled == value) return;
		enabled = value;
		gunCamera.SetActive (value);
		gunMoving.enabled = value;
		fireScript.enabled = value;
	}

	public void UpdateGunMoving(RaycastHit hit)
	{
		//если расстояние до какого-либо объекта очень маленькое, остановить движение дула
		gunMoving.can_moving = !(hit.distance <= 15f);
	}

	public Transform GetCameraTransform() { return gunCamera.transform; }
}
