﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectHighlighting : MonoBehaviour {

	
	[SerializeField] private Shader highlightShader;
	[SerializeField] private Color highlightColor = new Color32(100, 181, 246, 255);
	private List<string> selectableTags = new List<string>(){"WayPoint", "Decoration", "Shooter", "InterfaceObject"};
	
	private Transform lastSelectedObject;
	private Renderer lastSelectedObjectRenderer;
	private Shader lastSelectedObjectShader;
	

	public void HighlightGameObject(Transform selectedObject)
	{
		if (selectedObject == lastSelectedObject) return;
		ClearHighlighting();
		if(!selectableTags.Contains(selectedObject.tag)){ return; }

		Renderer selectedObjectRenderer = selectedObject.GetComponent<Renderer>()
									   ?? selectedObject.parent.GetComponent<Renderer>()
									   ?? selectedObject.GetChild(0).GetComponent<Renderer>();
		if (selectedObjectRenderer == null) { return; }
		
		Material selectedObjectMaterial = selectedObjectRenderer.material;
		lastSelectedObject = selectedObject;
		lastSelectedObjectRenderer = selectedObjectRenderer;
		lastSelectedObjectShader = selectedObjectMaterial.shader;

		selectedObjectMaterial.shader = highlightShader;
		//selectedObjectMaterial.SetColor("_HighlightColor", highlightColor);
	}

	public void ClearHighlighting(){
		if (lastSelectedObject != null)
		{
			lastSelectedObjectRenderer.material.shader = lastSelectedObjectShader;
			lastSelectedObject = null;
		}
	}
}
