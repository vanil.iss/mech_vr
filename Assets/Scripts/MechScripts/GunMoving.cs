﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;
using VRStandardAssets.Utils;

public class GunMoving : MonoBehaviour {

	[SerializeField] private float m_Damping = 0.5f;                                // The damping with which this gameobject follows the camera.
	[SerializeField] private float m_GunContainerSmoothing = 10f;                   // How fast the gun arm follows the reticle.
	private const float k_DampingCoef = -20f;                                       // This is the coefficient used to ensure smooth damping of this gameobject.
	//[SerializeField] private GunCollision gunCollision;
	[SerializeField] private Reticle m_Reticle;                                     // This is what the gun arm should be aiming at.
	[SerializeField] private Transform m_GunContainer;   							// This contains the gun arm needs to be moved smoothly.
	[SerializeField] private Transform gunCamera;

	Quaternion lookAtRotation;

	public bool can_moving = true;

	void FixedUpdate () {
		
		if (can_moving) {

			/*transform.rotation = Quaternion.Slerp (transform.rotation, UnityEngine.XR.InputTracking.GetLocalRotation (UnityEngine.XR.XRNode.Head),
				m_Damping * (1 - Mathf.Exp (k_DampingCoef * Time.deltaTime)));*/

			m_GunContainer.position = gunCamera.position;

			lookAtRotation = Quaternion.LookRotation (m_Reticle.ReticleTransform.position - m_GunContainer.position);

			m_GunContainer.rotation = Quaternion.Slerp (m_GunContainer.rotation, lookAtRotation,
				m_GunContainerSmoothing * Time.deltaTime);
		}

	}


}
