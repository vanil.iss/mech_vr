﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZone : MonoBehaviour
{
    public int HasProblem { get; set; }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.GetComponent<MainUnit>())
            HasProblem++;
    }

    private void OnTriggerExit(Collider other)
    {

        if (!other.GetComponent<MainUnit>())
            HasProblem--;
    }
}