﻿using Unit;

public class BuildingPart : MainUnit
{
	private Building building;

	void Start()
	{
		base.Start();
		health = 0;
		building = transform.parent.GetComponent<Building>();
	}
	public override void Hit(int damage, DamageType damageType = DamageType.Health) { building.Hit(damage); }
}
