﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameObjectData : MonoBehaviour {

	[SerializeField] private Data data;
	[SerializeField] private int health;
	private Vector3 bounds;
	[SerializeField] private UnityEvent onDeathEvent;
	/*меш выделения*/
	[SerializeField] private MeshRenderer selectionMesh = null;

	/*if it's true damage is inflicted both to the parent and the child*/
	public bool useParent = false;
	private ObjectHealth parentObjectHealth = null;

	void Start()
	{
		data = GameObject.FindGameObjectWithTag("Data").GetComponent<Data>();
		bounds = GetComponent<Collider>().bounds.size;

		if (useParent)
		{
			parentObjectHealth = transform.parent.GetComponent<ObjectHealth>();
			health = parentObjectHealth.health;
		}

		setSelection (false);
	}


	public void activisionHit(int value)
	{
		if (health > 0) {
			if (name.Equals ("head")) { health -= value * 2; } 
			else { health -= value; }

			if (useParent) { parentObjectHealth.health = health; }
		} else { onDeathEvent.Invoke (); }
	}

	public void toggleSelection(){ if (selectionMesh != null) { selectionMesh.enabled = !selectionMesh.enabled; } }
	public void setSelection(bool value){ if (selectionMesh != null) { selectionMesh.enabled = value; } }

}
