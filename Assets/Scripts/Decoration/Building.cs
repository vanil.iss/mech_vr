﻿using System.Collections;
using Unit;
using UnityEngine;

public class Building : MainUnit {

	private Transform mainHouse;
	private Transform fracturedHouse;
	private Transform[] fracturedHousePieces;

	private Animator fracturedHouseAnimator;
	private Color alphaChannel;
	private int fracturesLength;
	
	protected bool can_be_crushed = true;

	// Use this for initialization
	void Start ()
	{
		base.Start();
		mainHouse = transform.GetChild (0);
		fracturedHouse = transform.GetChild (1);

		fracturedHousePieces = fracturedHouse.GetComponentsInChildren<Transform>();
		fracturesLength = fracturedHousePieces.Length;

		fracturedHouseAnimator = fracturedHouse.GetComponent<Animator> ();
		fracturedHouse.gameObject.SetActive (false);
		alphaChannel.a = 0;
	}

	IEnumerator Destroy()
	{
		mainHouse.gameObject.SetActive (false);
		fracturedHouse.gameObject.SetActive (true);

		while (fracturedHouse.childCount > 0)
		{
			for (int i = 1; i < fracturesLength; i++)
			{

				if (!fracturedHousePieces[i])
				{
					continue;
				}

				Material[] materialsArray = fracturedHousePieces[i].gameObject.GetComponent<MeshRenderer>().sharedMaterials;

				for (int j = 0; j < materialsArray.Length; j++)
				{
					materialsArray[j].SetFloat("_SliceAmount", materialsArray[j].GetFloat("_SliceAmount") + Time.deltaTime * 0.3f);
					if (materialsArray[j].GetFloat("_SliceAmount") > 1f)
					{
						Destroy(fracturedHousePieces[i].gameObject);
					}
				}
			}
			yield return null;
		}

		gameObject.SetActive (false);
		enabled = false;
	}

	public override void Hit(int damage = 0, DamageType damageType = DamageType.Health)
	{
		health -= damage;
		base.Hit();
	}


	public override void OnDeath()
	{
		StartCoroutine(Destroy());
	}
}
