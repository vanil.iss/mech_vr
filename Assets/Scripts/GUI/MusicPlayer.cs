﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicPlayer : MonoBehaviour {

	public AudioClip[] clipsArray;
	int playingPosition, displayedPosition;
	bool is_pause;
	bool is_volume_on;

	AudioSource audioSource;
	
	GameObject musicNameText;
	GameObject volumeText;

	private Color playColor, pauseColor, standartColor;

	void Awake() { DontDestroyOnLoad(transform.gameObject); }
	
	// Use this for initialization
	void Start () {
		playColor = new Color32(77, 182, 172, 255);
		pauseColor =  new Color32(255, 205, 210, 255);
		standartColor = new Color(1F, 1F, 1F);
		
		clipsArray = Resources.LoadAll<AudioClip>("Music");
		musicNameText = GameObject.Find ("MusicName");
		volumeText = GameObject.Find ("Volume");
		audioSource = GetComponent<AudioSource> ();

		audioSource.volume = 0.1f;
		audioSource.PlayOneShot (clipsArray[0]);
		TogglePlay();
		
		UpdateMusicNameText();
		UpdateVolumeText ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!is_pause && !audioSource.isPlaying) {
			if (playingPosition == clipsArray.Length - 1){ playingPosition = 0; }
			else { playingPosition++; }
			audioSource.PlayOneShot (clipsArray [playingPosition]);
			displayedPosition = playingPosition;
			UpdateMusicNameText ();
		}
	}

	private void UpdateMusicNameText()
	{
		musicNameText.GetComponent<Text>().text = clipsArray[displayedPosition].name + "";
		if (playingPosition == displayedPosition)
		{
			if (is_pause) { musicNameText.GetComponent<Text>().color = pauseColor; }
			else { musicNameText.GetComponent<Text>().color = playColor; }
		}
		else { musicNameText.GetComponent<Text> ().color = standartColor; }			
	}

	private void UpdateVolumeText(){ volumeText.GetComponent<Text>().text = Mathf.CeilToInt(audioSource.volume*10) + ""; }
	
	public void NextComposition(){
		if (displayedPosition == clipsArray.Length - 1){ displayedPosition = 0; }
		else { displayedPosition++; }
		UpdateMusicNameText ();
	}

	public void PreviousComposition(){
		if (displayedPosition == 0) displayedPosition = clipsArray.Length;
		displayedPosition--;
		UpdateMusicNameText ();
	}

	public void TogglePlay(){
		if (playingPosition != displayedPosition) {
			playingPosition = displayedPosition;
			audioSource.Stop ();
			audioSource.PlayOneShot (clipsArray [playingPosition]);
			is_pause = false;
			UpdateMusicNameText ();
		} else {
			if (audioSource.isPlaying) {
				audioSource.Pause ();
				is_pause = true;
				UpdateMusicNameText ();
			} else {
				audioSource.UnPause ();
				is_pause = false;
				UpdateMusicNameText ();
			}
		}
	}

	public void IncreaseVolume(){
		if (audioSource.volume != 1f){ audioSource.volume += 0.1f; }
		UpdateVolumeText ();
	}


	public void DecreaseVolume(){
		if (audioSource.volume != 0f){ audioSource.volume -= 0.1f; }
		UpdateVolumeText ();
	}

	public void ToggleVolume(){
		if (audioSource.volume != 0) { audioSource.volume = 0; is_volume_on = false; } 
		else { is_volume_on = true; }
		UpdateVolumeText ();
	}	
}
