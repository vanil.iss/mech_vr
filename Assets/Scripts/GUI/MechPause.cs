﻿using System.Collections;
using System.Collections.Generic;
using Unit.Mech;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MechPause : MonoBehaviour {


	public bool is_pause;
	public GameObject menu;
	public CrossHair crossHair;

	private GameObject player;
	private RaycastHit hit;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		is_pause = false;
		crossHair = gameObject.GetComponent<CrossHair>();
	}

	// Update is called once per frame
	void Update()
	{
		if (is_pause && Input.GetMouseButtonDown(0))
		{
			if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 1000f))
			{
				if (hit.collider.name.Equals("Resume")) { is_pause = false; togglePause(); }
				if (hit.collider.name.Equals("Quit")) { Time.timeScale = 1.0f; SceneManager.LoadScene("LiftScene"); }
			}
		}
	}

	public void togglePause()
	{
		if (is_pause)
		{
			if (crossHair != null) { crossHair.enabled = false; }
			Cursor.lockState = CursorLockMode.None;
			Time.timeScale = 0f;
			if (player.GetComponent<MechController> ().GetCurrentMode() != MechMode.CabMode) {
				player.GetComponent<MechController> ().ChangeModeTo(MechMode.CabMode);
			}
			player.GetComponent<MechController>().enabled = false;
			menu.SetActive(true);
		}
		else
		{
			if (crossHair != null) { crossHair.enabled = true; }
			Cursor.lockState = CursorLockMode.Locked;
			Time.timeScale = 1f;
			player.GetComponent<MechController>().enabled = true;
			menu.SetActive(false);
		}
	}

}
