﻿using System.Collections;
using System.Collections.Generic;
using Unit;
using UnityEngine;

public abstract class MainUnit : MonoBehaviour {
	
	public int health = 10;
	
//TODO Change class to enum	
//	public DamageType damageType;
	
	public virtual void OnDeath() { }
	public virtual void OnHit(){ }

	public virtual void Hit(int damage = 0, DamageType damageType = DamageType.Health)
	{
		OnHit();
		if(health <= 0) OnDeath();
	}
	public bool IsAlive(){ return health > 0; }
	
	// Use this for initialization
	protected void Start () { }
	
	// Update is called once per frame
	protected void Update () {
		//Exmaple pause
		//if (Pause.TogglePause)
		//	return;
	}

	
}
