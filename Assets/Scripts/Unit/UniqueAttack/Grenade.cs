﻿using System.Collections;
using System.Collections.Generic;
using Unit;
using UnityEngine;


/*
 * Variant destroy:
 * 1 - timer in Enumuerator
 * 2 - check distance to target in Update
 * 
 */

public class Grenade : MonoBehaviour
{
    
    public float angle = 20f;
    public float speed = 2f;
    
    public DamageType damageType;
    public int damage;
    
    public float radiusExplosion = 30f;

    public float timerDeath;
    public  ParticleSystem deathSparks;

    private Rigidbody _rigidbody;
    private Vector3 target;

    public void Settings(Vector3 target)
    {
        _rigidbody = GetComponent<Rigidbody>();

        Vector3 dir = GetVelocity(target, angle);
        _rigidbody.velocity = speed * dir;

        this.target = target;
        
        StartCoroutine(InflictDamage());
    }

    private IEnumerator InflictDamage()
    {
        yield return new WaitForSeconds(timerDeath);
        deathSparks.Play();
        var hitColliders = Physics.OverlapSphere(transform.position, radiusExplosion);
        foreach (var collider in hitColliders)
        {
            var unit = collider.GetComponent<MainUnit>();
            Attached(unit);
        }
        Destroy(this, 3f);
    }

    private void Attached(MainUnit unit)
    {
        if (unit)
        {
            Debug.Log(unit.name + ":" + unit.health);
            unit.Hit(damage, damageType);
            Debug.Log(unit.name + ":" + unit.health);
        }
    }

    private Vector3 GetVelocity(Vector3 target, float angle)
    {
        Vector3 dir = target - transform.position; // get target direction

        float h = dir.y; // get height difference
        dir.y = 0; // retain only the horizontal direction
        float dist = dir.magnitude; // get horizontal distance
        float a = angle * Mathf.Deg2Rad; // convert angle to radians
        dir.y = dist * Mathf.Tan(a); // set dir to the elevation angle
        dist += h / Mathf.Tan(a); // correct for small height differences
        // calculate th    e velocity magnitude
        float vel = Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * a));

        return vel * dir.normalized;
    }

    private void Update()
    {
        if (!(Vector3.Distance(transform.position, target) < 5f)) return;
        
        var hitColliders = Physics.OverlapSphere(transform.position, radiusExplosion);
        foreach (var collider in hitColliders)
        {
            var unit = collider.GetComponent<MainUnit>();
            Attached(unit);
        }
        Destroy(this);
    }
}