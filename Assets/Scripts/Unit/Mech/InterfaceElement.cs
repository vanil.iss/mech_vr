﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public enum InterfaceElementType
{
	Button, SmallButton, Rod, LeverDiagonal, LeverForward
}

[Serializable]
public class InterfaceElement
{
	public UnityEvent elementEvent;
	public GameObject gameObject;
	public InterfaceElementType type;
	public bool can_be_clamped = false;

	public int animationState = 0;
	private AudioClip clip;
}