﻿using UnityEngine;
using Random = System.Random;

[System.Serializable]
public enum MechPartType { Body = 0, Bottom = 1, Cabine = 2, Gun = 3, LeftHand = 4, RightHand = 5 }
public enum MechPartState { Green = 0, Yellow = 1, Red = 2, Black = 3, Shield = 4 }
public enum MechMode { CabMode, GunMode }

[System.Serializable]
public class MechPart
{
	public MechPartType type;
	public int health = 100;
	public int maxHealth = 100;
	public MechPartState state = MechPartState.Green;

	public void Hit(int damage)
	{
		health -= damage;

		int s = 3;
		if (health > 0)
		{
			s = (int) Mathf.Lerp(3, 0, (float) health / maxHealth);
		}
		state = (MechPartState) s;
		
	}
}

namespace Unit.Mech
{
	public class Mech : MainUnit {
		
		[SerializeField] private MechPart[] parts = {};
		private Random _random = new Random();
		
		public bool is_shield_work;
		public int shield_power = 100;
		
		private MechMode[] _mechModesArray = {MechMode.CabMode, MechMode.GunMode};
		private int _cur_mode_index;
		
		public override void Hit(int damage = 0, DamageType damageType = DamageType.Health)
		{
			//get random part
			MechPart part = parts[_random.Next(0, parts.Length)];
			switch (damageType) {
				case DamageType.ShieldHealth:
					if (is_shield_work && shield_power > 0) { shield_power -= damage; } 
					else { part.Hit(damage); }
					break;
				case DamageType.Shield:
					if (is_shield_work && shield_power > 0) { shield_power -= damage; }
					break;
				case DamageType.Hard:
					if (is_shield_work && shield_power > 0) { shield_power -= (damage / 2); part.Hit(damage / 2); }
					else{ part.Hit(damage); }
					break;
			}
			OnPartHit(part);
			base.Hit();
		}

		public MechMode GetNextMode()
		{
			_cur_mode_index++;
			if (_cur_mode_index >= _mechModesArray.Length) _cur_mode_index = 0;
			return _mechModesArray[_cur_mode_index];
		}

		public MechMode GetCurrentMode()
		{
			return _mechModesArray[_cur_mode_index];
		}
		
		public virtual void OnPartHit(MechPart part) {}

		public void HitButton()
		{
			Hit(10, DamageType.Hard);
		}
	}
}
