using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unit.Mob;
using System;
using UnityEngine.AI;

public class MobModule : MainUnit
{
    public MobState currentState = MobState.Wait;

    public Transform target;

    public float distToTarget = 25f;
    private float _distToTarget;

    public GameObject facePoint;
    private RaycastHit hit;


    public Command moveCmd;
    public Command attackCmd;
	public Command deathCmd;

	public MainUnit mech;

	[HideInInspector] public int maxHealth;

	public GameObjectPool pool;

	private void Awake()
    {
		maxHealth = health;
    }


    protected new void Update()
    {
		base.Update();
        switch (currentState)
        {
            case MobState.Move:
                moveCmd.Execute();
//                ConditionMoveChange();
                break;
			case MobState.Shoot:
                attackCmd.Execute();
//                ConditionAttackChange();

				break;
		case MobState.Wait:
			break;
        }
    }

	public override void Hit (int damage = 0, Unit.DamageType damageType = Unit.DamageType.Health)
	{
		health -= damage;
		
		base.Hit ();
	}

	public override void OnHit()
	{
		switch (currentState)
		{
			case MobState.Move:
				moveCmd.ReactionToAggression();
				break;
			case MobState.Shoot:
				attackCmd.ReactionToAggression();
				break;
			case MobState.Wait:
				break;
		}
	}

	public override void OnDeath ()
	{
		
		if (currentState != MobState.Wait)
		{
			Debug.Log("DEATH");
			currentState = MobState.Wait;
			deathCmd.Execute ();

			if (pool != null)
			{
				StartCoroutine(Unspawn());
			}
		}
	
		currentState = MobState.Wait;
	//	animator.SetBool("Running", false);
	//	animator.SetTrigger("Death");
	}

	private IEnumerator Unspawn()
	{
		yield return new WaitForSeconds(5f);
		deathCmd.Disable();
		pool.Unspawn(gameObject);
	}
}