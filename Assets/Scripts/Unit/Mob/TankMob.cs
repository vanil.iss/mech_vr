﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unit.Mob;
using UnityEngine;
using Random = UnityEngine.Random;

public class TankMob : Mob {

	public AudioClip[] shootClips;
	public  ParticleSystem deathSparks;
	public ParticleSystem particleSparks;

	// Use this for initialization
	void Start () {
		base.Start();
	}
	
	// Update is called once per frame
	private new void Update () {
		base.Update();
		
		if (isRespawn)
		{
			Respawn();
			isRespawn = false;
		}

		switch (currentState)
		{
			case MobState.Move:
				Move();
				break;
			case MobState.Shoot:
				Shoot();
				break;
			case MobState.Wait:
				break;
			case MobState.Escape:
				break;
			case MobState.Death:
				Clear();
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}
		
	}

	private new void Shoot()
	{
		base.Shoot();
		
		if (_rateOfSpeed < rateOfSpeed) return;
		if (hit.collider == null) return;

		source.PlayOneShot(getRandomShoot());
		particleSparks.Play();
	}
	
	private AudioClip getRandomShoot()
	{
		return shootClips[Random.Range(0, shootClips.Length)];
	}

	private new void Clear()
	{
		base.Clear();
		if (isDeath) return;
		
		deathSparks.Play();

		isDeath = true;
	}
}
