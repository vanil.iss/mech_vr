﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unit.Mob;
//using UnityEditor;
using UnityEngine;

public class KamikazeMob : Mob {

	public float radiusExplosion = 30f;
	public  ParticleSystem deathSparks;


	// Update is called once per frame
	protected new void Update () {
		base.Update();

		if (isRespawn){
			Respawn();
			isRespawn = false;
		}

		switch (currentState){
			case MobState.Move:
				Move();
				break;
			case MobState.Shoot:
				Shoot();
				break;
			case MobState.Death:
				Clear();
				break;
			case MobState.Escape:
				Escape();
				break;
			case MobState.Wait:
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}
	}
	
	private new void Shoot()
	{
		var hitColliders = Physics.OverlapSphere(transform.position, radiusExplosion);
		foreach (var collider in hitColliders)
		{
			var unit = collider.GetComponent<MainUnit>();
			if (unit)
				unit.Hit(30, damageType);
		}
//		currentState = MobState.Death;
	}

	private new void Clear()
	{
		base.Clear();
		if (isDeath) return;

		deathSparks.Play();

		isDeath = true;
	}
}
