﻿using System;
using Unit;
using Unit.Mob;
using UnityEngine;

public class HedgehopperMob : Mob
{
//	private ParticleSystem particle;
    public GameObject particleSparks;

    private Light faceLight;

    private Cover cover;


    // Use this for initialization
    protected new void Start()
    {
        base.Start();

        Respawn();
//		particle = GetComponentInChildren<ParticleSystem>();

        particleSparks.GetComponent<ParticleSystem>().Stop();
        faceLight = particleSparks.GetComponentInChildren<Light>();
        faceLight.enabled = false;
    }


    // Update is called once per frame
    protected new void Update()
    {
        base.Update();

        if (isRespawn)
        {
            Respawn();
            isRespawn = false;
        }

        if (currentState != MobState.Death)
        {
//            if (!IsAlive())
//            {
//                agent.enabled = true;
//                //TODO Audio clip manager
////				source.PlayOneShot(data.getDeathUnitAudioClip());
//                currentState = MobState.Death;
//            }
//            else if
            if (health <= maxHealth / 2)
            {
                if (TakeCover())
                {
                    agent.enabled = true;
                    agent.Stop();
//                    agent.SetDestination(CoverManager.Instance.coversList[coverNum].transform.position);
//                    agent.Resume();
                    currentState = MobState.Escape;
                }
            }
        }


        switch (currentState)
        {
            case MobState.Move:
                Move();
                break;
            case MobState.Shoot:
                Shoot();
                break;
            case MobState.Death:
                Clear();
                break;
            case MobState.Escape:
                Escape();
                break;
            case MobState.Wait:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }


    private new void Shoot()
    {
        base.Shoot();
        Attached();
        if (_animTimeBeforeHit >= animTimeBeforeHit)
        {
            faceLight.enabled = true;

            particleSparks.GetComponent<ParticleSystem>().Stop();
            particleSparks.GetComponent<ParticleSystem>().Play();
        }
        else
            _animTimeBeforeHit += Time.deltaTime;

        faceLight.enabled = false;
    }

    private bool TakeCover()
    {
        if (CoverManager.Instance.coversList.Count == 0)
        {
            return false;
        }

        var coverDistance = new float[CoverManager.Instance.coversList.Count];


        var i = 0;
        foreach (var cover in CoverManager.Instance.coversList)
        {
            coverDistance[i] = Vector3.Distance(this.transform.position, cover.transform.position);
            i++;
        }

        var sortedDistance = (float[]) coverDistance.Clone();
        Array.Sort(sortedDistance);

        if (sortedDistance[0] <= 200)
        {
            for (int k = 0; k < CoverManager.Instance.coversList.Count; k++)
            {
                if (sortedDistance[k] > 200)
                    return false;
                int j = Array.IndexOf(coverDistance, sortedDistance[k]);
                cover = CoverManager.Instance.coversList[j];
                if (cover.safe && !cover.full)
                {
                    cover.full = true;
                    coverNum = j;
                    return true;
                }
            }
        }

        return false;
    }

    private new void Clear()
    {
        base.Clear();
        if (isDeath) return;

        if (coverNum > 0 && cover != null)
        {
            CoverManager.Instance.coversList[coverNum].GetComponent<CoverScript>().full = false;
            coverNum = -1;
        }

        isDeath = true;
//		Statistic.countKilled++;
    }
}