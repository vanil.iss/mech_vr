﻿namespace Unit.Mob
{
    public enum MobState
    {
        Move, Shoot, Wait, Escape, Death
    }
}