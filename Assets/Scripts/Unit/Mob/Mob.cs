﻿using System.Collections;
using Unit;
using Unit.Mech;
using Unit.Mob;
using UnityEngine;
using UnityEngine.AI;

public class Mob : MainUnit
{
    protected RaycastHit hit;

    public MobState currentState = MobState.Wait;
    public Transform target;
    protected Transform player;

    #region GrovingVariable

    protected float _distToTarget;
    protected float _rateOfSpeed;
    protected float _animTimeBeforeHit;
    protected float _timeRegeneration;
    protected float _timeBeforeTelepotation;


    #endregion

    #region Constance
    
    public DamageType damageType;
    [SerializeField] LayerMask raycastMask;

    public int damage;
    public float distToTarget = 25f;
    public float rateOfSpeed = 0.5f;
    public float animTimeBeforeHit = 1f;
    public float timeBeforeTelepotation = 2f;

    public int maxHealth;

    #endregion
    
    #region Bools

    public bool isRespawn = false;
    protected bool isDeath = false;
    protected bool isEndSlice = true;
    private bool isShelterDestroy = false;
    
    #endregion
    
    #region Escape

    protected int coverNum = -1;

    #endregion
   
    #region ComponentsCash

    protected Animator animator;
    protected Collider mobCollider;
    protected NavMeshAgent agent;
    protected AudioSource source;

    public GameObject model;
    public GameObject facePoint;

    #endregion
    

    protected void Awake()
    {
        animator = GetComponent<Animator>();
        
		if (GetComponent<CapsuleCollider>())
            mobCollider = GetComponent<CapsuleCollider>();
        else
            mobCollider = GetComponent<BoxCollider>();
		
        agent = GetComponent<NavMeshAgent>();
        source = GetComponent<AudioSource>();
        player = GameObject.FindWithTag("Player").transform;

        if (model == null)
            model = GetComponent<Transform>().Find("NPS").gameObject;
        
    }

    protected new void Start()
    {
        base.Start();

        currentState = MobState.Move;
        target = player;

    }

    protected new void Update()
    {
        base.Update();

        _distToTarget = Vector3.Distance(target.transform.position, transform.position);

        if (_rateOfSpeed <= rateOfSpeed)
            _rateOfSpeed += Time.deltaTime;

        SliceCheck();
    }

    public override void OnDeath()
    {
        if (currentState != MobState.Death)
        {
            agent.enabled = true;
            //TODO Audio clip manager
//			source.PlayOneShot(data.getDeathUnitAudioClip());
            currentState = MobState.Death;
            StartCoroutine(Unspawn());
        }
    }

    protected void Move()
    {
        agent.updatePosition = true;

        _animTimeBeforeHit = 0f;

        if (_distToTarget <= distToTarget && LookAtSomething())
        {
            currentState = MobState.Shoot;
            agent.enabled = false;
            return;
        }

        //TODO Определиться с укрытиями
        agent.SetDestination(target.transform.position);
        animator.SetTrigger("Running");
    }

    protected void Shoot()
    {
        if (_distToTarget > distToTarget)
        {
            currentState = MobState.Move;
            agent.enabled = true;

            return;
        }

        if (!LookAtSomething())
        {
            if (hit.collider != null)
            {
                if (hit.collider.tag.Equals("Decoration") || hit.collider.tag.Equals("Block"))
                {
                    currentState = MobState.Move;
                    agent.enabled = true;

                    return;
                }
            }

            Vector3 relativePos = target.transform.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos);

            rotation.y = transform.position.y;
            transform.rotation = rotation;
        }
        
        animator.SetTrigger("Shooting");
    }


    protected void Wait()
    {
    }

    protected void Escape()
    {
        _animTimeBeforeHit = 0f;

        if (coverNum >= 0)
        {
            Cover cover = CoverManager.Instance.coversList[coverNum];
//            var coverUnit = cover.GetComponent<MainUnit>();
            
            if (cover != null)
            {
                if (cover.health <= 1)
                {
                    agent.Stop();
                    agent.SetDestination(player.transform.position);
                    agent.Resume();
                    isShelterDestroy = true;
                    currentState = MobState.Move;
                    return;
                }
                
                if (Vector3.Distance(cover.transform.position, transform.position) <= 5)
                {
                    if (_timeRegeneration <= 10)
                    {
                        health += 1;
                        _timeRegeneration = 0;
                    }
                    else
                        _timeRegeneration += Time.deltaTime;
                }
                
                if (health == maxHealth)
                {
                    cover.full = false;
                    agent.Stop();
                    agent.SetDestination(player.transform.position);
                    agent.Resume();
                    currentState = MobState.Move;
                    return;

                }
                
//                В случае ненависти к телепортации - раскомментировать
//                agent.SetDestination(cover.transform.position);
//                animator.SetTrigger("Running");
                
                if (_timeBeforeTelepotation >= timeBeforeTelepotation)
                {
                    transform.position = cover.transform.position;
                }
                else
                    _timeBeforeTelepotation += Time.deltaTime;
            }

        }
        else
        {
            agent.Stop();
            agent.SetDestination(player.transform.position);
            agent.Resume();
            isShelterDestroy = true;
            currentState = MobState.Move;
            
        }

    }

    protected void Clear()
    {
        if (isDeath) return;

        if (animator.GetBool("Running"))
            animator.SetBool("Running", false);


        animator.SetTrigger("Death");

        agent.Stop();

        agent.enabled = false;
        mobCollider.enabled = false;
        source.enabled = false;
    }

    protected void Death()
    {
    }

    protected void Attached()
    {
        if (_rateOfSpeed < rateOfSpeed) return;
        if (hit.collider == null) return;

        _rateOfSpeed = 0;

        var unit = hit.collider.GetComponent<Mech>();
        if (unit)
            unit.Hit(damage, damageType);
    }


    private bool LookAtSomething()
    {
        return Physics.Raycast(facePoint.transform.position, facePoint.transform.forward, out hit, 1000f,
                   raycastMask) && hit.transform.gameObject.tag.Equals("Player");
    }

    private void SliceCheck()
    {
        if (isDeath && !isEndSlice)
        {
            model.GetComponent<SkinnedMeshRenderer>().material.SetFloat("_SliceAmount",
                model.GetComponent<SkinnedMeshRenderer>().material.GetFloat("_SliceAmount") + Time.deltaTime * 0.2f);
            isEndSlice = false;
            if (model.GetComponent<SkinnedMeshRenderer>().material.GetFloat("_SliceAmount") > 1f)
            {
                isEndSlice = true;
                this.enabled = false;
            }
        }
        else
        {
            if (!isDeath && !isEndSlice)
            {
                model.GetComponent<SkinnedMeshRenderer>().material.SetFloat("_SliceAmount", 0f);
                isEndSlice = true;
            }

            if (isDeath)
                isEndSlice = false;
        }
    }

    protected void Respawn()
    {
        player = GameObject.FindWithTag("Player").transform;
//		healthMech = player.GetComponent<HealthMech>();
        agent = GetComponent<NavMeshAgent>();
        agent.enabled = true;
        mobCollider.enabled = true;
        animator.enabled = true;
        source.enabled = true;

        currentState = MobState.Move;
        isDeath = false;
    }

    protected IEnumerator Unspawn()
    {
        yield return new WaitForSecondsRealtime(8f);
    }
}