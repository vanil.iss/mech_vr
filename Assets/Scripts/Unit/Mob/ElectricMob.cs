﻿using DigitalRuby.LightningBolt;
using Unit;
using Unit.Mech;
using Unit.Mob;
using UnityEngine;

public class ElectricMob : Mob {
	
	private Light faceLight;
	public LightningBoltScript sparksOnAttack;
	public LineRenderer lineOnAttak;

	private int absorbedEnergy;
	
	public float rateOfSpeedHealth = 1f;
	private float _rateOfSpeedHealth;
	
	public ParticleSystem deathSparks;
	
	protected new void Awake()
	{
		base.Awake();
	}

	private void Start()
	{
		base.Start();		
		Respawn();
		
		sparksOnAttack.enabled = false;
		absorbedEnergy = 0;
	}

	
	// Update is called once per frame
	protected new void Update () {
		base.Update();
		
		if (isRespawn){
			Respawn();
			
			absorbedEnergy = 0;
			isRespawn = false;
		}

//		if (currentState != MobState.Death)
//		{
//			if (!IsAlive())
//			{
//				agent.enabled = true;
//				//TODO Audio clip manager
////				source.PlayOneShot(data.getDeathUnitAudioClip());
//				currentState = MobState.Death;
//			} 
//		}
		
		if (_rateOfSpeedHealth <= rateOfSpeedHealth)
			_rateOfSpeedHealth += Time.deltaTime;
		

		switch (currentState){
			case MobState.Move:
				Move();
				if (currentState == MobState.Shoot)
				{
					sparksOnAttack.enabled = true;
					lineOnAttak.enabled = true;
				}
				break;
			case MobState.Shoot:
				Shoot();
				break;
			case MobState.Death:
				Clear();
				break;
		}
	}
	
	private new void Shoot()
	{
		//agent stop -> not used
		base.Shoot();
		Attached();
		if (currentState == MobState.Move)
		{
			sparksOnAttack.enabled = false;
			lineOnAttak.enabled = false;
			
			//Update pos -> not used
			return;
		}
	}


	private new void Attached()
	{
		if (hit.collider == null) return;
		
		var unit = hit.collider.GetComponent<Mech>();
		if (!unit) return;
		
		if (_rateOfSpeed < rateOfSpeed)
		{
			_rateOfSpeed = 0;
			if (absorbedEnergy == 10)
			{
				health += 2;
				absorbedEnergy = 2;
			}
			absorbedEnergy += 2;
				
			if (unit.shield_power - damage >= 0)
				unit.Hit(damage, damageType);
			else
				unit.shield_power = 0;
		}
		
		if (_rateOfSpeedHealth > rateOfSpeedHealth && (!unit.is_shield_work || unit.shield_power <= 0))
		{
			_rateOfSpeedHealth = 0;

			unit.Hit(1, DamageType.Hard);
	

		}

		
	}

	private new void Clear()
	{
		base.Clear();
		if (isDeath) return;
		
		sparksOnAttack.enabled = false;
		lineOnAttak.enabled = false;
		
		deathSparks.Play();

		isDeath = true;

	}
}
