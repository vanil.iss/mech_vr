﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Unit.Mob;
using UnityEngine;

public class ProtectionMobsSpawner : TaskableObject
{

	public List<PointProtection> list;

	public int countHedgehopper;
	public int countElectric;

	public float timeSpawn = 3;

	public override void Action()
	{
		base.Action();
	}

	public override IEnumerator Execute()
	{
		while (countElectric + countHedgehopper > 0)
		{
			if (countHedgehopper > 0){
				countHedgehopper--;
				GameObject mob = MobPoolManager.Instance.hedgehopperMobPool.Spawn(transform.position, transform.rotation);
				SettingsMobModule(mob.GetComponent<MobModule>());
				
				yield return new WaitForSecondsRealtime(timeSpawn);
			} 
			else if (countElectric > 0){
				countElectric--;
				GameObject mob = MobPoolManager.Instance.electricMobPool.Spawn(transform.position, transform.rotation);
					
				SettingsMobModule(mob.GetComponent<MobModule>());

				yield return new WaitForSecondsRealtime(timeSpawn);
			}
		}
		
		NextTask();
	}

	private void SettingsMobModule(MobModule mobModule)
	{
		mobModule.mech = GameObject.FindGameObjectWithTag("Player").GetComponent<MainUnit>();
		mobModule.target = mobModule.mech.transform;
		mobModule.moveCmd.spawner = gameObject;
		mobModule.moveCmd.Activate();
		mobModule.currentState = MobState.Move;
	}
}