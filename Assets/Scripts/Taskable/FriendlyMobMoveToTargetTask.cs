﻿using System.Collections;
using System.Collections.Generic;
using Unit.Mob;
using UnityEngine;

public class FriendlyMobMoveToTargetTask : TaskableObject
{
    public bool waitFinish;
    public float timeToFinish;
    
    public MobModule mob;
    public TargetMoveCommand moveCommand;

    public Transform target;
    
    public override IEnumerator Execute()
    {
        moveCommand.target = target;
        moveCommand.state = 1;
        moveCommand.busy = true;;
        mob.currentState = MobState.Move;
        
        if (waitFinish)
            while (moveCommand.state == 1)
            {
                yield return new WaitForSeconds(0.5f);
            }

        yield return new WaitForSeconds(timeToFinish);
        
        moveCommand.busy = false;
        
        NextTask();
    }
}
